"use strict";
/** Socket */

console.log("soy el socket js");

var socket = io.connect('http://localhost:3002/');
var notificaciones = io.connect('http://localhost:3002/notificaciones');
// Cuando se conecta el server recibimos el id
socket.on('cliente_nueva_conexion', function (socket_id) {
	console.error(socket_id);
});
// Canal para enviar el socket id a la DB
socket.on('canal login', function (socket_id) {
	ajax('actualizar_socket_id', 'POST', {socket_id}).then(res => {
		console.log('Socket conectado');
		// console.log(socket_id + ' guardado');
	}).catch(e => {
		console.warn(e);
	});
});
socket.on('canal secreto cliente', function (msg) {
	console.warn('privado');
	console.log(msg);
	obtenerNotificaciones();
});
