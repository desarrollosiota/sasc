"use script";

/** Hace peticiones a la API con la url global */
var mi_ajax = [];
async function ajax($url = '/api', $metodo = 'GET', $data = {}, xhr = '') {
	// if (xhr == '') xhr = 'holi';
	// if (mi_ajax[xhr] && mi_ajax[xhr].readyState != 4) {
	// 	mi_ajax[xhr].abort();
	// }
	mi_ajax[xhr] = $.ajax({
		// la URL para la petición
		url : window.location.origin + '/api/' + $url,
		// especifica si será una petición POST o GET
		type : $metodo,
		// la información a enviar
		data : $data,
		// el tipo de información que se espera de respuesta
		dataType : 'json',
	});
	return mi_ajax[xhr];
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

/**
* Funcion para obtener $_GET de un url
* @param param string : espera nombre de parametro enviado por get
* @return string
*/
const $_GET = function (param = "") {
	var url = document.URL;
	url = String(url.match(/\?+.+/));
	url = url.replace("?", "");
	url = url.split("#");
	url = url[0];
	url = url.split("&");
	for(let i = 0, long = url.length; i < long; i++) {
		// Haciendo un array delimitado por '='
		let parametro = url[i].split("=");
		let index = parametro[0];
		// Verificando que sea el parametro deseado
		if (index == param) {
			if(typeof(parametro[1]) !== 'undefined') {
				let value = decodeURIComponent(parametro[1]);
				return value.replaceAll(["#!", "#"], "");
			}
		}
	}
	return false;
}

/**
 * Metodo para remplazar una busqueda en un string
 * @param  string|array 	search
 * @param  string|array 	replacement
 */
String.prototype.replaceAll = function(search, replacements) {
	let value = this.valueOf();
	/** Verificando si es un array search */
	if(Array.isArray(search)) {
		/** Verificando si es un array replacement */
		if(Array.isArray(replacements)) {
			let last_replace = '';
			/** Recorriendo los datos */
			for(let i = 0, long = search.length; i < long; i++) {
				let replace = '';
				let replacement = replacements[i];
				/** Verificando existencia */
				if(replacement != undefined) {
					last_replace = replacement;
					replace = replacement;
				} else {
					replace = last_replace;
				}
				value = value.split(search[i]).join(replace);
			}
		} else {
			/** Recorriendo datos */
			for(let i = 0, long = search.length; i < long; i++) {
				value = value.split(search[i]).join(replacements);
			}
		}
		return value;
	}
	return value.split(search).join(replacements);
}

/** Funcion para mostrar datos monetarios */
function format_currency(amount = 0, decimals = 2) {
	var neg = false;
	if(amount < 0) {
		neg = true;
		amount = Math.abs(amount);
	}
	return (neg ? "-$" : '$') + parseFloat(amount, 10).toFixed(decimals).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}

/**
 * Metodo capitalizar un string
 *
 * Ejemplo:
 *
 * 		let test = 'hola, como estas?';
 * 		test.capitalize() => 'Hola, como estas?';
 * 		test.capitalize(true) => 'Hola, Como Estas?';
 *
 * @param {Boolean} $allTheWords -> Boleana para saber si se capitalizaran todas las palabras
 * @return string
 */
String.prototype.capitalize = function($allTheWords = false) {
	if($allTheWords) {
		return this.toLowerCase().replace(/(^|\s)([a-z])/g, function(m, p1, p2) {
			return p1 + p2.toUpperCase();
		});
	}
	return this.charAt(0).toUpperCase() + this.slice(1);
}


/**
 * Metodo para remplazar una busqueda en un string
 * @param  string date: fecha que se va a formatear
 * @param  string format: tipo del formato
 * @param  string separator: separador de la fecha
 */
function formatDate(date,format,separator = '/'){
   var date = new Date(date);
   if(date.toString() != 'Invalid Date'){
		 let D = date.getDate();
		 let M = date.getMonth();
		 let Y = date.getFullYear();
		 let H = date.getHours();
		 let MIN = date.getMinutes();
		 let S = date.getSeconds();
		 switch (format) {
			case 'FL-MX':
			   return D.toString().padStart(2,0) + separator + (M + 1).toString().padStart(2,0) + separator + Y;
			   break;
			case 'HL-MX':
			   return H + ':' + MIN.toString().padStart(2,0) + ':' + S.toString().padStart(2,0);
			   break;
			case 'FHL-MX':
			   return D.toString().padStart(2,0) + separator + (M + 1).toString().padStart(2,0) + separator + Y + ' ' + H + ':' + MIN.toString().padStart(2,0) + ':' + S.toString().padStart(2,0);
			   break;
			default:
			   return date;
		 }
   }
   return date;
}

function beautifulTimeElapsed(time_stamp) {
	let $time = new Date(time_stamp);
	let $now = new Date();
	let $res = '';
	// $time = self:: timeElapsed($time);
	if ($time.getDate() == $now.getDate()) {
		if ($time.getHours() == $now.getHours()) {
			if ($time.getMinutes() == $now.getMinutes()) {
				return 'hace unos momentos';
			} else {
				parseInt(Math.abs($time - $now) / 60000) == 1 ? $res = 'hace 1 minuto' : '';
				parseInt(Math.abs($time - $now) / 60000) > 1 ? $res = 'hace ' + parseInt(Math.abs($time - $now) / 60000) + ' minutos' : '';
				return $res || 'hace unos momentos';
			}
		} else {
			parseInt(Math.abs($time - $now) / 3600000) == 1 ? $res = 'hace 1 hora' : '';
			parseInt(Math.abs($time - $now) / 3600000) > 1 ? $res = 'hace ' + parseInt(Math.abs($time - $now) / 3600000) + ' horas' : '';
			if ($res != '') return $res;
		}
	} 
	let minutes = $time.getMinutes();
	let hours = $time.getHours();
	if (minutes < 10) minutes = "0" + minutes;
	if (hours < 10) hours = "0" + hours;
	return $time.getDate() + ' de ' + numberToMonth($time.getMonth() + 1) + ' a las ' + hours + ':' + minutes;
	
}

function numberToMonth($n) {
	let $return = '';
	$n == 1 ? $return = 'Enero' : '';
	$n == 2 ? $return = 'Febrero' : '';
	$n == 3 ? $return = 'Marzo' : '';
	$n == 4 ? $return = 'Abril' : '';
	$n == 5 ? $return = 'Mayo' : '';
	$n == 6 ? $return = 'Junio' : '';
	$n == 7 ? $return = 'Julio' : '';
	$n == 8 ? $return = 'Agosto' : '';
	$n == 9 ? $return = 'Septiembre' : '';
	$n == 10 ? $return = 'Octubre' : '';
	$n == 11 ? $return = 'Noviembre' : '';
	$n == 12 ? $return = 'Diciembre' : '';
	return $return;
}