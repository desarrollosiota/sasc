"use strict";

$(document).ready(function ($) {
	$('#page_loader').removeClass('hide');
	$('#tarjetas_inicio').addClass('hide');
	ajax('ticket', 'GET').then((tickets) => {
		let tpl = tplTarjetas(tickets);
		$('div#tarjetas_inicio').html(tpl);
	}).catch((e) => {
		console.warn(e);
	}).finally(()=>{
		$('#page_loader').addClass('hide');
		$('#tarjetas_inicio').removeClass('hide');
	});
});

function tplTarjetas(tickets) {
	let tpl = '';
	tickets.forEach((ticket) => {
		let status = (ticket.status == 0) ? 'warning' : (ticket.status == 1 || ticket.status == 3) ? 'primary' : 'alert';
		let status_texto = (ticket.status == 0) ? 'Ab.' : (ticket.status == 1 || ticket.status == 3) ? 'Term.' : 'Can.';
		tpl += `
			<div class="cell">
				<div class="card-info ${status}">
					<div class="card-info-label">
						<div class="card-info-label-text">
							${status_texto}
						</div>
					</div>
					<div class="card-section">
						<div class="card-info-content">
							<h3 class="lead elipsis">${ticket.cliente.nombre}</h3>
							<p><b>Ticket No. </b> ${ticket.id}</p>
							<p><b>Monto total: </b> ${format_currency(ticket.monto_total)}</p>
							<p><b>Servicios: </b>${ticket.tickets_detalle.length}</p>
						</div>
					</div>
					<hr style='margin: .25rem 1em;'>
					<div class="card-section">
						<span class="float-right">
						${beautifulTimeElapsed(ticket.created_at)}
						</span>
						<a href='/operacion/servicios/nuevo?ticket=${ticket.id}' title='Ver ticket'>
							<i class="fas fa-eye fa-lg" style='color:#000000b5;'></i>
						</a>
					</div>
				</div>
			</div>

		`;
		// <button class="btn" onclick="window.location = '/operacion/servicios/nuevo?ticket=${ticket.id}'">
		// Ver
		// </button>
	});
	return tpl;
}
