console.warn('ticket_view.js');

var ticket_id = false;
if ($_GET('ticket')) {
	ticket_id = $_GET('ticket');
}

$(document).ready(function ($) {
	if ($_GET('ticket')) {
		continuar_ticket();
	} else {
		buscar_clientes();
	}
});

function buscar_clientes(){
	ajax('cliente', 'GET').then((data) => {
		var HTML = [];
		HTML.push(`<option value="">Seleccione Cliente</option>`);
		data.forEach(cliente => {
			HTML.push(`<option value="${cliente.id}">${cliente.nombre}</option>`);
		});
		$('#select_cliente').html(HTML.join(''));
		$('#nuevo_ticket').removeClass('hide');
		$('#spinner_page').addClass('hide');
		$("#select_cliente").select2({'placeholder':'Seleccione Cliente', theme: "foundation",openOnEnter: false});
  }).catch((e) => {
	  toastr["error"]('No se pudo cargar la lista de clientes, favor de recargar la pagina', "Error");
  });
}

function crear_ticket(){
	var id_cliente = $('#select_cliente').val();
	var comentarios = $('#com_nuevo_ticket').val();
	$('#spinner_page').removeClass('hide');
	$('#nuevo_ticket').addClass('hide');
	if(id_cliente != ''){
		ajax('ticket','POST',{
			cliente_id: id_cliente,
			comentarios: comentarios
		}).then(data => {
			toastr["success"]("Se creo el ticket No. T-00"+data.id, "Nuevo Ticket");
			ticket_id = data.id;
			continuar_ticket();
		}).catch((e) => {
    		toastr["error"]("No se pudo crear el ticket", "Error");
			$('#spinner_page').addClass('hide');
			$('#nuevo_ticket').removeClass('hide');
		});
	}else{
    	toastr["error"]("Favor de seleccionar un cliente", "Error");
	}
}

function continuar_ticket(){
	var ticket = ticket_id;
	if(ticket){
		ajax('ticket/'+ticket,'GET').then(data => {
			$('#res_ticket').html('T-0000'+data.id);
			$('#res_cliente').html(data.cliente.nombre);
			$('#res_comentario').html(data.comentarios);
			$('#res_fecha').html(formatDate(data.created_at, 'FHL-MX'));
			$('#nuevo_ticket').addClass('hide');
			$('#spinner_page').addClass('hide');
			$('#dashboard').removeClass('hide');
			console.log(data);
			if(data.cliente.servicios != undefined){
				var HTML = [`<option value="">Seleccione un servicio</option>`];
				data.cliente.servicios.forEach(servicio => {
					HTML.push(`<option value="${servicio.pivot.id}">${servicio.nombre}</option>`);
				});
				$('#select_servicio').html(HTML.join(''));
				$("#select_servicio").select2({'placeholder':'Seleccione un servicio', theme: "foundation",openOnEnter: false});
			}
			let tpl_tabla_detalles = '';
			let total_ticket = 0;
			let no_servicios = 0;
			data.tickets_detalle.forEach((detalle) => {
				total_ticket += parseInt(detalle.precio);
				no_servicios ++;
				tpl_tabla_detalles += `
					<div class="checkout-summary-item" id='servicio_${detalle.id}'>
						<div class="item-name">
							<p>${detalle.servicios_costo.servicio.nombre}</p>
							<p><span class="title">Unidad: </span>${detalle.unidad_id}</p>
							<p><h6 class="subheader">${beautifulTimeElapsed(detalle.created_at, 'FHL-MX')}</h6></p>
						</div>
						<div class="item-price">
							<p class="title">${format_currency(detalle.precio)}</p>
							${data.status == 0 ? `<a href="#!" class="float-right" onclick='eliminar_servicio(${detalle.id})' id='text_${detalle.id}'>Borrar</a>` : ''}
							<i class="fas fa-spinner fa-spin fa-lg float-right hide" id='loader_${detalle.id}'></i>
						</div>
					</div>`
			});
			$('#no_servicios').html(no_servicios);
			$('#total_ticket').html(format_currency(total_ticket));
			$('#items').html(tpl_tabla_detalles);
			localStorage.setItem('_tk_detalle',JSON.stringify({ticket_id: data.id,terminado: data.updated_at}));
			if (data.status > 0) {
				$('#div_captura').addClass('hide');
				$('#btn-terminar').addClass('hide');
				$('#btn-terminado').removeClass('hide');
				$('#div_comentarios').removeClass('hide');
				mostrar_comentarios();
			}
		}).catch(error => {
			buscar_clientes();
			console.log("error", error);
		});
	}else{
    	toastr["error"]("No se definió un ticket", "Error");
	}
};

function guardar_servicio(){
	var datos = JSON.parse(localStorage._tk_detalle);
	if(datos.ticket_id != undefined){
		datos.servicio_costo_id = $('#select_servicio').val();
		datos.unidad_id = $('#unidad').val();
		datos.comentario = $('#comentario_servicio').val();
		if(datos.servicio_costo_id != '' && datos.unidad_id != ''){
			$('button#nuevo_servicio_boton').prop('disabled', true);
			$('#nuevo_servicio_loader').removeClass('hide');
			$('#nuevo_servicio_text').addClass('hide');
			ajax('ticket_detalle','POST', datos).then(data => {
				ticket_id = data.ticket_id;
				continuar_ticket();
				$('#unidad').val('');
				$('#comentario_servicio').val('');
				toastr["success"]('Se guardo correctamente el servicio', "Guardado");
			}).catch(error => {
				toastr["error"]('No se pudo guardar el servicio', "Error");
			}).finally(()=>{
				$('#nuevo_servicio_loader').addClass('hide');
				$('#nuevo_servicio_text').removeClass('hide');
				$('button#nuevo_servicio_boton').prop('disabled', false);
			});
		}else{
			toastr["error"]('Favor de capturar los campos necesarios', "Error");
		}
	}else{
		toastr["error"]('No se selecciono un ticket', "Error");
	}
}

function eliminar_servicio(id){
	let ticket_id = $_GET('ticket');
	if(id){
		$('#loader_'+id).removeClass('hide');
		$('#text_' + id).addClass('hide');
		ajax('ticket_detalle/' + ticket_id + '/' + id,'DELETE').then(res => {
			$('#no_servicios').html(parseInt($('#no_servicios').html()) -1 );
			let monto_actual = $('#total_ticket').html();
			monto_actual = monto_actual.replaceAll(['$',','],['','']);
			let monto_servicio = $('#servicio_' + id + ' .item-price .title').html();
			monto_servicio = monto_servicio.replaceAll(['$',','],['','']);
			$('#total_ticket').html(format_currency(parseInt(monto_actual) - parseInt(monto_servicio)));
			$('#servicio_' + id).remove();
			toastr["success"]("Se borro correctamente el servicio", "Borrar servicio");
		}).catch(error => {
			console.log(error);
			toastr["error"](error.responseJSON, "Borrar servicio");
			$('#loader_'+id).addClass('hide');
			$('#text_' + id).removeClass('hide');
		});
	}else{

	}
}

function terminar_ticket(){
	var ticket = JSON.parse(localStorage._tk_detalle);
	if(ticket.ticket_id != undefined && ticket.ticket_id > 0){
		ajax('ticket_terminado', 'POST', {'ticket_id': ticket.ticket_id}).then(data => {
			console.log(data);
			toastr["success"]('El ticket ha sido concluido con exito', "Terminar ticket");
			// Limpiar y prepara para nuevo ticket
			ticket_id = ticket.ticket_id;
			continuar_ticket();
		}).catch(error => {
			toastr["error"](error, "Terminar ticket");
		});
	}else{
		toastr["error"]('No se pudo terminar el ticket, favor de recargar la pagina', "Terminar ticket");
	}
}

function guardar_comentaro(){
	var datos = JSON.parse(localStorage._tk_detalle);
	datos.comentario = $('#comentario_ticket').val();
	if(datos.comentario != ''){
		$('button#btn-comentar').prop('disabled', true);
		$('#btn-comentar i').removeClass('hide');
		$('#btn-comentar span').addClass('hide');
		ajax('ticket_comentario','POST',datos).then(data => {
			toastr["success"]("Se agrego el comentario correctamente", "Comentar ticket");
			$('#comentario_ticket').val('');
			mostrar_comentarios();
		}).catch(error => {
			toastr["error"]('No se pudo guardar el comentario', "Comentar ticket");
		}).finally(()=>{
			$('#btn-comentar i').addClass('hide');
			$('#btn-comentar span').removeClass('hide');
			$('button#btn-comentar').prop('disabled', false);
		});
	}else{
		toastr["warning"]('Favor de capturar un comentario', "Comentar ticket");
	}
}

function mostrar_comentarios(){
	var datos = JSON.parse(localStorage._tk_detalle);
	$('#comentarios_generales section').css('filter','blur(1.5px)');
	$('#comments_loader').removeClass('hide');
	ajax('ticket_comentario/' + datos.ticket_id,'GET').then(data => {
		if(data){
			console.log(data);
			$html = [];
			data.comentarios.forEach(comentario => {
				$html.push(`<div class="event">
						<div class="content">
							<div class="date">
								${beautifulTimeElapsed(comentario.created_at,'FHL-MX')}
							</div>
							<div class="summary">
								<a>${comentario.usuario.empleado.nombre.capitalize(true) +' '+
										comentario.usuario.empleado.ap_paterno.capitalize(true)}</a> ha comentado
							</div>
							<div class="extra text">
								${comentario.comentario}
							</div>
						</div>
					</div>
					</br>`);
			});
			$html.push(`<div class="event">
				<div class="content">
					<div class="date">
						${beautifulTimeElapsed(data.usuario[0].updated_at,'FHL-MX')}
					</div>
					<div class="summary">
						<a>${data.usuario[0].empleado.nombre.capitalize(true) +' '+
						data.usuario[0].empleado.ap_paterno.capitalize(true)}</a> ha cerrado el ticket
					</div>
				</div>
			</div>
			</br>`);
			$('#comentarios_generales .ui.feed').html($html.join(''));
		}
		console.log(data);
		// Solo falta imprimir los comentarios y es tocho, bueno falta lo de si se va a usar el estatus pagado
	}).catch(error => {
		console.log(error);
	}).finally(()=>{
		$('#comentarios_generales section').css('filter','blur(0px)');
		$('#comments_loader').addClass('hide');
	});
}


function prueba_correo(){
	ajax('send_mail','POST').then(data => {
		console.log(data);
	});
}
