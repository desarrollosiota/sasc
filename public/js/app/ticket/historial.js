"use strict";

var ajax_tickets = '';

$(document).ready(function ($) {
	// Tickets
	obtenerTickets();
	// Cientes
	ajax('cliente', 'GET').then((clientes) => {
		let tpl = `<option value="-1">Todos los clientes</option>`;
		clientes.forEach(cliente => {
			tpl += `<option value="${cliente.id}">${cliente.nombre}</option>`;
		});
		$('select#cliente_id').html(tpl);
		$("select#cliente_id").select2({ 'placeholder': 'Seleccione Cliente', theme: "foundation", openOnEnter: false });
	}).catch((e) => {
		toastr["error"]('No se pudo cargar la lista de clientes, favor de recargar la pagina', "Error");
	});
	// Select status
	$("select#cliente_id").select2({ 'placeholder': 'Seleccione Cliente', theme: "foundation", openOnEnter: false });
	$("select#status").select2({ 'placeholder': 'Seleccione un status', theme: "foundation", openOnEnter: false });
	// Fechas
	let d = new Date();
	let mes = ((d.getMonth() + 1) < 10) ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
	let dia = ((d.getDate()) < 10) ? "0" + (d.getDate()) : (d.getDate());
	$('input#fecha_final').val(`${d.getFullYear()}-${mes}-${dia}`);
	d.setMonth(d.getMonth() - 1);
	mes = ((d.getMonth() + 1) < 10) ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
	dia = ((d.getDate()) < 10) ? "0" + (d.getDate()) : (d.getDate());
	$('input#fecha_inicial').val(`${d.getFullYear()}-${mes}-${dia}`);
	// Eventos
	// $(document).on('change', 'select', function() {
	// 	let filtros = obtenerValoresFiltros();
	// 	obtenerTickets(filtros);
	// });
	// $(document).on('change', 'input', function () {
	// 	let filtros = obtenerValoresFiltros();
	// 	obtenerTickets(filtros);
	// });
});

function obtenerTickets() {
	let filtros = obtenerValoresFiltros();
	$('tbody#spiner_tabla_tickets').removeClass('hide');
	$('tbody#tickets').addClass('hide');
	ajax('ticket' + filtros, 'GET', {}, 'ticket').then((tickets) => {
		let tpl = tplTablaTickets(tickets);
		$('tbody#tickets').html(tpl);
		$('tbody#spiner_tabla_tickets').addClass('hide');
		$('tbody#tickets').removeClass('hide');
	}).catch((e) => {
		console.warn(e);
		$('tbody#spiner_tabla_tickets').addClass('hide');
		$('tbody#tickets').removeClass('hide');
	});
}

function obtenerValoresFiltros() {
	let filtros = $('form#filtros').serializeArray();
	let get = '';
	filtros.forEach((filtro) => {
		if (filtro.value == '' || filtro.value == '-1') return;
		let operador = (get == '') ? '?' : '&';
		get += operador + filtro.name + '=' + filtro.value;
	});
	return get;
}

function tplTablaTickets(tickets) {
	let tpl = '';
	tickets.forEach((ticket) => {

		// let total_monto = 0;
		let total_servicios = ticket.tickets_detalle.length;

		// ticket.tickets_detalle.forEach((detalle) => {
		// 	total_monto += parseFloat(detalle.precio);
		// });
		
		let status = (ticket.status == 0) ? '#FFB300' : (ticket.status == 1 || ticket.status == 3) ? '#006BBB' : '#DB3A2C';
		let status_texto = (ticket.status == 0) ? 'Abierto' : (ticket.status == 1 || ticket.status == 3) ? 'Terminado' : 'Cancelado';
		let terminado = (ticket.status == 0) ? 'N/A' : beautifulTimeElapsed(ticket.updated_at);

		tpl += `
			<tr>
				<td onclick="window.location = '/operacion/servicios/nuevo?ticket=${ticket.id}'" data-label="No. Ticket" style="border-left: solid 5px ${status};cursor:pointer;">
					<a href="#">${ticket.id}</a>
				</td>
				
				<td data-label="Status">${status_texto}</td>
				<td data-label="Cliente">${ticket.cliente.nombre}</td>
				<td data-label="Comentario">${ticket.comentarios}</td>
				<td data-label="Servicios">${total_servicios}</td>
				
				<td data-label="Creado">${beautifulTimeElapsed(ticket.created_at)}</td>
				<td data-label="Terminado">${terminado}</td>
			</tr>
		`;
		// <td data-label="Total">${format_currency(total_monto)}</td>
	});
	if (tpl == '') tpl = `<tr><td colspan="8"><center>No hay resultados</center></td></tr>`;
	return tpl;

}
