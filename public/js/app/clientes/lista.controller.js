"use strict";

window.onload = function() {
	buscar_estados();
	$(document).on('submit', 'form#frm_editar_cliente', (e) => {
		e.preventDefault();
	});
	/** Evento para buscar municipios despues de seleccionar un estado */
	$(document).on('change', 'select#select_estados', () => {
		buscar_municipios();
	});
	/** Evento para el campo de busqueda */
	$(document).on('keyup', 'input#busqueda_cliente', function(event) {
		try {
			let data = JSON.parse(localStorage._cl_data_clientes);
			// Conseguir la template con el filtro
			$('#spiner_tabla_clientes').removeClass('hide');
			let tpl = tplTablaClientes(data, $(this).val());
			// Render data
			$('tbody#lista_clientes').html(tpl);
			$('#spiner_tabla_clientes').addClass('hide');
			$('.ui.dropdown_ui').dropdown_ui();
		} catch(e) {
			console.warn(e);
		}
	});

	// Obteniendo usuarios
	obtenerClientes();
};

/** Obtiene todos los usuarios */
function obtenerClientes() {
	$('#spiner_tabla_clientes').removeClass('hide');
	$('#lista_clientes').addClass('hide');
    ajax('cliente', 'GET').then((data) => {
		localStorage._cl_data_clientes = JSON.stringify(data);
		let tpl = tplTablaClientes(data);
        // Render de clientes
        $('tbody#lista_clientes').html(tpl);
        $('.ui.dropdown_ui').dropdown_ui();
    }).catch((e) => console.warn(e)).finally((f)=>{
		$('#spiner_tabla_clientes').addClass('hide');
		$('#lista_clientes').removeClass('hide');
    });
}

function tplTablaClientes(clientes, filtro = ''){
	filtro = filtro.trim();
	let tpl = '';
	
	// Recorriendo los usuarios obtenidos
	clientes.forEach((cliente) => {
		// Condicional para filtro de busqueda
		try {
			if (
					cliente.nombre.toLowerCase().indexOf(filtro) > -1 ||
					cliente.telefono.toLowerCase().indexOf(filtro) > -1 ||
					cliente.correo.toLowerCase().indexOf(filtro) > -1 ||
					cliente.direccion.toLowerCase().indexOf(filtro) > -1) {
					tpl += `
						<tr>
							<td data-label="Nombre">${cliente.nombre}</td>
							<td data-label="Teléfono">${cliente.telefono}</td>
							<td data-label="Correo">${cliente.correo}</td>
							<td data-label="Dirección">${cliente.direccion}</td>
							<td data-label="Acciones">
								<div class="ui right pointing dropdown_ui circular blue basic icon button" style="float: none !important;">
									<i class="fas fa-list"></i>
									<div class="menu">
										<div class="header">
											<i class="list large icon"></i>
											Seleccione una opción
										</div>
										<div class="divider"></div>
										<div class="item add_contacto" onclick='window.location.href="/clientes/contactos?id_cliente=${cliente.id}"' title="Revise la información detallada del cliente">
											<i class="fas fa-users" style="color: #1779ba;"></i>  Contactos
										</div>
										<div class="item add_service" onclick='window.location.href="/clientes/servicios?id_cliente=${cliente.id}"' title="Revise la información detallada del cliente">
											<i class="fas fa-truck" style="color: #1779ba;"></i> Asignar servicio
										</div>
										<div class="item" data-toggle="editar_cliente_modal" onclick="obtenerCliente(${cliente.id})" title="Revise la información detallada del cliente">
											<i class="fas fa-pencil-alt" style="color: #1779ba;"></i>  Editar
										</div>
										<div class="item" onclick="eliminarCliente(${cliente.id})" title="Revise la información detallada del cliente">
											<i class="fas fa-trash-alt" style="color: #cc4b37;"></i>  Eliminar
										</div>
									</div>
								</div>
							</td>
						</tr>`;
				}
		} catch (e) {
			console.warn(e);
			return;
		}
	});
	if (tpl == '') {
		tpl += `
			<tr>
				<td colspan="5"><div><center>No existen clientes</center></div></td>
			</tr>
		`;
	}
	return tpl;
}

/** Obtiene un solo usuario por id */
function obtenerCliente(id) {
    // Mostrar loader y ocultar data
	$('div.centrado').show();
    $('#frm_editar_cliente').addClass('invisible');
    $('#spiner_editar_cliente').removeClass('hide');
    ajax('cliente/' + id, 'GET').then((cliente) => {
        console.log(cliente);
        // Checar si se encontro al usuario
        if (Object.keys(cliente).length == 0) {
            alertify.error("Hubo un error al intentar editar el cliente")
            $('div#editar_cliente_modal').foundation('close');
            return 0;
        }
        $('input#nombre').val(cliente.nombre);
        $('input#rfc').val(cliente.rfc);
        $('input#direccion').val(cliente.direccion);
        $('input#telefono').val(cliente.telefono);
        $('input#correo').val(cliente.correo);
        $('input#alias').val(cliente.alias);

        try {
            $('select#select_estados').val(cliente.estado_id);
            buscar_municipios().then((data) => {
                $('select#select_municipios').val(cliente.municipio_id);
            });
        } catch (e) {
            console.log('El usuario no agrego estado ni municipio');
        }

		$('div#botones_editar_cliente').html(`
			<a class="button expanded success" onclick="editarCliente(${cliente.id})" id="boton_guardar">
				<i class="fas fa-spinner fa-spin fa-lg hide"></i>
				<span>Guardar</span>
			</a>
			<a class="button expanded" href="#!" onclick="$('#frm_nuevo_cliente')[0].reset();">Limpiar</a>
		`);

    }).catch((e) => {
        // Cerrar modal en caso de error
        $('div#editar_cliente_modal').foundation('close');
        console.warn(e);
    }).finally((f) => {
        // Ocultar loader
    	$('div.centrado').hide();
    	$('#spiner_editar_cliente').addClass('hide');
        $('#frm_editar_cliente').removeClass('invisible');
    });
}

/** Edita un solo usuario por id */
function editarCliente(id) {
    // Loader y ocultar botones
	$('#boton_guardar>i').removeClass('hide');
	$('#boton_guardar>span').addClass('hide');
    // Obteniendo data del formulario
    let cliente_editado = $('form#frm_editar_cliente').serializeObject();
    console.log(cliente_editado);
    // Enviando data al server
    ajax('cliente/' + id, 'PUT', cliente_editado).then((res) => {
		console.log(res);
		alertify.success("Cliente " + cliente_editado.nombre + " editado");
        // Cerrar modal
        $('div#editar_cliente_modal').foundation('close');
        // Refresh de los usuarios
        obtenerClientes();
    }).catch((e) => {
        // Cerrar modal
        // $('div#editar_cliente_modal').foundation('close');
		let err_txt = (typeof e.responseText == "string") ? e.responseText : 'Error en el servidor';
		alertify.error("No se pudo editar el cliente: " + err_txt);
		console.log(e);
    }).finally((e) => {
        // Loader y botones
		$('#boton_guardar>i').addClass('hide');
		$('#boton_guardar>span').removeClass('hide');
    });
}

/** Elimina un solo usuario por id */
function eliminarCliente(id) {
    alertify.confirm('Necesita Confirmacion', '¿Realmente desea eliminar el usuario?', function(){
	    ajax('cliente/' + id, 'DELETE').then((res) => {
	        if (res) {
    			alertify.success('El cliente fue eliminado con exito');
	            // Refresh de los clientes
	            obtenerClientes();
	        } else {
    			alertify.error('No se pudo eliminar el cliente');
	            console.warn('cliente no encontrado');
	        }
	    }).catch((e) => {
    		alertify.error('No se pudo eliminar el cliente');
	        console.warn(e);
	    });
    }, function() {
		// Cliente no eliminado
	});

}

var buscar_estados = async ()=>{
	var estados = await ajax('estados','POST').catch(e => e);
	var select_datos = [];
	if(Object.keys(estados).length > 0){
		select_datos.push(`<option value="0">Estado</option>`);
		estados.forEach(estado => {
			select_datos.push(`<option value="${estado.id}">${estado.nombre}</option>`);
		});
		$('select#select_estados').html(select_datos.join(''));
	}
}

var buscar_municipios = async ()=>{
	var id_estado = $('#select_estados').val();
	if(id_estado > 0){
		var municipios = await ajax('municipios','POST',{'id_estado':id_estado}).catch(e => e);
		var select_datos = [];
		if(Object.keys(municipios).length > 0){
			municipios.forEach(municipio => {
				select_datos.push(`<option value="${municipio.id}">${municipio.nombre}</option>`);
			});
			$('select#select_municipios').html(select_datos.join(''));
		}
	}else{
		// alertify.error("No se selecciono un estado");
	}
}
