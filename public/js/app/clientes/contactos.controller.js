'use strict'

$(document).ready(function($) {
	obtenerContactos();
	$(document).on('keyup', 'form#frm_nuevo_contacto', function(event) {
		if (event.keyCode == 13) guardar_contacto();
		event.preventDefault();
	});
	$(document).on('keyup', 'form#frm_editar_contacto', function(event) {
		if (event.keyCode == 13) editar_contacto();
		event.preventDefault();
	});
	$(document).on('submit', 'form', function (event) {
		event.preventDefault();
	});
	/** Evento para el campo de busqueda */
	$(document).on('keyup', 'input#busqueda_contacto', function (event) {
		try {
			let data = JSON.parse(localStorage._con_contactos);
			let tpl = tplContactos(data, $(this).val());
			// Render de contactos
			$('tbody#lista_contactos').html(tpl);
		} catch (e) {
			console.warn(e);
		}
	});
});

/** Obtiene todos los usuarios */
async function obtenerContactos() {
	var $id_cliente = $_GET('id_cliente');
	if($id_cliente){
		$('tbody#spiner_tabla_contactos').removeClass('hide');
		$('tbody#lista_contactos').addClass('hide');
		ajax('contactos_get', 'POST', {id_cliente: $id_cliente}).then((data) => {
			$('p#subtitulo_cliente').html('<a href="/clientes" style="color:white;">Cliente: ' + data.nombre + '</a>');
			localStorage._con_contactos = JSON.stringify(data);
			let tpl = tplContactos(data, '');
			// Render de contactos
			$('tbody#lista_contactos').html(tpl);
		}).catch(e=>{
			console.log(e);
		}).finally((f) => {
			$('tbody#spiner_tabla_contactos').addClass('hide');
			$('tbody#lista_contactos').removeClass('hide');
		});
	}else{
		alert('No se selecciono ningun cliente');
	}
}

async function guardar_contacto() {
	if($_GET('id_cliente')){
		$('form#frm_nuevo_contacto').submit();
		var datos_contactos = $('#frm_nuevo_contacto').serializeObject();
		datos_contactos.id_cliente = $_GET('id_cliente');
		// $('#frm_nuevo_contacto').submit();
		$('#btn_guardar_contacto>span').addClass('hide');
		$('#btn_guardar_contacto>i').removeClass('hide');
		ajax('agregar_contacto', 'POST', datos_contactos).then((data) => {
			alertify.success('El contacto '+datos_contactos.nombre + " ha sido creado con exito");
			$('#nuevo_contacto').foundation('close');
			$('#frm_nuevo_contacto')[0].reset();
			obtenerContactos();
		}).catch((err) => {
			let err_txt = (typeof err.responseText == "string") ? err.responseText : 'Error en el servidor';
			alertify.error("No se pudo dar de alta el contacto: " + err_txt)
			console.log(err);
		}).finally((f) => {
			$('#btn_guardar_contacto>span').removeClass('hide');
			$('#btn_guardar_contacto>i').addClass('hide');
		});
	}else{
		alertify.error("No se ha seleccionado un cliente", )
	}
}

async function datos_contacto(id_contacto){
	$('#editar_contacto').foundation('open');
	$('#frm_editar_contacto').addClass('invisible');
	$('#spiner_editar_contacto').removeClass('hide');
	ajax('datos_contacto', 'POST', {id_contacto: id_contacto}).then((data) => {
		if(Object.keys(data).length > 0){
			$('#editar_nombre').val(data.nombre);
			$('#editar_telefono').val(data.telefono);
			$('#editar_correo').val(data.correo);
			$('#editar_puesto').val(data.puesto);
			$('#id_contacto').val(data.id);
		}else{
			$('#editar_contacto').foundation('close');
			alertify.error("No se puede recuperar la inormacion del contacto");
		}
	}).catch((err) => {
		let err_txt = (typeof err.responseText == "string") ? err.responseText : 'Error en el servidor';
		console.log(err);
		alertify.error("No se puede recuperar la inormacion del contacto", )
		$('#editar_contacto').foundation('close');
	}).finally((f) => {
		$('#frm_editar_contacto').removeClass('invisible');
		$('#spiner_editar_contacto').addClass('hide');
	});
}

async function editar_contacto(){
	var datos_contactos = $('#frm_editar_contacto').serializeObject();
	$('#frm_nuevo_contacto').submit();
	$('#btn_editar_contacto>span').addClass('hide');
	$('#btn_editar_contacto>i').removeClass('hide');
	ajax('editar_contacto', 'POST', datos_contactos).then((data) => {

		alertify.success('El contacto: '+datos_contactos.nombre + " ha sido editado") ;
		$('#editar_contacto').foundation('close');
		$('#frm_nuevo_contacto')[0].reset();
		obtenerContactos();
	}).catch((err) => {
		let err_txt = (typeof err.responseText == "string") ? err.responseText : 'Error en el servidor';
		alertify.error("No se pudo editar el contacto", )
		console.log(err);
	}).finally((f) => {
		$('#btn_editar_contacto>span').removeClass('hide');
		$('#btn_editar_contacto>i').addClass('hide');
	});
}

async function eliminar_contacto(id_contacto){
	alertify.confirm('Necesita Confirmacion', '¿Realmente desea eliminar el contacto?', function () {
		$('button#eliminar_contacto_' + id_contacto).prop('disabled', true);
		$('button#eliminar_contacto_' + id_contacto).html(`<i class="fas fa-cog fa-lg fa-spin"></i>`);
		ajax('eliminar_contacto', 'POST', {id_contacto}).then((res) => {
			if (res) {
				alertify.success('El contacto fue eliminado con éxito');
				// Refresh de los contactos
				obtenerContactos();
			} else {
				alertify.error('No se pudo eliminar el contacto');
				console.warn('contacto no encontrado');
			}
		}).catch((e) => {
			$('button#eliminar_contacto_' + id_contacto).prop('disabled', false);
			$('button#eliminar_contacto_' + id_contacto).html(`<i class="fas fa-trash-alt fa-lg"></i>`);
			alertify.error('No se pudo eliminar el contacto');
			console.warn(e);
		});
	}, function () {
		// Contacto no eliminado
	});
}

function tplContactos(contactos, filtro) {
	let tpl = '';
	contactos.contactos.forEach((contacto) => {

		try {
			if (
				contacto.nombre.toLowerCase().indexOf(filtro) > -1 ||
				contacto.telefono.toLowerCase().indexOf(filtro) > -1 ||
				contacto.correo.toLowerCase().indexOf(filtro) > -1 ||
				contacto.puesto.toLowerCase().indexOf(filtro) > -1) {
					tpl += `
						<tr>
							<td data-label="Nombre">${contacto.nombre}</td>
							<td data-label="Teléfono">${contacto.telefono}</td>
							<td data-label="Correo">${contacto.correo}</td>
							<td data-label="Puesto">${contacto.puesto}</td>
							<td data-label="Acciones">
								<button class="clear button" onclick='datos_contacto(${contacto.id});' onclick=""><i class="fas fa-pencil-alt fa-lg"></i></button>
								<button class="clear button alert" onclick="eliminar_contacto(${contacto.id});" id="eliminar_contacto_${contacto.id}"><i class="fas fa-trash-alt fa-lg"></i></button>
							</td>
						</tr>
					`;
				}
		} catch (error) {
			console.warn(error);
			return;
		}
	});
	if (tpl == '') tpl = '<tr><td colspan="5">No hay contactos</td></tr>';
	return tpl;
}