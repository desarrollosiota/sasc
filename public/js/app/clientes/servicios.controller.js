var $id_cliente = 0;

$(document).ready(function($) {

	$id_cliente = $_GET('id_cliente');
	obtenerServicios();
	buscar_servicios();

	// Evento que se corre cada que se abre el modal, busca e inicializa el select de los servicios
	$(document).on('open.zf.reveal', '#nuevo_servicio', function(event) {
		if(cargarSelect2('select#servicio')){
			$('#costo').val('');
			$('#comentario').val('');
		}else{	
			buscar_servicios();
			$('#nuevo_servicio').trigger('open.zf.reveal');
		}
	});
	// 
	// $(document).on('keyup', 'form#frm_nuevo_servicio', function (event) {
	// 	if (event.keyCode == 13) guardar_contacto();
	// 	event.preventDefault();
	// });
	// $(document).on('keyup', 'form#frm_editar_servicio', function (event) {
	// 	if (event.keyCode == 13) editar_contacto();
	// 	event.preventDefault();
	// });
	$(document).on('submit', 'form', function (event) {
		event.preventDefault();
	});

	$('.select2').on('keypress', function (e) {
		if (e.keyCode === 13) {
			e.preventDefault();
   			e.stopPropagation(); 
		}
	});

	$(document).on('submit', '#frm_editar_servicio', function(event) {
		event.preventDefault();
	});

	$(document).on('keyup', 'input#busqueda_servicio', function(event) {
		let clientes = JSON.parse(localStorage._csc_clientes);
		let tpl = tplListaServicios(clientes, $(this).val());
		// Render de usuarios
		$('tbody#lista_servicios').html(tpl);
	})
});

/** Obtiene todos los usuarios */
function cargarSelect2(objeto, elemento = 0){
	// Busca el catalogo de servicios almacenados en localstorage
	var	servicios = JSON.parse(localStorage.getItem('_servicios'));
	// Se valida si localstorage contenía los datos
	if(Object.keys(servicios).length > 0){
		var sercivios_datos = [];
		sercivios_datos.push(`<option value="">Seleccionar Opción</option>`);
		let csc_clientes = JSON.parse(localStorage._csc_clientes);
		servicios.forEach(servicio => {
			let encontrado = false;
			csc_clientes.servicios.forEach((csc_servicio) => {
				if (csc_servicio.id == servicio.id) encontrado = true;
			});
			if (!encontrado) {
				if (servicio.id == elemento) {
					sercivios_datos.push(`<option selected='selected' value="${servicio.id}">${servicio.nombre.capitalize(true)}</option>`);
				} else {
					sercivios_datos.push(`<option value="${servicio.id}">${servicio.nombre.capitalize(true)}</option>`);
				}
			}
		});
		$(objeto).html(sercivios_datos.join(''));
		$(objeto).select2({'placeholder':'Seleccionar Opción', theme: "foundation"});
		return true;
	}
	return false;
}

/** Obtiene todos los usuarios */
function obtenerServicios() {
	if($id_cliente){
		$('tbody#spiner_tabla_servicios').removeClass('hide');
		$('tbody#lista_servicios').addClass('hide');
		var tpl = '';
	    ajax('cliente_servicio_costo/'+$id_cliente, 'GET').then((cliente) => {
			$('p#subtitulo_cliente').html('<a href="/clientes" style="color:white;">Cliente: ' + cliente.nombre + '</a>');
			localStorage._csc_clientes = JSON.stringify(cliente);
			tpl = tplListaServicios(cliente);
	        // Render de usuarios
	        $('tbody#lista_servicios').html(tpl);
	    }).catch(e=>{
	    	console.log(e);
	    }).finally((f) => {
			$('tbody#spiner_tabla_servicios').addClass('hide');
			$('tbody#lista_servicios').removeClass('hide');
			if (tpl == '') {
				tpl += `
					<tr>
						<td colspan="5"><div><center>No existen usuarios</center></div></td>
					</tr>
				`;
			}
		});
	}else{
		alert('No se selecciono ningun servicio');
	}
}

/** Obtiene todos los usuarios */
function datos_editar_servicio(id_servicio){
	// Se abre el modal
	$('#editar_servicio').foundation('open');
	// Se muestra el loader
	$('#frm_editar_servicio').addClass('invisible');
	$('#spiner_editar_servicio').removeClass('hide');
	// Se enviá el ajax
	ajax('buscarServiciosDatos', 'POST', {'servicio_id': id_servicio, 'cliente_id': $_GET('id_cliente')}).then(data => {
		if(Object.keys(data).length > 0){
			$('#id_servicio').val(data.servicios[0].id);
			cargarSelect2('select#servicio_editar', data.servicios[0].pivot.servicio_id);
			$('#costo_editar').val(data.servicios[0].pivot.costo);
			$('#comentario_editar').val(data.servicios[0].pivot.descripcion);
		}else{
			$('#editar_servicio').foundation('close');
			alertify.error("No se puede recuperar la inormacion del contacto");
		}
	}).catch((err) => {
		let err_txt = (typeof err.responseText == "string") ? err.responseText : 'Error en el servidor';
		console.log(err);
		alertify.error("No se puede editar la inormacion del servicio")
		$('#editar_servicio').foundation('close');
	}).finally((f) => {
		// Oculta el loader sin importar la respuesta
		$('#frm_editar_servicio').removeClass('invisible');
		$('#spiner_editar_servicio').addClass('hide');
	});
}

async function guardar_servicio(){
	if($_GET('id_cliente')){
		var datos_sevicio = $('#frm_editar_servicio').serializeObject();
		$('#frm_editar_servicio').submit();
		$('#btn_editar_servicio>span').addClass('hide').prop('disabled', true);
		$('#btn_editar_servicio>i').removeClass('hide');
		ajax('cliente_servicio_costo/'+$_GET('id_cliente'), 'PUT', datos_sevicio).then((data) => {
			alertify.success('El precio del servicio se actualizo con exito');
			$('#editar_servicio').foundation('close');
			$('#frm_editar_servicio')[0].reset();
			obtenerServicios();
		}).catch((err) => {
			let err_txt = (typeof err.responseText == "string") ? err.responseText : 'Error en el servidor';
			alertify.error("No se pudo editar el ervicio")
			console.log(err);
		}).finally((f) => {
			$('#btn_editar_servicio>span').removeClass('hide').prop('disabled', false);
			$('#btn_editar_servicio>i').addClass('hide');
		});
	}else{
		alertify.error("No se ha seleccionado un cliente")
	}
}

/** Obtiene todos los usuarios */
async function buscar_servicios(){
	try{
		var servicios = await ajax('catalogoServicios','POST');
		if(Object.keys(servicios).length > 0){
			localStorage.setItem('_servicios', JSON.stringify(servicios));
		}else{
			alert('No se pudieron recuperar los servicios, favor de recargar la pagina');	
		}
	}catch(err){
		alert('Ocurrio un error al buscar los servicios');
	}
}

/** Obtiene todos los usuarios */
async function guardar_servicio_costo(){
	var datos = {
		cliente_id: parseInt($id_cliente),
		servicio_id: $('#servicio').val(),
		costo: $('#costo').val(),
		descripcion: $('#comentario').val()}
	if(datos.servicio_id && datos.costo && datos.cliente_id){
		try{
			var respuesta = await ajax('cliente_servicio_costo','POST',datos);
			alertify.success('Se creo correctamente la relación');
			$('#nuevo_servicio').foundation('close');
			obtenerServicios();
		}catch(err){
			console.log(err);
			alertify.error('No se pudo guardar el servicio del cliente');
		}
	}else{
		alertify.error('Favor de seleccionar un servicio y capturar el costo');
	}
}

/** Elimina un solo cliente por id */
function eliminarCliente(id) {
	if ($_GET('id_cliente')) {
		alertify.confirm('Necesita Confirmacion', '¿Realmente desea eliminar el cliente?', function(){
			$('button#eliminar_cliente_' + id).prop('disabled', true);
			$('button#eliminar_cliente_' + id).html(`<i class="fas fa-cog fa-lg fa-spin"></i>`);
			ajax('cliente_servicio_costo/' + $_GET('id_cliente'), 'DELETE', {id}).then((res) => {
				if (res) {
					alertify.success('El cliente fue eliminado con exito');
					// alertify.success("Cliente eliminado");
					// Refresh de los cliente
					obtenerServicios();
				} else {
					alertify.error('No se pudo eliminar el cliente');
					console.warn('Cliente no encontrado');
				}
				console.log(res);
				
			}).catch((e) => {
				$('button#eliminar_cliente_' + id).prop('disabled', false);
				$('button#eliminar_cliente_' + id).html(`<i class="fas fa-trash-alt fa-lg"></i>`);
				alertify.error('No se pudo eliminar el cliente');
				console.warn(e);
			});
		},function(){
			
		});
	} else {
		alertify.error('No se ha seleccionado un cliente');
	}
}

function tplListaServicios(servicios, filtro = '') {
	let tpl = '';
	filtro = filtro.trim();
	servicios.servicios.forEach((servicio) => {
		try {
			console.log(servicio);
			servicio.pivot.costo = format_currency(servicio.pivot.costo);
			if (
				servicio.nombre.toLowerCase().indexOf(filtro) > -1 ||
				servicio.pivot.costo.toLowerCase().indexOf(filtro) > -1 ||
				servicio.pivot.descripcion.toLowerCase().indexOf(filtro) > -1) {
					tpl += `
						<tr>
							<td data-label="Servicio">${servicio.nombre.capitalize(true)}</td>
							<td data-label="Costo">${servicio.pivot.costo}</td>
							<td data-label="Comentarios">${servicio.pivot.descripcion}</td>
							<td data-label="Acciones">
								<button class="clear button" onclick='datos_editar_servicio(${servicio.pivot.servicio_id});'><i class="fas fa-pencil-alt fa-lg"></i></button>
								<button class="clear button alert" id="eliminar_cliente_${servicio.pivot.servicio_id}" onclick="eliminarCliente(${servicio.pivot.servicio_id});"><i class="fas fa-trash-alt fa-lg"></i></button>
							</td>
						</tr>
					`;
				}
		} catch (error) {
			console.warn(error);
			return;
		}

	});
	if (tpl == '') tpl = `<tr><td colspan="4">No hay servicios para mostrar</td></tr>`;
	return tpl;
}

// let encontrado = false;
					// data.tickets_detalle.forEach((ticket_detalle) => {
					// 	if (ticket_detalle.servicios_costo.servicio_id == servicio.pivot.servicio_id) {
					// 		encontrado = true;
					// 	}
					// });
					// if (!encontrado) {
					// }