"use strict";

window.onload = function() {
	buscar_estados();

	$(document).on('change', 'select#select_estados', () => {
		buscar_municipios();
	});

	$(document).on('keypress', 'form#frm_nuevo_cliente', (e) => {
		if (e.keyCode == 13) $('form#frm_nuevo_cliente').submit();
	});
	$(document).on('submit', 'form#frm_nuevo_cliente', (e) => {
		guardar_cliente();
		e.preventDefault();
	});
};

var guardar_cliente = ()=>{
	$('#btn_nuevo_usuario>span').addClass('hide');
	$('#btn_nuevo_usuario>i').removeClass('hide');
	// Obteniendo los datos del form	
	let data_cliente = $('form#frm_nuevo_cliente').serializeObject();
	// Enviar ajax
	ajax('cliente', 'POST', data_cliente).then((data) => {
		alertify.success('El Cliente: '+data_cliente.nombre + " ha sido creado con exito");
		// Borrar formulario
		limpiar_nuevo_cliente();
	}).catch((err) => {
		// Ajax retorna 400
		let err_txt = (typeof err.responseText == "string") ? err.responseText : 'Error en el servidor';
		alertify.error("No se pudo dar de alta el cliente: " + err_txt);
		console.log(err);
	}).finally((f) => {
		$('#btn_nuevo_usuario>span').removeClass('hide');
		$('#btn_nuevo_usuario>i').addClass('hide');
	});
}

var buscar_estados = async ()=>{
	var estados = await ajax('estados','POST').catch(e => e);
	var select_datos = [];
	if(Object.keys(estados).length > 0){
		select_datos.push(`<option value="">Seleccionar Opción</option>`);
		estados.forEach(estado => {
			select_datos.push(`<option value="${estado.id}">${estado.nombre}</option>`);
		});
		$('#select_estados').html(select_datos.join(''));
		$("#select_estados").select2({'placeholder':'Seleccionar Opción', theme: "foundation", dropdownParent: $('body')});

	}
}

var buscar_municipios = async ()=>{
	var id_estado = $('#select_estados').val();
	if(id_estado > 0){
		var municipios = await ajax('municipios','POST',{'id_estado':id_estado}).catch(e => e);
		var select_datos = [];
		if(Object.keys(municipios).length > 0){
		select_datos.push(`<option value="">Seleccionar Opción</option>`);
			municipios.forEach(municipio => {
				select_datos.push(`<option value="${municipio.id}">${municipio.nombre}</option>`);
			});
			$('#select_municipios').html(select_datos.join(''));
			$("#select_municipios").select2({'placeholder':'Seleccionar Opción', theme: "foundation"});
			$("#select_municipios").prop({disabled: '',});
		}
	}else{
		// alertify.error("No se selecciono un estado");
	}
}

var limpiar_nuevo_cliente = ()=>{
	$('#frm_nuevo_cliente')[0].reset();
	$('#select_estados').val(null).trigger('change');
	$('#select_municipios').val(null).trigger('change');
}
