"use strict";

var ticket_id = document.URL.split("/").pop();

console.log('el jefe');

continuar_ticket();

function continuar_ticket() {
	var ticket = ticket_id;
	if (ticket) {
		ajax('ticket/' + ticket, 'GET').then(data => {
			$('button#btn-terminado').addClass('hide');
			$('button#btn-cancelado').addClass('hide');
			$('button#btn-abierto').addClass('hide');
			$('button#btn-pagar').addClass('hide');
			$('button#btn-pagado').addClass('hide');

			$('#res_ticket').html('T-0000' + data.id);
			$('#res_cliente').html(data.cliente.nombre);
			$('#res_comentario').html(data.comentarios);
			$('#res_fecha').html(beautifulTimeElapsed(data.created_at));
			$('#nuevo_ticket').addClass('hide');
			$('#spinner_page').addClass('hide');
			$('#dashboard').removeClass('hide');
			console.log(data);
			if (data.cliente.servicios != undefined) {
				var HTML = [`<option value="">Seleccione un servicio</option>`];
				data.cliente.servicios.forEach(servicio => {
					HTML.push(`<option value="${servicio.pivot.id}">${servicio.nombre}</option>`);
				});
				$('#select_servicio').html(HTML.join(''));
				$("#select_servicio").select2({ 'placeholder': 'Seleccione un servicio', theme: "foundation", openOnEnter: false });
			}
			let tpl_tabla_detalles = '';
			let total_ticket = 0;
			let no_servicios = 0;
			data.tickets_detalle.forEach((detalle) => {
				total_ticket += parseInt(detalle.precio);
				no_servicios++;
				tpl_tabla_detalles += `
					<div class="checkout-summary-item" id='servicio_${detalle.id}'>
						<div class="item-name">
							<p>${detalle.servicios_costo.servicio.nombre}</p>
							<p><span class="title">Unidad: </span>${detalle.unidad_id}</p>
							<p><h6 class="subheader">${beautifulTimeElapsed(detalle.created_at, 'FHL-MX')}</h6></p>
						</div>
						<div class="item-price">
							<p class="title">${format_currency(detalle.precio)}</p>
						</div>
					</div>`
			});
			$('#no_servicios').html(no_servicios);
			$('#total_ticket').html(format_currency(total_ticket));
			$('#items').html(tpl_tabla_detalles);
			localStorage.setItem('_tk_detalle', JSON.stringify({ ticket_id: data.id, terminado: data.updated_at }));
			if (data.status == 0) {
				$('button#btn-abierto').removeClass('hide');

				// $('div#div_comentarios').removeClass('hide');
				// mostrar_comentarios();
			} else if (data.status == 1) {
				$('button#btn-terminado').removeClass('hide');
				$('button#btn-pagar').removeClass('hide');
				
				$('div#div_comentarios').removeClass('hide');
				mostrar_comentarios();
			} else if (data.status == 2) {
				$('button#btn-cancelado').removeClass('hide');
				
			} else if (data.status == 3) {
				$('button#btn-pagado').removeClass('hide');
				
				$('div#div_comentarios').removeClass('hide');
				mostrar_comentarios();
			}
			
		}).catch(error => {
			console.log("error", error);
		});
	} else {
		toastr["error"]("No se definió un ticket", "Error");
	}
};

function pagarTicket() {
	var ticket = ticket_id;
	if (ticket) {
		ajax('pagar_ticket', 'POST', { ticket_id: ticket }).then(data => {
			console.log(data);
			toastr["success"]('El ticket ha sido almacenado como pagado', "Terminar ticket");
			// Limpiar y prepara para nuevo ticket
			ticket_id = ticket;
			continuar_ticket();
		}).catch(error => {
			toastr["error"](error, "Terminar ticket: " + error.responseText);
		});
	} else {
		toastr["error"]('No se pudo terminar el ticket, favor de recargar la página', "Terminar ticket");
	}
}

function guardar_comentaro() {
	var datos = JSON.parse(localStorage._tk_detalle);
	datos.comentario = $('#comentario_ticket').val();
	if (datos.comentario != '') {
		$('button#btn-comentar').prop('disabled', true);
		$('#btn-comentar i').removeClass('hide');
		$('#btn-comentar span').addClass('hide');
		ajax('ticket_comentario', 'POST', datos).then(data => {
			toastr["success"]("Se agrego el comentario correctamente", "Comentar ticket");
			$('#comentario_ticket').val('');
			mostrar_comentarios();
		}).catch(error => {
			toastr["error"]('No se pudo guardar el comentario', "Comentar ticket");
		}).finally(() => {
			$('#btn-comentar i').addClass('hide');
			$('#btn-comentar span').removeClass('hide');
			$('button#btn-comentar').prop('disabled', false);
		});
	} else {
		toastr["warning"]('Favor de capturar un comentario', "Comentar ticket");
	}
}

function mostrar_comentarios() {
	var datos = JSON.parse(localStorage._tk_detalle);
	$('#comentarios_generales section').css('filter', 'blur(1.5px)');
	$('#comments_loader').removeClass('hide');
	ajax('ticket_comentario/' + datos.ticket_id, 'GET').then(data => {
		if (data) {
			// console.log(data);
			let $html = [];
			data.comentarios.forEach(comentario => {
				$html.push(`<div class="event">
						<div class="content">
							<div class="date">
								${beautifulTimeElapsed(comentario.created_at, 'FHL-MX')}
							</div>
							<div class="summary">
								<a>${comentario.usuario.empleado.nombre.capitalize(true) + ' ' +
					comentario.usuario.empleado.ap_paterno.capitalize(true)}</a> ha comentado
							</div>
							<div class="extra text">
								${comentario.comentario}
							</div>
						</div>
					</div>
					</br>`);
			});
			$html.push(`<div class="event">
				<div class="content">
					<div class="date">
						${beautifulTimeElapsed(data.usuario[0].updated_at, 'FHL-MX')}
					</div>
					<div class="summary">
						<a>${data.usuario[0].empleado.nombre.capitalize(true) + ' ' +
				data.usuario[0].empleado.ap_paterno.capitalize(true)}</a> ha cerrado el ticket
					</div>
				</div>
			</div>
			</br>`);
			$('#comentarios_generales .ui.feed').html($html.join(''));
			// console.log($html);
		}
		// console.log(data);
		// Solo falta imprimir los comentarios y es tocho, bueno falta lo de si se va a usar el estatus pagado
	}).catch(error => {
		console.log(error);
	}).finally(() => {
		$('#comentarios_generales section').css('filter', 'blur(0px)');
		$('#comments_loader').addClass('hide');
	});
}
