"use strict";
var catalogo = 'Servicio';

$(document).ready(function ($) {
	// Bootear catalogos
	obtenerCatalogos().then(() => {
		renderCatalogos();
	}).catch((e) => {
		// Poner mensaje de error para el usuario
		console.warn(e);
	}).finally((f) => {
		// Quitar loaders
		$('div#catalogos_cargando').hide();
		$('div#catalogos_contenido').toggle(400);
	});

	// Evento para tecla enter en campos de editar
	$(document).on('keyup', 'input.editar_evento', function(e) {
		if (e.keyCode == 13) {
			let data = $(this).data();
			confirmarEditarElemento(data.id, data.prefijo, data.elem_id);
		}
	});
	
	// Evento para tecla enter en campos de guardar
	$(document).on('keyup', 'input.nuevo_evento', function(e) {
		if (e.keyCode == 13) guardarConcepto();
	});
	
	$(document).on('click', 'input#a_modal_nuevo_concepto', function(){
		if (localStorage._cat_nuevo_prefijo != catalogo) {
			$('input#input_nuevo_nombre').val('');
			$('input#input_nuevo_descripcion').val('');
		}
		localStorage._cat_nuevo_prefijo = catalogo;
		$('h1#header_nuevo_concepto').html(`Crear Nuevo ${catalogo.replace('_', ' ')}`);
	});
	
	// Evento para el form de nuevo
	$(document).on('submit', 'form#form_nuevo', function (event) {
		event.preventDefault();
	});
	// $(document).on('click', '.tabs-title', function(event) {
	// 	console.log($('.tabs-title.is_active').attr('catalogo'));
	// 	console.log(event);
	/** Evento para el campo de busqueda */
	$(document).on('keyup', 'input#busqueda_catalogo', function (event) {
		let filtro = $(this).val();
		try {
			// Servicios
			let tpl1 = tplListadoCatalogos(JSON.parse(localStorage._cat_catalogos).servicio, 'servicio', filtro);
			$('div#panel_servicios_contenido').html(tpl1);
			//Puestos
			let tpl2 = tplListadoCatalogos(JSON.parse(localStorage._cat_catalogos).puesto, 'puesto', filtro);
			$('div#panel_puestos_contenido').html(tpl2);
			// Estado civil
			let tpl3 = tplListadoCatalogos(JSON.parse(localStorage._cat_catalogos).estado_civil, 'estado_civil', filtro);
			$('div#panel_estados_civiles_contenido').html(tpl3);
		} catch (e) {
			console.warn(e);
		}
	});
});

/** Funciones de rendereado de catalogos */
const renderServicios = function () {
	let tpl = tplListadoCatalogos(JSON.parse(localStorage._cat_catalogos).servicio, 'servicio');
	$('div#panel_servicios_contenido').html(tpl);
}
const renderPuestos = function () {
	let tpl = tplListadoCatalogos(JSON.parse(localStorage._cat_catalogos).puesto, 'puesto');
	$('div#panel_puestos_contenido').html(tpl);
}
const renderEstadosCiviles = function () {
	let tpl = tplListadoCatalogos(JSON.parse(localStorage._cat_catalogos).estado_civil, 'estado_civil');
	$('div#panel_estados_civiles_contenido').html(tpl);
}
const renderCatalogos = function () {
	renderServicios();
	renderPuestos();
	renderEstadosCiviles();
}
/** Funciones de rendereado de catalogos */

/** Ajax para los catalogos */
const obtenerCatalogos = async function () {
	// Obtener servicios
	return await ajax('catalogos', 'GET').then((catalogos) => {
		// Data
		localStorage._cat_catalogos = JSON.stringify(catalogos);
	}).catch((err) => {
		// Ajax retorna 400
		console.warn(err);
	});
}

/** Cambia los p del elemento seleccionado por inputs */
const guardarConcepto = function () {
	let prefijo = localStorage._cat_nuevo_prefijo.toLowerCase();
	$('form#form_nuevo').submit();
	let data = $('form#form_nuevo').serializeObject();
	if (data.nombre == "") return;
	ajax(prefijo, 'POST', data).then((res) => {
		// Hay que reestablecer la data del objeto
		let catalogos = JSON.parse(localStorage._cat_catalogos);
		catalogos[prefijo].push(res);
		localStorage._cat_catalogos = JSON.stringify(catalogos);
		alertify.success("Nuevo concepto agregado");
		$('div#modal_nuevo_concepto').foundation('close');
		$('input#input_nuevo_nombre').val('');
		$('input#input_nuevo_descripcion').val('');
		renderCatalogos();
	}).catch((err) => {
		console.warn(err);
		alertify.error("Error en el servidor: " + err.responseText)
	});
}

/** Cambia los p del elemento seleccionado por inputs */
const editarElemento = function(id, prefijo, elem_id) {
	renderCatalogos();
	$("p#" + id + "_nombre").replaceWith(function () {
		let texto = ($(this).text() == 'null') ? '' : $(this).text();
		return $("<input class='editar_evento' data-id='"+ id +"' data-prefijo='"+ prefijo +"' data-elem_id="+ elem_id +" type='text' value='" + texto + "' id='" + id + "_nombre' placeholder='Define un valor' />", {}).append($(this).contents());
	});
	$("p#" + id + "_descripcion").replaceWith(function () {
		let texto = ($(this).text() == 'null') ? '' : $(this).text();
		return $("<input class='editar_evento' data-id='"+ id +"' data-prefijo='"+ prefijo +"' data-elem_id="+ elem_id +" type='text' value='" + texto + "' id='" + id + "_descripcion' placeholder='Define un valor' />", {}).append($(this).contents());
	});
	$("td#" + id + "_editar").html(`
					<button class="clear button" onclick="confirmarEditarElemento('${id}', '${prefijo}', ${elem_id})">
						<div id="${id + '_confirmar_editar'}" style="color:green;">
							<i class="fas fa-check fa-lg"></i>
						</div>
					</button>
	`);
	$("td#" + id + "_eliminar").html(`
					<button class="clear button" onclick="renderCatalogos()">
						<div " id="${id + '_cancelar_editar'}">
							<i class="fas fa-arrow-left fa-lg"></i>
						</div>
					</button>
	`);
}

/** Pregunta si quieres eliminar el elemento seleccionado */
const eliminarElemento = function (prefijo, elem_id, id) {
	alertify.confirm('Necesita Confirmacion', '¿Realmente desea eliminar el ' + prefijo.replace('_', ' ') + '?', function () {
		$('td#' + prefijo + '_' + id + '_eliminar>button').prop('disabled', true);
		$('td#' + prefijo + '_' + id + '_eliminar>button>div').html(`<i class="fas fa-cog fa-lg fa-spin"></i>`);

		ajax(prefijo + '/' + elem_id, 'DELETE').then((res) => {
			if (res != 1 || res != '1') throw { responseText: 'No se pudo eliminar este servicio'};
			let catalogos = JSON.parse(localStorage._cat_catalogos);
			catalogos[prefijo].forEach((cat, i) => {
				if (cat.id != elem_id) return;
				catalogos[prefijo].splice(i, 1);
			});
			localStorage._cat_catalogos = JSON.stringify(catalogos);
			alertify.success("Concepto eliminado")
			renderCatalogos();
		}).catch((err) => {
			$('td#' + prefijo + '_' + id + '_eliminar>button').prop('disabled', false);
			$('td#' + prefijo + '_' + id + '_eliminar>button>div').html(`<i class="fas fa-trash-alt fa-lg"></i>`);
			console.warn(err);
			alertify.error("Error en el servidor: " + err.responseText)
		});
	}, function() {
		// Nada
	});
}

const confirmarEditarElemento = function (id, prefijo, elem_id) {
	let nombre = $("input#" + id + "_nombre").val();
	let descripcion = $("input#" + id + "_descripcion").val();
	$('td#' + id + '_editar>button').prop('disabled', true);
	$('td#' + id + '_editar>button').html(`<i class="fas fa-cog fa-lg fa-spin"></i>`);
	ajax(prefijo + '/' + elem_id, 'PUT', {nombre,descripcion}).then((res) => {
		// Hay que reestablecer la data del objeto
		let catalogos = JSON.parse(localStorage._cat_catalogos);
		catalogos[prefijo].forEach((cat, i) => {
			if (cat.id != elem_id) return;
			catalogos[prefijo][i] = res;
		});
		localStorage._cat_catalogos = JSON.stringify(catalogos);
		alertify.success("Concepto editado")
		renderCatalogos();
	}).catch((err) => {
		$('td#' + id + '_editar>button').prop('disabled', false);
		$('td#' + id + '_editar>button').html(`<i class="fas fa-pencil-alt fa-lg"></i>`);
		alertify.error("Error: " + err.responseText)
		console.warn(err);
	});	
}

/** Imprime la template para la lista de los catalogos */
const tplListadoCatalogos = function(data = {}, prefijo = '', filtro = '') {
	let tpl = `
		<table class="unstriped table-expand">
			<thead>
				<th>Concepto</th>
				<th class="show-for-medium">Descripción</th>
				<th>Editar</th>
				<th>Borrar</th>
			</thead>
			<tbody>`;
				// <tr>
				// 	<td colspan="5">
				// 		<center>
				// 			<a href="#" id="a_modal_nuevo_concepto" data-prefijo="${prefijo}" data-open="modal_nuevo_concepto">Crea un nuevo ${prefijo.replace('_', ' ')}</a>
				// 		<center>
				// 	</td>
				// </tr>
	let tpl_tbody = '';
	data.forEach((d, i) => {
		d.descripcion = (!d.descripcion) ? ' ' : d.descripcion;
		try {
			if (
				d.nombre.toLowerCase().indexOf(filtro) > -1 ||
				d.descripcion.toLowerCase().indexOf(filtro) > -1) {

					tpl_tbody += `
						<tr id="${prefijo + '_' + i}">
							<td width="35%"><p id="${prefijo + '_' + i + '_nombre'}">${d.nombre}</p></td>
							<td class="show-for-medium" width="45%"><p id="${prefijo + '_' + i + '_descripcion'}">${d.descripcion}</p></td>
							<td width="10%" id="${prefijo + '_' + i + '_editar'}">
								<button class="clear button" onclick="editarElemento('${prefijo + '_' + i}', '${prefijo}', ${d.id})">
									<i class="fas fa-pencil-alt fa-lg"></i>
								</button>
							</td>
			
							<td width="10%" id="${prefijo + '_' + i + '_eliminar'}">
								<button class="clear button" onclick="eliminarElemento('${prefijo}', ${d.id}, ${i})">
									<div style="color:#DB3A2C;">
										<i class="fas fa-trash-alt fa-lg"></i>
									</div>
								</button>
							</td>
						</tr>
					`;
			}
		} catch (error) {
			console.warn(error);
			return;
		}
	});
	if (tpl_tbody == '') {
		tpl_tbody += `
			<tr>
				<td colspan="5"><div><center>No hay datos en este catálogo</center></div></td>
			</tr>
		`;
	}
	tpl_tbody += `</tbody></table>`;
	return tpl + tpl_tbody;
}
