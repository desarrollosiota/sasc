"use strict";
$(document).ready(function($) {
    $(document).on('keypress', '#login_form', event => {
        if(event.keyCode == 13){
            login();
        }
    });   
});
/** Funcion que hace el login y maneja las respuestas */
function login() {
    // Obteniendo los datos del form
    let formulario = $('form#login_form').serializeArray();
    let usuario = formulario[0].value;
    let password = formulario[1].value;

    let data_login = {
        usuario,
        password
    };
    // Checar los datos
    if (usuario == '' || password == '') {
        alert('Llena bien los campos');
        return;
    }
    // Enviar ajax
    ajax('login', 'POST',data_login)
    .then((data) => {
        // Redirigir a home
        window.location.href = '/';
    }).catch((err) => {
        // Ajax retorna 400
        alert('Error en inicio de sesion');
        console.log(err);
    });
}
