"use script";

/** Obtener datos del usuario */
var g_session = {};

var obtenerUsuarioActual = async () => {
	// g_session = await ajax('usuario_actual','POST').catch(e => e);
	ajax('usuario_actual','POST').then((data) => {
		g_session = data.original;
		$('p#user_name').html(data.original.nombre);
		$('small#user_rol').html(data.original.usuario_rol.nombre);
		obtenerNotificaciones();
	}).catch(e => {
		// si algo pasa mal obteniendo al usuario, tronar y mandar a login
		console.warn(e);
		// logout();
	});
}
obtenerUsuarioActual();

/** Obtener notificaciones */
function obtenerNotificaciones() {
	ajax('notificacion').then(data => {
		// Las notificaciones se obtuvieron exitosamente
		// console.log(data);

		// Algunas variables
		let no_leidas = data.no_leidas;

		console.warn('notificaciones no leidas> ' + no_leidas);
		// Se recorren las notificaciones
		data.notificaciones.forEach((d, i) => {
			// console.log(i + '. notificacion: \n', d, '\nmetadata: \n', JSON.parse(d.metadata));
			
			// Listita de notificaciones
			console.log(d.id + ' ' + JSON.parse(d.metadata).tipo_notificacion, JSON.parse(d.metadata), d);
		});

		// !! POR AQUI SE DEBE HACER EL RENDEREO DE NOTIS CON LA DATA DE ARRIBA !!

	}).catch((e) => {
		// Error al obtener notificaciones
		console.warn(e);
	});
}

function leerNotificacion(notificacion_id) {
	ajax('notificacion/' + notificacion_id, 'PUT').then(data => {
		// Noti leida
		console.log(data);
	}).catch(e => {
		console.warn(e);
	});
}

/** Cerrar sesion en la aplicacion */
function logout() {
	ajax('logout','POST')
	.then((data) => {
		console.log('redirigir a login');
		window.location.href = '/login';
	}).catch((e) => {
		console.log(e);
		console.warn('error');
	});
}

$(document).ready(function($) {
	$(document).foundation();
	$(document).confirmWithReveal();
	// Configuracion de toastr
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-bottom-full-width",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "300",
		"timeOut": "6000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
	// Configuracion de AlertifyJS
	alertify.defaults = {
		// dialogs defaults
		autoReset:true,
		basic:false,
		closable:true,
		closableByDimmer:true,
		frameless:false,
		maintainFocus:true, // <== global default not per instance, applies to all dialogs
		maximizable:true,
		modal:true,
		movable:true,
		moveBounded:false,
		overflow:true,
		padding: true,
		pinnable:true,
		pinned:true,
		preventBodyShift:false, // <== global default not per instance, applies to all dialogs
		resizable:true,
		startMaximized:false,
		transition:'pulse',

		// notifier defaults
		notifier:{
			// auto-dismiss wait time (in seconds)
			delay:5,
			// default position
			position:'bottom-right',
			// adds a close button to notifier messages
			closeButton: false
		},

		// language resources
		glossary:{
			// dialogs default title
			title:'¡Atencion!',
			// ok button text
			ok: 'Aceptar',
			// cancel button text
			cancel: 'Cancelar'
		},

		// theme settings
		theme:{
			// class name attached to prompt dialog input textbox.
			input:'ajs-input',
			// class name attached to ok button
			ok:'ajs-ok',
			// class name attached to cancel button
			cancel:'ajs-cancel'
		}
	};
// 	var elem = new Foundation.ResponsiveToggle('#example-animated-menu');
});

// $(function() {
//   $(window).scroll(function() {
//     var winTop = $(window).scrollTop();
//     if (winTop >= 30) {
//       $("body").addClass("sticky-shrinknav-wrapper");
//     } else{
//       $("body").removeClass("sticky-shrinknav-wrapper");
//     }
//   });
// });
