"use strict";

window.onload = function() {
	cargar_puestos();
	cargar_estados_civiles();
	buscar_estados();
	/** Evento para buscar municipios despues de seleccionar un estado */
	$(document).on('change', 'select#select_estados', () => {
		buscar_municipios();
	});
	/** Evento para el campo de busqueda */
	$(document).on('keyup', 'input#busqueda_usuario', function(event) {
		try {
			let data = JSON.parse(localStorage._us_data_usuarios);
			// Conseguir la template con el filtro
			let tpl = tplTablaUsuarios(data, $(this).val());
			// Render data
			$('tbody#lista_usuarios').html(tpl);
		} catch(e) {
			console.warn(e);
		}
	});
	// Obteniendo usuarios
	obtenerUsuarios();
};

/** Obtiene todos los usuarios */
function obtenerUsuarios() {
	$('tbody#lista_usuarios').addClass('hide');
	$('tbody#spiner_tabla_usuarios').removeClass('hide');
    ajax('usuario', 'GET').then((data) => {
		localStorage._us_data_usuarios = JSON.stringify(data);
		let tpl = tplTablaUsuarios(data);
        // Render de usuarios
        $('tbody#lista_usuarios').html(tpl);
    }).catch((e) => {
    	console.warn(e)
    }).finally((f) => {
		$('tbody#lista_usuarios').removeClass('hide');
		$('tbody#spiner_tabla_usuarios').addClass('hide');
    });
}

function tplTablaUsuarios(usuarios, filtro = ''){
	filtro = filtro.trim();
	let tpl = '';
	// Recorriendo los usuarios obtenidos
	usuarios.forEach((usuario) => {
		// Condicional para filtro de busqueda
		try {
			if (
				usuario.nombre.toLowerCase().indexOf(filtro) > -1 ||
				usuario.usuario.toLowerCase().indexOf(filtro) > -1 ||
				usuario.usuario_rol.nombre.toLowerCase().indexOf(filtro) > -1 ||
				usuario.correo.toLowerCase().indexOf(filtro) > -1) {
				tpl += `
					<tr>
						<td data-label="Nombre">${usuario.nombre}</td>
						<td data-label="Usuario">${usuario.usuario}</td>
						<td class="show-for-large" data-label="Correo">${usuario.correo}</td>
						<td data-label="Permisos">${usuario.usuario_rol.nombre}</td>
						<td data-label="Acciones">
							<button class="clear button" data-toggle="editar_usuario_modal" onclick="obtenerUsuario(${usuario.id})"><i class="fas fa-pencil-alt fa-lg"></i></button>
							<button class="clear button alert" onclick="eliminarUsuario(${usuario.id})"><i class="fas fa-trash-alt fa-lg"></i></button>
						</td>
					</tr>
				`;
			}
		} catch (e) {
			return;
		}
	});
	if (tpl == '') {
		tpl += `
			<tr>
				<td colspan="5"><div><center>No existen usuarios</center></div></td>
			</tr>
		`;
	}
	return tpl;
}

/** Obtiene un solo usuario por id */
function obtenerUsuario(id) {
    // Mostrar loader y ocultar data
	$('#frm_editar_usuario').addClass('invisible');
	$('#spiner_editar_usuario').removeClass('hide');
    ajax('usuario/' + id, 'GET').then((usuario) => {
    	// Se cargan los select ya que si se cargan mientras estan ocultos se crean con tamaño diferente
    	$("select#select_puestos").select2({'placeholder':'Seleccionar Opción', theme: "foundation",openOnEnter: false});
		$("#select_estados_civiles").select2({'placeholder':'Seleccionar Opción', theme: "foundation"});
		$("#select_estados").select2({'placeholder':'Seleccionar Opción', theme: "foundation", dropdownParent: $('body')});
		$("#select_municipios").select2({'placeholder':'Seleccionar Opción', theme: "foundation"});
        
        // Checar si se encontro al usuario
        if (Object.keys(usuario).length == 0) {
            toastr["error"]("Hubo un error al intentar editar el usuario", "Error")
            $('div#editar_usuario_modal').foundation('close');
            return 0;
        }
        $('input#usuario').val(usuario.usuario);
        $('input#correo').val(usuario.correo);
        $('input#direccion').val(usuario.empleado.direccion);
        $('input#fecha_nacimiento').val(usuario.empleado.fecha_nacimiento);
        $('input#nombre').val(usuario.nombre);
        $('input#password').val('');
        $('input#telefono').val(usuario.empleado.telefono);
        $('input#ap_paterno').val(usuario.empleado.ap_paterno);
        $('input#ap_materno').val(usuario.empleado.ap_materno);
        $('select#select_estados_civiles').val(usuario.empleado.estado_civil_id);
        $('select#select_puestos').val(usuario.empleado.puesto_id);
		$('select#select_tipo_usuario').val(usuario.usuario_rol_id);

        try {
            $('select#select_estados').val(usuario.empleado.estado_id);
            buscar_municipios().then((data) => {
                $('select#select_municipios').val(usuario.empleado.municipio_id);
            });
        } catch (e) {
            console.log('El usuario no agrego estado ni municipio');
        }
		$('div#botones_editar_usuario').html(`
			<a class="button expanded success" onclick="editarUsuario(${usuario.id})" id="btn_editar_usuario">
				<i class="fas fa-spinner fa-spin fa-lg hide"></i>
				<span>Guardar</span>
			</a>
			<a class="button expanded" href="#!" onclick="$('#frm_nuevo_usuario')[0].reset();">Limpiar</a>`);

    }).catch((e) => {
        // Cerrar modal en caso de error
        $('div#editar_usuario_modal').foundation('close');
        console.warn(e);
    }).finally((f) => {
        // Ocultar loader
		$('#frm_editar_usuario').removeClass('invisible');
		$('#spiner_editar_usuario').addClass('hide');
    });
}

/** Edita un solo usuario por id */
function editarUsuario(id) {
    // Loader del boton
	$('#btn_editar_usuario>i').removeClass('hide');
	$('#btn_editar_usuario>span').addClass('hide');
    // Obteniendo data del formulario
    let usuario_editado = $('form#frm_editar_usuario').serializeObject();
    // Enviando data al server
    ajax('usuario/' + id, 'PUT', usuario_editado).then((res) => {
        console.log(res);
        toastr["success"](usuario_editado.nombre + " editado", "Listo");
        // Cerrar modal
        $('div#editar_usuario_modal').foundation('close');
        // Refresh de los usuarios
        obtenerUsuarios();
    }).catch((e) => {
        // Cerrar modal
        // $('div#editar_usuario_modal').foundation('close');
		let err_txt = (typeof e.responseText == "string") ? e.responseText : 'Error en el servidor';
		toastr["error"]("No se pudo editar el usuario: " + err_txt, "Error")
		console.log(err);
    }).finally((e) => {
        // Loader y botones
		$('#btn_editar_usuario>i').addClass('hide');
		$('#btn_editar_usuario>span').removeClass('hide');
    });
}

/** Elimina un solo usuario por id */
function eliminarUsuario(id) {
    alertify.confirm('Necesita Confirmacion', '¿Realmente desea eliminar el usuario?', function(){
	    ajax('usuario/' + id, 'DELETE').then((res) => {
	        if (res) {
    			alertify.success('El usuario fue eliminado con exito');
	            // toastr["success"]("Usuario eliminado", "Listo");
	            // Refresh de los usuarios
	            obtenerUsuarios();
	        } else {
    			alertify.error('No se pudo eliminar el usuario');
	            console.warn('usuario no encontrado');
	        }
	    }).catch((e) => {
			alertify.error('No se pudo eliminar el usuario');
	        console.warn(e);
	    });
	},function(){
		
	});
}

var cargar_puestos = async () => {
	var puestos = await ajax('usuarios','POST').catch(e => e);
	var select_datos = [];
	if(puestos){
		puestos.forEach(puesto => {
			select_datos.push(`<option value="${puesto.id}">${puesto.nombre}</option>`);
		});
		$('select#select_puestos').html(select_datos.join(''));
	}
}

var cargar_estados_civiles = async () => {
	var estados_civiles = await ajax('usuarios_estados_civiles','POST').catch(e => e);
	var select_datos = [];
	if(estados_civiles){
		estados_civiles.forEach(estado_civil => {
			select_datos.push(`<option value="${estado_civil.id}">${estado_civil.nombre}</option>`);
		});
		$('select#select_estados_civiles').html(select_datos.join(''));
	}
}

var buscar_estados = async ()=>{
	var estados = await ajax('estados','POST').catch(e => e);
	var select_datos = [];
	if(Object.keys(estados).length > 0){
		select_datos.push(`<option value="0">Estado</option>`);
		estados.forEach(estado => {
			select_datos.push(`<option value="${estado.id}">${estado.nombre}</option>`);
		});
		$('select#select_estados').html(select_datos.join(''));
	}
}

var buscar_municipios = async ()=>{
	var id_estado = $('#select_estados').val();
	if(id_estado > 0){
		var municipios = await ajax('municipios','POST',{'id_estado':id_estado}).catch(e => e);
		var select_datos = [];
		if(Object.keys(municipios).length > 0){
			municipios.forEach(municipio => {
				select_datos.push(`<option value="${municipio.id}">${municipio.nombre}</option>`);
			});
			$('select#select_municipios').html(select_datos.join(''));
		}
	}else{
		// toastr["error"]("No se selecciono un estado", "Error");
	}
}
