"use strict";

window.onload = function() {
	// $('#nacimiento').fdatepicker();
	cargar_puestos();
	cargar_estados_civiles();
	buscar_estados();

	$(document).on('change', 'select#select_estados', () => {
		buscar_municipios();
	});

	$(document).on('keypress', 'form#frm_nuevo_usuario', (e) => {
		if (e.keyCode == 13) $('form#frm_nuevo_usuario').submit();
	});

	$(document).on('submit', 'form#frm_nuevo_usuario', function(event) {
		guardar_usuario();
		event.preventDefault();
	});
	
	$('.select2').on('keypress', function (e) {
		if (e.keyCode === 13) {
			e.preventDefault();
   			e.stopPropagation(); 
		}
	});
};

var guardar_usuario = ()=>{
	$('#btn_nuevo_usuario>i').removeClass('hide');
	$('#btn_nuevo_usuario>span').addClass('hide');
	// Obteniendo los datos del form
	// $('form#frm_nuevo_usuario').submit();
	let data_usuario = $('form#frm_nuevo_usuario').serializeObject();
	// Enviar ajax
	ajax('usuario', 'POST', data_usuario)
	.then((data) => {
		alertify.success(data_usuario.usuario + " creado");
		// Borrar formulario
		limpiar_nuevo_usuario();
	}).catch((err) => {
		// Ajax retorna 400
		let err_txt = (typeof err.responseText == "string") ? err.responseText : 'Error en el servidor';
		alertify.error("No se pudo crear el usuario: " + err_txt);
		console.log(err);
	}).finally((f) => {
		$('#btn_nuevo_usuario>i').addClass('hide');
		$('#btn_nuevo_usuario>span').removeClass('hide');
	});
}

var cargar_puestos = async () => {
	var puestos = await ajax('usuarios','POST').catch(e => e);
	var select_datos = [];
	if(puestos){
		select_datos.push(`<option value="">Seleccionar Opción</option>`);
		puestos.forEach(puesto => {
			select_datos.push(`<option value="${puesto.id}">${puesto.nombre}</option>`);
		});
		$('#select_puestos').html(select_datos.join(''));
		$("#select_puestos").select2({'placeholder':'Seleccionar Opción', theme: "foundation",openOnEnter: false});
	}
}

var cargar_estados_civiles = async () => {
	var estados_civiles = await ajax('usuarios_estados_civiles','POST').catch(e => e);
	var select_datos = [];
	if(estados_civiles){
		select_datos.push(`<option value="">Seleccionar Opción</option>`);
		estados_civiles.forEach(estado_civil => {
			select_datos.push(`<option value="${estado_civil.id}">${estado_civil.nombre}</option>`);
		});
		$('#select_estados_civiles').html(select_datos.join(''));
		$("#select_estados_civiles").select2({'placeholder':'Seleccionar Opción', theme: "foundation"});
	}
}

var buscar_estados = async ()=>{
	var estados = await ajax('estados','POST').catch(e => e);
	var select_datos = [];
	if(Object.keys(estados).length > 0){
		select_datos.push(`<option value="">Seleccionar Opción</option>`);
		estados.forEach(estado => {
			select_datos.push(`<option value="${estado.id}">${estado.nombre}</option>`);
		});
		$('#select_estados').html(select_datos.join(''));
		$("#select_estados").select2({'placeholder':'Seleccionar Opción', theme: "foundation", dropdownParent: $('body')});

	}
}

var buscar_municipios = async ()=>{
	var id_estado = $('#select_estados').val();
	if(id_estado > 0){
		var municipios = await ajax('municipios','POST',{'id_estado':id_estado}).catch(e => e);
		var select_datos = [];
		if(Object.keys(municipios).length > 0){
		select_datos.push(`<option value="">Seleccionar Opción</option>`);
			municipios.forEach(municipio => {
				select_datos.push(`<option value="${municipio.id}">${municipio.nombre}</option>`);
			});
			$('#select_municipios').html(select_datos.join(''));
			$("#select_municipios").select2({'placeholder':'Seleccionar Opción', theme: "foundation"});
			$("#select_municipios").prop({disabled: '',});
		}
	}else{
		// alertify.error("No se selecciono un estado");
	}
}

const limpiar_nuevo_usuario = ()=>{
	$('#frm_nuevo_usuario')[0].reset();
	$('#select_estados').val(null).trigger('change');
	$('#select_municipios').val(null).trigger('change');
	$('#select_estados_civiles').val(null).trigger('change');
	$('#select_puestos').val(null).trigger('change');
}
