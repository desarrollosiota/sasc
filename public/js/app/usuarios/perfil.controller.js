"use strict";

window.onload = function () {
	ajax('usuario_actual', 'POST').then((data) => {
		let g_session = data;
		cargar_puestos();
		cargar_estados_civiles();
		buscar_estados();

		// Cargar data usuario ----------

		// Checar si se encontro al usuario
		if (Object.keys(usuario).length == 0) {
			toastr["error"]("Hubo un error al intentar editar el usuario", "Error")
			return 0;
		}
		$('input#usuario').val(g_session.original.usuario);
		$('span#titulo_nombre_perfil').html(g_session.original.usuario);
		$('input#correo').val(g_session.original.correo);
		$('input#direccion').val(g_session.original.empleado.direccion);
		$('input#fecha_nacimiento').val(g_session.original.empleado.fecha_nacimiento);
		$('input#nombre').val(g_session.original.nombre);
		$('input#password').val('');
		$('input#telefono').val(g_session.original.empleado.telefono);
		$('input#ap_paterno').val(g_session.original.empleado.ap_paterno);
		$('input#ap_materno').val(g_session.original.empleado.ap_materno);
		$('select#select_estados_civiles').val(g_session.original.empleado.estado_civil_id);
		$('select#select_puestos').val(g_session.original.empleado.puesto_id);
		$('select#select_tipo_usuario').val(g_session.original.usuario_rol_id);

		$('div#spiner_usuario').addClass('hide');
		$('form#frm_usuario').removeClass('hide');
		// Cargar data usuario ----------

		// Se cargan los select ya que si se cargan mientras estan ocultos se crean con tamaño diferente
		// $("select#select_puestos").select2({ 'placeholder': 'Seleccionar Opción', theme: "foundation", openOnEnter: false });
		// $("#select_estados_civiles").select2({ 'placeholder': 'Seleccionar Opción', theme: "foundation" });
		// $("#select_estados").select2({ 'placeholder': 'Seleccionar Opción', theme: "foundation", dropdownParent: $('body') });
		// $("#select_municipios").select2({ 'placeholder': 'Seleccionar Opción', theme: "foundation" });

		/** Evento para buscar municipios despues de seleccionar un estado */
		$(document).on('change', 'select#select_estados', () => {
			buscar_municipios();
		});
		$(document).on('submit', 'form', (e) => {
			e.preventDefault();
		});
	});
	
};

/** Edita un solo usuario por id */
function actualizarUsuario() {
	// Loader del boton
	$('#btn_usuario>i').removeClass('hide');
	$('#btn_usuario>span').addClass('hide');
	// Obteniendo data del formulario
	let usuario_editado = $('form#frm_usuario').serializeObject();
	// Enviando data al server
	ajax('actualizar_perfil', 'PUT', usuario_editado).then((res) => {
		console.log(res);
		$('span#titulo_nombre_perfil').html(res.usuario);
		toastr["success"]("Tu perfil ha sido editado", "Listo");
	}).catch((e) => {
		toastr["error"]("No se pudo editar tu perfil" , "Error")
		console.log(err);
	}).finally((e) => {
		// Loader y botones
		$('#btn_usuario>i').addClass('hide');
		$('#btn_usuario>span').removeClass('hide');
	});
}

var cargar_puestos = async () => {
	var puestos = await ajax('usuarios', 'POST').catch(e => e);
	var select_datos = [];
	if (puestos) {
		puestos.forEach(puesto => {
			select_datos.push(`<option value="${puesto.id}">${puesto.nombre}</option>`);
		});
		$('select#select_puestos').html(select_datos.join(''));
	}
}

var cargar_estados_civiles = async () => {
	var estados_civiles = await ajax('usuarios_estados_civiles', 'POST').catch(e => e);
	var select_datos = [];
	if (estados_civiles) {
		estados_civiles.forEach(estado_civil => {
			select_datos.push(`<option value="${estado_civil.id}">${estado_civil.nombre}</option>`);
		});
		$('select#select_estados_civiles').html(select_datos.join(''));
	}
}

var buscar_estados = async () => {
	var estados = await ajax('estados', 'POST').catch(e => e);
	var select_datos = [];
	if (Object.keys(estados).length > 0) {
		select_datos.push(`<option value="0">Estado</option>`);
		estados.forEach(estado => {
			select_datos.push(`<option value="${estado.id}">${estado.nombre}</option>`);
		});
		$('select#select_estados').html(select_datos.join(''));
	}
}

var buscar_municipios = async () => {
	var id_estado = $('#select_estados').val();
	if (id_estado > 0) {
		var municipios = await ajax('municipios', 'POST', { 'id_estado': id_estado }).catch(e => e);
		var select_datos = [];
		if (Object.keys(municipios).length > 0) {
			municipios.forEach(municipio => {
				select_datos.push(`<option value="${municipio.id}">${municipio.nombre}</option>`);
			});
			$('select#select_municipios').html(select_datos.join(''));
		}
	} else {
		// toastr["error"]("No se selecciono un estado", "Error");
	}
}
