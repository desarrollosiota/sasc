<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);

// /*
// |--------------------------------------------------------------------------
// | Funciones globales para el frontend del proyecto
// |--------------------------------------------------------------------------
// |
// | Se van agregando funciones que son de ayuda para tener un ambiente
// | frontend lo mas escalable posible.
// | NO HAY QUE agarrarle tanto cariño, ya que esta estructura se sustituye
// | facilmente por cualquier framework para FrontEnd!!
// |
// */
//
// /**
//  * Construimos el path con todo y http, dominio y puerto
//  * @return text $link
//  */
// function construirLink() {
//     # Construyendo el link completo
//     $link = env('APP_URL');
//     # Checando el puerto, para concatenarlo
//     if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '') {
//         $link .= ':'.$_SERVER['SERVER_PORT'];
//     }
//     # Retornar resultado
//     return $link;
// }
//
// /**
//  * Construimos la url que va despues del dominio
//  * @return text $url
//  */
// function construirUrl() {
//     # Url despues del dominio
//     $url = $_SERVER['REQUEST_URI'];
//     # Eliminando datos de GET en la url. Ej: 'dominio.com/url?datos=10'
//     if ( strpos($url, '?') ) {
//         $url = substr($url, 0, strpos($url, "?"));
//     }
//     # Si esta vacia ponemos index
//     $url = ($url == '' || $url == '/') ? '/index' : $url;
//     # Retornar resultado
//     return $url;
// }
//
// /**
//  * Incluye los javascript globales personalizados y librerias
//  */
// function incluirJsGlobales() {
//     # Construir el link
//     $link = construirLink();
//     # Agregar el path definido para los JS globales
//     $ruta_archivo = $link.'/js/librerias/';
//
//     /** Arreglo con los nombres de los js que necesitamos */
//
//         $js_incluidos = [
//             'jquery.min.js',
//             'funciones.globales.js',
//             'zurb/vendor/foundation.min.js'
//         ];
//
//     # Incluyendo cada uno
//     foreach ($js_incluidos as $archivo) {
//         echo "<script type='text/javascript' src='" . $ruta_archivo . $archivo . "'></script>";
//     }
// }
//
// /**
//  * Incluye los javascript de la pagina actual
//  * @param [bool] $controller, $model, $template
//  */
// function incluirJs($controller = true, $model = true, $template = true) {
//     # Construir el link
//     $link = construirLink();
//     $url = construirUrl();
//     # Agregar el path definido para los JS
//     $ruta_archivo = $link.'/js/app'.$url;
//     # Incluyendo los JS que se necesiten
//     if ($controller)    echo "<script type='text/javascript' src='" . $ruta_archivo . ".controller.js'></script>";
//     if ($model)         echo "<script type='text/javascript' src='" . $ruta_archivo . ".model.js'></script>";
//     if ($template)      echo "<script type='text/javascript' src='" . $ruta_archivo . ".template.js'></script>";
// }
//
// /**
//  * Incluye los css de la pagina actual
//  */
// function incluirCss() {
//     # Construir el link
//     $link = construirLink();
//     $url = construirUrl();
//     # Agregar el path definido para los JS
//     $ruta_archivo = $link.'/css/app'.$url;
//     # Incluyendo el css
//     echo "<link rel='stylesheet' type='text/css' href='" . $ruta_archivo . ".css'>";
// }
//
//
// /**
//  * Incluye los javascript globales personalizados y librerias
//  */
// function incluirCssGlobales() {
//     # Construir el link
//     $link = construirLink();
//     # Agregar el path definido para los css globales
//     $ruta_archivo = $link.'/css/librerias/';
//
//     /** Arreglo con los nombres de los css que necesitamos */
//
//         $css_incluidos = [
//             'zurb/foundation.min.css'
//         ];
//
//     # Incluyendo cada uno
//     foreach ($css_incluidos as $archivo) {
//         echo "<link rel='stylesheet' type='text/css' href='" . $ruta_archivo . $archivo . "'>";
//     }
// }
// incluirJsGlobales();
// incluirCssGlobales();
