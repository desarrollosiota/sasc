<?php

namespace App\Helpers\notificaciones;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

use App\Http\Models\Usuario;
use App\Http\Models\Notificacion;

use Exception;

/**
 * Clase que ayuda en validaciones
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
class NotificacionesHelper
{

	/** Tipos de notificaciones - OJO el 'tipo_noti' esta como los ids de la base de datos */
	private $tipos_de_notificaciones = [
		# Notificacion para ticket nuevo
		'nuevo_ticket' => [
			'tipo_noti' => 1,
			'link' => '/reportes/ticket/'
			// 'link' => 'sdf 1'
		],
		# Notificacion para cuando se cancela un ticket
		'cancelar_ticket' => [
			'tipo_noti' => 2,
			'link' => '/reportes/ticket/'
			// 'link' => 'sdf 2'
		],
		# Notificacion para cuando alguien reactiva un ticket cancelado
		'reactivar_ticket' => [
			'tipo_noti' => 3,
			'link' => '/reportes/ticket/'
			// 'link' => 'sdf 3'
		],
		# Notificacion para cuando se termina un ticket
		'terminar_ticket' => [
			'tipo_noti' => 4,
			'link' => '/reportes/ticket/'
			// 'link' => 'sdf 4'
		],
		# Notificacion para cuando un empleado comenta ticket
		'empleado_comenta_ticket' => [
			'tipo_noti' => 5,
			'link' => '/reportes/ticket/'
			// 'link' => 'sdf 5'
		],
		# Notificacion para cuando un admin comenta ticket
		'admin_comenta_ticket' => [
			'tipo_noti' => 6,
			'link' => '/operacion/servicios/nuevo?ticket='
			// 'link' => 'sdf 6'
		]
	];

	/**
	 * Emite el tipo de notificacion que se requiera
	 * @param  string $tipo_notificacion
	 * @param  int $usuario_id # Es al usuario especifico a quien se le enviara la notificacion
	 * @param  bool $admins # Si es true, se le emite a todos los administradores
	 * @return bool
	 */
	public function emit($tipo_notificacion = '', $usuario_id = 0, $admins = false, $metadata = []) {
		@session_start();
		if (!isset($this->tipos_de_notificaciones[$tipo_notificacion])) return false;
		$tipo_noti = $this->tipos_de_notificaciones[$tipo_notificacion]['tipo_noti'];
		
		// return (json_encode($this->tipos_de_notificaciones[$tipo_notificacion]));
		
		$metadata['link'] = $this->tipos_de_notificaciones[$tipo_notificacion]['link'] . $metadata['ticket_id'];
		// $metadata['link'] = 'link';
		
		$metadata['tipo_notificacion'] = $tipo_notificacion;
		$metadata = json_encode($metadata);
		// $cuerpo_notificacion = Notificacion::find($tipo_noti);
		
		$socket_ids = [];
		# Si se envia un id de usuario, se le emite notificacion
		if ($usuario_id != 0) {
			$usuario = Usuario::find($usuario_id);
			if (count((array)$usuario) > 0) {
				$notificacion = new Notificacion;
				$notificacion->usuario_id = $usuario->id;
				$notificacion->tipo_notificacion_id = $tipo_noti;
				$notificacion->metadata = $metadata;
				$notificacion->leida = 0;
				$notificacion->save();
				# Arreglo de ids para emitir
				$socket_ids[] = $usuario->socket_id;
			}
		}
		# Si admins es true, se le envia una notificacion a cada uno de ellos
		if ($admins) {
			$admins = Usuario::where('usuario_rol_id', 2)->get(['id','socket_id']);
			foreach($admins as $admin) {
				# Omitiendo el id que se envio para emitir
				if ($admin->id == $usuario_id || $admin->id == $_SESSION['id']) continue;
				# Creando notificacion para cada admin
				$notificacion = new Notificacion;
				$notificacion->usuario_id = $admin->id;
				$notificacion->tipo_notificacion_id = $tipo_noti;
				$notificacion->metadata = $metadata;
				$notificacion->leida = 0;
				$notificacion->save();
				# Arreglo de ids para emitir
				$socket_ids[] = $admin->socket_id;
			}
		}
		# En calor conectamos, emitimos y cerramos
		try {
			$client = new Client(new Version1X('http://10.0.0.4:3002'));
			$client->initialize();
			$client->emit('canal server', ['socket_id' => $socket_ids, 'data' => json_decode($metadata)]);
			$client->close();
		} catch (Exception $e) {
			//
		}

		return true;
	}

}
