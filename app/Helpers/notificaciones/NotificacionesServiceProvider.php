<?php

namespace App\Helpers\notificaciones;

use Illuminate\Support\ServiceProvider;

class NotificacionesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
      \App::bind('NotificacionesHelper', function()
      {
          return new \App\Helpers\notificaciones\NotificacionesHelper;
      });
    }
}
