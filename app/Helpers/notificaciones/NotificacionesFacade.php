<?php

namespace App\Helpers\notificaciones;

use Illuminate\Support\Facades\Facade;

class NotificacionesFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'NotificacionesHelper';
    }
}
