<?php

namespace App\Helpers\views404;

use Illuminate\Support\Facades\Facade;

class Views404Facade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'Views404Helper';
    }
}
