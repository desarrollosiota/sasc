<?php

namespace App\Helpers\views404;

/**
 * Clase que ayuda en validaciones
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
class Views404Helper
{

	/**
	 * Compara dos arreglos:
	 * @param  string $uri  [Uri de donde proviene el usuario]
	 * @return void
	 */
	public function check($uri) {
		// return response()->view('usuarios.lista_usuarios');
		@session_start();
		# Checar sesion
		if (count($_SESSION) > 0) {
			# Mostrar vista de error para admins
			if ($_SESSION['usuario_rol_id'] == 2) return response()->view('errors.404_admin');
			# Mostrar vista de error para empleados
			if ($_SESSION['usuario_rol_id'] == 1) return response()->view('errors.404_user');
		}
		# No hay sesion checando si la ruta original viene de login
		if ($uri == 'login') return $next($request);
		# Redireccionando a login
		return redirect('/login');
	}

}
