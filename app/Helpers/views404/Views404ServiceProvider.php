<?php

namespace App\Helpers\views404;

use Illuminate\Support\ServiceProvider;

class Views404ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
      \App::bind('Views404Helper', function()
      {
          return new \App\Helpers\views404\Views404Helper;
      });
    }
}
