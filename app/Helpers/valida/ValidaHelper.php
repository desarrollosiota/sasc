<?php

namespace App\Helpers\valida;

/**
 * Clase que ayuda en validaciones
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
class ValidaHelper
{
    /** Arregelo que registra los errores */
    private static $e = [];

    /**
     * Compara dos arreglos:
     * El primero '$data' es el arreglo a evaluar
     * El segundo '$rules' tiene los indices que debe haber en el primero
     * El tercer parametro funciona para enviar un mensaje como respuesta de error
     * respuesta de error en caso de que la comparacion salga incorrecta
     * @param  $data    [array]
     * @param  $rules   [array]
     * @param  $error   [text]
     * @return true || die()
     */
    public function check($data,$rules,$error='Campos necesarios') {
        $data = array_filter($data);
        foreach ($rules as $rule) {
            if(!array_key_exists($rule, $data)){
                array_push(self::$e, $rule);
            }
        }
        if (count(self::$e) > 0) {
            # Existen errores, se retorna un status 400 y un mensaje con lo errores encontrados
            header('Status: 400');
            $msg = '';
            for ($i=0, $length = count(self::$e); $i < $length; $i++) {
                $msg .= self::$e[$i];
                if ($i+1 != $length) $msg .= ', ';
            }
            die($error.': '.$msg);
        }
        return true;
    }

}
