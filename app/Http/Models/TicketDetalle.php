<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TicketDetalle extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'ticket_detalle';
	
	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $fillable = [
		// 'nombre', 'usuario', 'password',
	];

	/**
	 * Define una relación muchos a uno con la tabla ticket
	 */
	public function ticket(){
		return $this->belongsTo('App\Http\Models\Ticket');
	}

	/**
	 * Define una relación muchos a uno con la tabla servicio_costo
	 */
	public function servicios_costo(){
		return $this->belongsTo('App\Http\Models\Servicio_costo', 'servicio_costo_id');
	}

	/**
	 * Define una relación muchos a uno con la tabla unidad
	 */
	public function unidad(){
		return $this->belongsTo('App\Http\Models\Unidad');
	}
}
