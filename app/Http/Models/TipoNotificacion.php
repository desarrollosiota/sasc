<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TipoNotificacion extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'tipo_notificaciones';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $fillable = [
		#
	];

	/**
	 * Define una relación de a uno a uno con la tabla empleado
	 */
	public function notificacion(){
		return $this->hasMany('App\Http\Models\Notificacion');
	}
}
