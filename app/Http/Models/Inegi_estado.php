<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Inegi_estado extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'inegi_estado';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];

	/**
	 * Define una relación muchos a uno con la tabla cliente
	 */
	public function municipios(){
		return $this->hasMany('App\Http\Models\Inegi_municipio');
	}
}
