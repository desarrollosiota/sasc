<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class contacto extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'contacto';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];

	/**
	 * Define una relación muchos a uno con la tabla cliente
	 */
	public function cliente(){
		return $this->belongsTo('App\Http\Models\Cliente');
	}
}
