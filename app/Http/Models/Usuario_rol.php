<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario_rol extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'usuario_rol';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = ['nombre','descripcion'];
	
	/**
	 * Define una relación uno a muchos con la tabla usuario
	 */
	public function usuarios(){
		return $this->hasMany('App\Http\Models\Usuario');
	}
}
