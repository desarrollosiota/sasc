<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Servicio_costo extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'cliente_servicio_costo';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];

	/**
	 * Define una relación muchos a uno con la tabla servicio
	 */
	public function servicio(){
		return $this->belongsTo('App\Http\Models\Servicio');
	}

	/**
	 * Define una relación muchos a uno con la tabla cliente
	 */
	// public function cliente(){
	// 	return $this->belongsTo('App\Http\Models\Cliente');
	// }

	/**
	 * Define una relación uno a muchos con la tabla ticket_detalle
	 */
	// public function tickets_detalle(){
	// 	return $this->hasMany('App\Http\Models\Ticket_detalle');
	// }
}