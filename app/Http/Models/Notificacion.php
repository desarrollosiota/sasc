<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'notificaciones';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $fillable = [
		#
	];

	/**
	 * Define una relación muchos a uno con la tabla usuario
	 */
	public function usuario(){
		return $this->belongsTo('App\Http\Models\Usuario');
	}

	/**
	 * Define una relación de a uno a uno con la tabla tipo notificacion
	 */
	public function tipo_notificacion(){
		return $this->belongsTo('App\Http\Models\TipoNotificacion');
	}
}
