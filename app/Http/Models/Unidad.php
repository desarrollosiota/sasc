<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class unidad extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'unidad';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];

	/**
	 * Define una relación muchos a uno con la tabla cliente
	 */
	public function cliente(){
		return $this->belongsTo('App\Http\Models\Cliente');
	}
}
