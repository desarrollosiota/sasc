<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'ticket';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $fillable = [
		// 'nombre', 'usuario', 'password',
	];

	/**
	 * Define una relación uno a uno con la tabla cliente
	 */
	public function cliente(){
		return $this->belongsTo('App\Http\Models\Cliente');
	}

	/**
	 * Define una relación muchos a uno con la tabla empleado
	 */
	public function empleado(){
		return $this->belongsTo('App\Http\Models\Empleado', 'usuario_id');
	}

	/**
	 * Define una relación uno a muchos con la tabla ticket_detalle
	 */
	public function tickets_detalle(){
		return $this->hasMany('App\Http\Models\TicketDetalle');
	}
}
