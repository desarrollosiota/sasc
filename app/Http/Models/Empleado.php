<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'empleado';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];

	/**
	 * Define una relación uno a uno con la tabla usuario
	 */
	public function usuario(){
		return $this->belongsTo('App\Http\Models\Usuario');
	}

	/**
	 * Define una relación uno a uno con la tabla usuario
	 */
	public function puesto(){
		return $this->belongsTo('App\Http\Models\Puesto');
	}

	/**
	 * Define una relación uno a muchos con la tabla ticket
	 */
	public function tickets(){
		return $this->hasMany('App\Http\Models\Ticket');
	}
}
