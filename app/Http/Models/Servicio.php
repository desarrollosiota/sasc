<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'servicio';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];
	
	/**
	 * Define una relación muchos a uno con la tabla servicio_costo
	*/
	public function Servicios_costo(){
		return $this->hasMany('App\Http\Models\Servicio_costo');
	}

	/**
	 * Define una relación muchos a muchos con la tabla cliente
	 */
	public function clientes(){
		return $this->belongsToMany('App\Http\Models\Cliente', 'cliente_servicio_costo')->withPivot('id', 'costo', 'descripcion');
	}

}

?>