<?php

namespace App\http\models;

use Illuminate\Database\Eloquent\Model;

class TicketComentarios extends Model
{
   /**
    * Define el nombre de la tabla del modelo
    * @var string
    */
   protected $table = 'ticket_comentarios';

   /**
    * Define una relación uno a uno con la tabla ticket
    */
   public function ticket(){
      return $this->belongsTo('App\Http\Models\Ticket');
   }

   /**
    * Define una relación uno a uno con la tabla usuario
    */
   public function usuario(){
      return $this->belongsTo('App\Http\Models\Usuario');
   }
}
