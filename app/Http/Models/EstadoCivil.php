<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'estado_civil';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];
}
