<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'puesto';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];
	
	/**
	 * Define una relación muchos a uno con la tabla servicio_costo
	*/
	public function empleado(){
		return $this->hasMany('App\Http\Models\Empleado');
	}
}
