<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'cliente';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $filleable = [];
	
	/**
	 * Define una relación uno a muchos con la tabla contacto
	 */
	public function contactos(){
		return $this->hasMany('App\Http\Models\Contacto');
	}

	/**
	 * Define una relación uno a muchos con la tabla unidad
	 */
	public function unidades(){
		return $this->hasMany('App\Http\Models\Unidad');
	}

	/**
	 * Define una relación uno a muchos con la tabla servicio_costo
	 */
	public function servicios_costo(){
		return $this->hasMany('App\Http\Models\Servicio_costo');
	}
	
	/**
	 * Define una relación muchos a muchos con la tabla servicio
	 */
	public function servicios(){
		return $this->belongsToMany('App\Http\Models\Servicio', 'cliente_servicio_costo')->withPivot('id', 'costo', 'descripcion');
	}


}
