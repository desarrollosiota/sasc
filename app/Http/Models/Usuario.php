<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{

	/**
	 * Define el nombre de la tabla del modelo
	 * @var string
	 */
	protected $table = 'usuario';

	/**
	 * Los campos que pueden ser asignados masiva mente
	 * @var array
	 */
	protected $fillable = [
		'nombre', 'usuario', 'password', 'correo'
	];

	/**
	 * Los campos que deben ser ocultados en los datos
	 * @var array
	 */
	protected $hidden = [
		'password'
	];

	/**
	 * Define una relación muchos a uno con la tabla usuario_rol
	 */
	public function usuario_rol(){
		return $this->belongsTo('App\Http\Models\Usuario_rol');
	}

	/**
	 * Define una relación uno a muchos con la tabla notificaciones
	 */
	public function notificacion(){
		return $this->hasMany('App\Http\Models\Notificacion');
	}

	/**
	 * Define una relación de a uno a uno con la tabla empleado
	 */
	public function empleado(){
		return $this->hasOne('App\Http\Models\Empleado');
	}
}
