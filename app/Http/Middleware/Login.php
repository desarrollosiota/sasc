<?php

namespace App\Http\Middleware;

use Illuminate\Http\Response;
use Closure;

class Login
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		@session_start();
		if (count($_SESSION) > 0) return redirect('/');
		return $next($request);
	}
	
}