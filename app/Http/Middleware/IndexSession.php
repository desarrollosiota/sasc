<?php

namespace App\Http\Middleware;

use Illuminate\Http\Response;
use Closure;

class IndexSession
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		@session_start();
		# Lista de urls para redireccionar dependiendo del tipo de sesion
		if ($request->route()->uri == '/' && $_SESSION['usuario_rol_id'] == 1) return redirect('/operacion');
		# Retornar vista normal
		return $next($request);
	}
	
}