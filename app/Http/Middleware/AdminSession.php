<?php

namespace App\Http\Middleware;

use Response;
use Closure;
use Views404;

class AdminSession
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		@session_start();
		# Hay sesion, verificar el tipo de usuario
		if ($_SESSION['usuario_rol_id'] == 2) return $next($request);
		# Retornamos 404 y el fallback de web.php hara su trabajo
		return Views404::check($request->route()->uri);
	}
	
}
