<?php

namespace App\Http\Middleware;

use Response;
use Closure;
use Views404;

class AuthSession
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixedthrow api
	 */
	public function handle($request, Closure $next)
	{
		@session_start();
		# Checando sesion
		if (count($_SESSION) == 0) {
			if (strstr($request->route()->uri, 'api/') && $request->route()->methods[0] != 'GET') {
				return Response::json('No autorizado', 401);
			}
			return Views404::check($request);
		}
		# Existe sesion, continuar
		return $next($request);
	}
}
