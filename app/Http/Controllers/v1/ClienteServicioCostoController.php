<?php

namespace App\Http\Controllers\v1;

use Throwable;
// use PDOException;
use Response;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Http\Models\Cliente;
use App\Http\Models\Servicio;
use App\Http\Models\Servicio_costo AS servicioCosto;

use Valida;

class ClienteServicioCostoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		return Response::json(Cliente::with('servicios')->get(), 200);
		// $cliente = Cliente::with('servicios')->first();
		// foreach ($cliente->servicios as $servicio) {
		// 	$cliente->servicios[] =  $servicio->pivot;
		// }
		// return $cliente;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		$data = Request::input();
		# Checando los inputs obligatorios
		Valida::check($data, ['cliente_id', 'servicio_id', 'costo']);
		try{
			# Todo correcto, insertamos data
			$cliente = Cliente::find($data['cliente_id']);
			$servicio_data = [];
			$servicio_data['costo'] = $data['costo'];
			$servicio_data['descripcion'] = (is_null($data['descripcion'])) ? '' : $data['descripcion'];
			// $servicio_data['descripcion'] = $data['descripcion'];
			$cliente->servicios()->attach($data['servicio_id'], $servicio_data);
			# Retorna el cliente
			return Response::json($cliente, 200);
		}catch(Throwable $e) {
			// return Response::json($e, 400);
			return Response::json('Error al guardar los datos', 400);
		}
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id){
		return Response::json(Cliente::with('servicios')->find($id), 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		$data = Request::input();
		# Checando los inputs obligatorios
		Valida::check($data, ['servicio_id', 'costo']);
		try{
			# Todo correcto, insertamos data
			$cliente = Cliente::with('servicios')->find($id);
			$servicio_data = [];
			$servicio_data['costo'] = $data['costo'];
			$servicio_data['descripcion'] = is_null($data['descripcion']) ? '' : $data['descripcion'];
			$cliente->servicios()->updateExistingPivot($data['servicio_id'], $servicio_data, false);
			# Retorna el cliente
			return Response::json($cliente, 200);
		}catch (Throwable $e){
			return Response::json($e->getMessage(), 400);
			return Response::json('Error al editar, verifique su información', 400);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		$data = Request::input();
		try{
			$cliente = Cliente::with('servicios')->find($id);
			$cliente->servicios()->detach($data['id']);
			return Response::json('ok', 200);
		}catch (Throwable $e){
			return Response::json($e->getMessage(), 400);
			return Response::json('Error al borrar, verifique sus datos', 400);
		}
	}

	/**
	 * Busca todos los servicios dados de alta
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function buscarServicios(){
		return Response::json(
			Servicio::select('id', 'nombre')->get()
			, 200);
		// return Response::json(
		// 	DB::table('servicio')
		// 		->leftJoin('cliente_servicio_costo','servicio.id','=','cliente_servicio_costo.servicio_id')
		// 		->select('servicio.*','cliente_servicio_costo.cliente_id')
		// 		->whereNull('cliente_servicio_costo.cliente_id')
		// 		->get()
		// 	,200);
		
	}

	/**
	 * Busca todos los servicios dados de alta
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function buscarServiciosDatos(Request $request){
		$data = Request::input();
		# Checando los inputs obligatorios
		Valida::check($data, ['cliente_id', 'servicio_id']);
		return Response::json(
			Cliente::with(['servicios' => function($query) use ($data){
				$query->find($data['servicio_id']);
			}])->find($data['cliente_id'])
			, 200);
	}
}
