<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Request;
use Response;
use Throwable;
use Exception;
use PDOException;

use Valida;

use App\Http\Models\TicketDetalle;
use App\Http\Models\Ticket;
use App\Http\Models\Servicio_costo AS ServicioCosto;

class TicketDetalleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		return Response::json(TicketDetalle::get(), 200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		$data = Request::input();
		Valida::check($data, ['ticket_id', 'servicio_costo_id', 'unidad_id'], 'Verifique los campos obligatorios');

		try{
			$servicio = ServicioCosto::find($data['servicio_costo_id']);
			$ticket_detalle = new TicketDetalle;
			$ticket_detalle->ticket_id = $data['ticket_id'];
			$ticket_detalle->servicio_costo_id = $data['servicio_costo_id'];
			$ticket_detalle->unidad_id = $data['unidad_id'];
			$ticket_detalle->precio = $servicio->costo;
			$ticket_detalle->comentario = (isset($data['comentario'])) ? $data['comentario'] : '';
			$ticket_detalle->status = 0;
			$ticket_detalle->save();

			return Response::json($ticket_detalle, 200);
		} catch (PDOException $e) {
			// return Response::json($e, 400);
			return Response::json('Hubo un error al guardar el servicio', 400);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id){
		return Response::json(TicketDetalle::find($id), 200);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		$data = Request::input();
		try{
			$ticket_detalle = TicketDetalle::find($id);
			(isset($data['unidad_id'])) ? $ticket_detalle->unidad_id = $data['unidad_id'] : ''; // puede ser nulo
			(isset($data['comentario'])) ? $ticket_detalle->comentario = $data['comentario'] : ''; // puede ser nulo
			$ticket_detalle->save();

			return Response::json($ticket_detalle, 200);
		} catch (PDOException $e) {
			// return Response::json($e, 400);
			return Response::json('Hubo un error al editar el servicio', 400);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id){
		return Response::json('Hubo un error al eliminar el servicio', 400);
	}

	public function removerServicio($ticket_id, $ticket_detalle_id) {
		try{
			$ticket = Ticket::find($ticket_id);
			if($ticket->status == 0){
				$ticket_detalle = TicketDetalle::destroy($ticket_detalle_id);
				return Response::json($ticket_detalle, 200);
			}else{
				throw new Exception("EL ticket debe estar activo para poder eliminar servicios");
			}
		} catch (Throwable $e) {
			// return Response::json($e, 400);
			return Response::json('Hubo un error al eliminar el servicio', 400);
		}
	}

	/**
	 * Guarda el servicio como status terminado, status 1.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function servicioTerminado(Request $request){
		$data = Request::input();
		Valida::check($data, ['id'], 'No se especifico el ticket');
		try{
			$ticket_detalle = TicketDetalle::find($data['id']);
			if (is_null($ticket_detalle)) throw new Exception("Err", 1);
			$ticket_detalle->status = 1;
			$ticket_detalle->save();

			return Response::json($ticket_detalle, 200);
		} catch (Throwable $e) {
			return Response::json($e, 400);
			return Response::json('Hubo un error al concluir el servicio', 400);
		}
	}

	/**
	 * Reactiva un servicio como no terminado, status 0.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function reactivarServicio(Request $request){
		$data = Request::input();
		Valida::check($data, ['id'], 'No se especifico el ticket');
		try{
			$ticket_detalle = TicketDetalle::find($data['id']);
			if (is_null($ticket_detalle)) throw new Exception("Err", 1);
			$ticket_detalle->status = 0;
			$ticket_detalle->save();

			return Response::json($ticket_detalle, 200);
		} catch (Throwable $e) {
			// return Response::json($e, 400);
			return Response::json('Hubo un error al concluir el servicio', 400);
		}
	}
}
