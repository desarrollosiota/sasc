<?php

namespace App\Http\Controllers\v1;

use Request;
use Response;
use App\Http\Controllers\Controller;

use App\Http\Models\Notificacion;

class NotificacionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		@session_start();
		$notificaciones = Notificacion::where('usuario_id', $_SESSION['id'])
								->orderBy('id', 'desc')
								->take(10)
								->get();

		$no_leidas = Notificacion::where([
			['leida', 0], ['usuario_id', $_SESSION['id']]
		])->count();

		return Response::json([
			'notificaciones' => $notificaciones,
			'no_leidas' => $no_leidas
		], 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$notificacion = Notificacion::find($id);
		$notificacion->leida = 1;
		$notificacion->save();
		return Response::json([], 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
