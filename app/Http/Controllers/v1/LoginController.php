<?php

namespace App\Http\Controllers\v1;

use Valida;
use Hash;

use Request;
use Response;
use App\Http\Controllers\Controller;

use App\Http\Models\Usuario;

class LoginController extends Controller {

	/**
	 * Asigna variables de sesion
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function login(Request $request) {

		$data = Request::input();
		// Validando que $data contenga 'usuario' y 'password'
		Valida::check($data, ['usuario', 'password'], 'Llena los campos!');
		ini_set('session.cache_expire', 86400);
		ini_set('session.cookie_lifetime', 86400);
		ini_set('session.gc_maxlifetime', 86400);
		@session_start();
		# Buscar por usuario
		$sql = Usuario::where('usuario', $data['usuario'])->first();

		if (count((array)$sql) > 0) {
			# Checando password
			if (Hash::check($data['password'], $sql->password)) {
				# Guardando la sesion
				$_SESSION['id'] = $sql->id;
				$_SESSION['usuario'] = $sql->usuario;
				$_SESSION['nombre'] = $sql->nombre;
				$_SESSION['usuario_rol_id'] = $sql->usuario_rol_id;
				# Respuesta exitosa
				return Response::json([
					'usuario' => $sql->usuario,
					'usuario_rol_id' => $sql->usuario_rol_id,
				], 200);
			}
		}

		# Respuesta de error
		return Response::json($sql, 400);
	}

	/**
	 * Actualiza el id de sesion de socket
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 * 
	 */
	public function actualizarSocketId(Request $request) {
		$data = Request::input();
		// Validando que $data contenga 'usuario' y 'password'
		Valida::check($data, ['socket_id'], 'Error recibiendo socket_id');
		@session_start();
		$usuario = Usuario::find($_SESSION['id']);
		$usuario->socket_id = $data['socket_id'];
		$usuario->save();
		$_SESSION['socket_id'] = $data['socket_id'];
		return Response::json($usuario, 200);
	}

	/**
	 * Destruye la sesion
	 * @return \Illuminate\Http\Response
	 */
	public function logout() {
		@session_start();
		@session_destroy();
		return json_encode(true);
	}

}
