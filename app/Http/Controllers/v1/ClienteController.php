<?php

namespace App\Http\Controllers\v1;

use Request;
use Response;
use App\Http\Controllers\Controller;

use Valida;
use Notificaciones;

use App\Http\Models\Cliente;
use App\Http\Models\Contacto;
use App\Http\Models\Inegi_estado;
use App\Http\Models\Inegi_municipio;

class ClienteController extends Controller
{
	/**
	 * Metodo de TEST para el socket tipo y asi ue
	 */
	// public function sdf() {
		// 
		// 
	// }
	/**
	 * Muestra una lista de todos los usuarios y sus roles
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return Response::json(Cliente::get(), 200);
	}

	/**
	 * Almacena los usuarios en la DB, valida que la data contenga
	 * nombre, usuario, password y correo, tambien valida que el correo
	 * no sea repetido
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$data = Request::input();
		# Checando los inputs obligatorios
		Valida::check($data, ['nombre', 'rfc'], 'Nombre y RFC son obligatorios. Falta ');

		# Checando que no exista la direccion de correo
		if (Self::buscarRfc($data['rfc'])) {
			return Response::json('Este RFC ya existe en el sistema', 400);
		}

		# Todo correcto, insertamos data
		$nuevo_cliente = new Cliente;
		$nuevo_cliente->nombre = ucfirst($data['nombre']);
		$nuevo_cliente->rfc = $data['rfc'];
		$nuevo_cliente->nombre_fiscal = (isset($data['nombre_fiscal'])) ? $data['nombre_fiscal'] : '';
		$nuevo_cliente->direccion = (isset($data['direccion'])) ? ucfirst($data['direccion']) : '';
		$nuevo_cliente->telefono = (isset($data['telefono'])) ? $data['telefono'] : '';
		$nuevo_cliente->correo = (isset($data['correo'])) ? $data['correo'] : '';
		(isset($data['municipio'])) ? $nuevo_cliente->municipio_id = $data['municipio'] : ''; // puede ser nulo

		$nuevo_cliente->save();

		# Retorna el nuevo usuario insertado con su ID
		return Response::json($nuevo_cliente, 200);
	}

	/**
	 * Muestra un usuario especifico
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		# Retorna el usuario o un arreglo vacio
		$cliente = Cliente::find($id);
		if ($cliente != null && $cliente->municipio_id && $cliente->municipio_id != 0) {
			$cliente->estado_id = Inegi_municipio::find($cliente->municipio_id)->inegi_estado_id;
		}
		return Response::json($cliente, 200);
	}

	/**
	 * Actualiza un usuario especifico
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$data = Request::input();
		# Obteniendo el usuario a editar
		$cliente = Cliente::find($id);
		if (count((array)$cliente) == 0) {
			return Response::json('No existe el cliente', 400);
		}
		# Checar que no se repita el rfc en otro lado
		if (isset($data['rfc'])) {
			if (Self::buscarRfc($data['rfc'], $id)) {
				return Response::json('Este rfc ya está registrado', 400);
			}
		}
		# Actualizando datos
		(isset($data['nombre'])) ? $cliente->nombre = $data['nombre'] : '';
		(isset($data['rfc'])) ? $cliente->rfc = $data['rfc'] : '';
		(isset($data['nombre_fiscal'])) ? $cliente->nombre_fiscal = $data['nombre_fiscal'] : '';
		(isset($data['direccion'])) ? $cliente->direccion = $data['direccion'] : '';
		(isset($data['municipio_id'])) ? $cliente->municipio_id = $data['municipio_id'] : '';
		(isset($data['telefono'])) ? $cliente->telefono = $data['telefono'] : '';
		(isset($data['correo'])) ? $cliente->correo = $data['correo'] : '';
		$cliente->save();

		return Response::json([], 200);
	}

	/**
	 * Borra un usuario especifico
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		# Retorna 1 en caso de haber borrado algo o 0 en caso contrario
		return Response::json(Cliente::destroy($id), 200);
	}

	/**
	 * Funcion que nos ayuda a ver si el rfc existe o no
	 * @param  text $rfc [El rfc con el que vamos a buscar]
	 * @param  int $id [Si se envia un id, este se usara para exluirlo de la consulta]
	 * @return bool
	 */
	private function buscarRfc($rfc, $id = -1) {
		$checar_rfc = Cliente::where('rfc', $rfc);
		if ($id != -1) $checar_rfc->where('id', '!=', $id);
		if (count((array)$checar_rfc->first()) > 0) return true;
		return false;
	}

	public function contactos_get(){
		$id = $_POST['id_cliente'] ?? '';
		if($id){
			$cliente = Cliente::find($id);
			if($cliente){
				$cliente->contactos;
			}
		}else{
			return Response::json('No se especifico el cliente',400);
		}
		return Response::json($cliente,200);
	}

	public function agregar_contacto(){
		$data = Request::input();
		# Checando los inputs obligatorios
		Valida::check($data, ['nombre','id_cliente']);
		$nuevo_contacto = new Contacto;
		$nuevo_contacto->nombre = ucfirst($data['nombre']);
		$nuevo_contacto->cliente_id = $data['id_cliente'];
		isset($data['telefono']) ? $nuevo_contacto->telefono = $data['telefono'] : '';
		isset($data['correo']) ? $nuevo_contacto->correo = $data['correo'] : '';
		isset($data['puesto']) ? $nuevo_contacto->puesto = $data['puesto'] : '';
		$nuevo_contacto->save();
		return Response::json($nuevo_contacto,200);
	}

	public function editar_contacto(){
		$data = Request::input();
		Valida::check($data, ['nombre','id_contacto']);
		$contacto = Contacto::find($data['id_contacto']);
		$contacto->nombre = $data['nombre'];
		isset($data['telefono']) ? $contacto->telefono = $data['telefono'] : '';
		isset($data['correo']) ? $contacto->correo = $data['correo'] : '';
		isset($data['puesto']) ? $contacto->puesto = $data['puesto'] : '';
		$contacto->save();
		return Response::json($contacto,200);
	}

	public function datos_contacto(){
		$data = Request::input();
		return Response::json(Contacto::find($data['id_contacto']), 200);
	}

	public function eliminar_contacto(Request $request) {
		$data = Request::input();
		Valida::check($data, ['id_contacto']);
		return Response::json(Contacto::destroy($data['id_contacto']), 200);
	}

}
