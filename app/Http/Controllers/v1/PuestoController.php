<?php

namespace App\Http\Controllers\v1;

use Request;
use Response;
use App\Http\Controllers\Controller;

use Valida;

use App\Http\Models\Puesto;

class PuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Puesto::get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Request::input();
        if (is_null($data['nombre'])) return Response::json("Concepto es obligatorio", 400);
        $puesto = new Puesto;
        $puesto->nombre = $data['nombre'];
        $puesto->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : '';
        $puesto->save();
        return Response::json($puesto, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Response::json(Puesto::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Request::input();
        if (is_null($data['nombre'])) return Response::json("Concepto es obligatorio", 400);
        $puesto = Puesto::find($id);
        $puesto->nombre = $data['nombre'];
        $puesto->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : '';
        $puesto->save();
        return Response::json($puesto, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            # Retorna 1 en caso de haber borrado algo o 0 en caso contrario
            $sdf = Puesto::destroy($id);
            return Response::json($sdf, 200);
        }catch(Throwable $e){
            return Response::json('No se puede eliminar el servicio', 400);
        }
    }
}
