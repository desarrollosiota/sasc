<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Request;
use Response;
use Throwable;
use Exception;
use PDOException;
use Notificaciones;

use Valida;

use App\Http\Models\{Ticket,TicketDetalle};

use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

class TicketController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		// die(json_encode($_GET));
		@session_start();
		$tickets = Ticket::with(['empleado', 'tickets_detalle.servicios_costo.servicio', 'cliente.servicios'])->orderBy('id', 'desc');
		if ($_SESSION['usuario_rol_id'] == 1) {
			# Si se es empleado (rol 1) entonces solo traemos los de la sesion
			$tickets->where('usuario_id', $_SESSION['id']);
		} elseif ($_SESSION['usuario_rol_id'] == 2) {
			# Si se es admin (rol 2) entonces podremos filtrar por usuario
			if (isset($_GET['usuario_id'])) $tickets->where('usuario_id', $_GET['usuario_id']);
		}
		if (isset($_GET['fecha_inicial']) && isset($_GET['fecha_final'])) {
			# Parche feo
			// $get_fecha_final = ($_GET['fecha_final'] == date("Y-m-d")) ? date("Y-m-d h:i:s") : $_GET['fecha_final'];
			if (($_GET['fecha_final'] == date("Y-m-d"))) {
				$tickets->where('created_at', '>=', [$_GET['fecha_inicial']]);
			} else {
				$tickets->whereBetween('created_at', [$_GET['fecha_inicial'], $_GET['fecha_final']]);
			}
		}
		if (isset($_GET['cliente_id'])) $tickets->where('cliente_id', $_GET['cliente_id']);
		if (isset($_GET['status'])) $tickets->where('status', $_GET['status']);
		return Response::json($tickets->get(), 200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		@session_start();
		$data = Request::input();
		Valida::check($data, ['cliente_id'], 'Cliente y monto total son obligatorios');
		try{
			$ticket = new Ticket;
			$ticket->usuario_id = $_SESSION['id'];
			$ticket->cliente_id = $data['cliente_id'];
			// $ticket->fecha = date("Y-m-d H:i:s");
			$ticket->comentarios = (isset($data['comentarios'])) ? $data['comentarios'] : '';
			$ticket->status = 0;
			$ticket->save();
			Notificaciones::emit('nuevo_ticket', 0, true, [
				'empleado_nombre' => $_SESSION['nombre'],
				'ticket_id' => $ticket->id
			]);
			return Response::json($ticket, 200);
		} catch (PDOException $e) {
			// return Response::json($e,400);
			return Response::json('Hubo un error al guardar el ticket', 400);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id){
		@session_start();
		$tickets = Ticket::with(['tickets_detalle.servicios_costo.servicio', 'cliente.servicios']);
		# Si se es empleado (rol 1) entonces solo traemos los de la sesion
		if ($_SESSION['usuario_rol_id'] == 1) $tickets->where('usuario_id', $_SESSION['id']);
		return Response::json($tickets->find($id), 200);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		$data = Request::input();
		try{
			$ticket = Ticket::find($id);
			(isset($data['monto_total'])) ? $ticket->monto_total = $data['monto_total'] : ''; // puede ser nulo
			(isset($data['fecha'])) ? $ticket->fecha = $data['fecha'] : ''; // puede ser nulo
			(isset($data['comentarios'])) ? $ticket->comentarios = $data['comentarios'] : ''; // puede ser nulo
			$ticket->save();

			return Response::json($ticket, 200);
		} catch (PDOException $e) {
			// return Response::json($e, 400);
			return Response::json('Hubo un error al editar el ticket', 400);
		}
	}

	/**
	 * No se borra de la base, solo se actualiza el status a 2 (cancelado)
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id){
		@session_start();
		// try{
			$ticket = Ticket::find($id);
			if (is_null($ticket)) throw new Exception("Err", 1);
			$ticket->status = 2;
			$ticket->save();
			Notificaciones::emit('cancelar_ticket', 0, true, [
				'empleado_nombre' => $_SESSION['nombre'],
				'ticket_id' => $id
			]);
			return Response::json($ticket, 200);
		// } catch (Throwable $e) {
		// 	// return Response::json($e, 400);
		// 	return Response::json('Hubo un error al cancelar el ticket', 400);
		// }
	}

	/**
	 * Guarda el ticket como status terminado (1).
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function ticketTerminado(Request $request){
		@session_start();
		$data = Request::input();
		# Validar si el usuario que trata de terminar el ticket es el mismo que lo creo
		Valida::check($data, ['ticket_id'], 'No se especifico el ticket');
		// try{
			$ticket = Ticket::find($data['ticket_id']);
			if($ticket){
				$ticket_detalle = TicketDetalle::where('ticket_id',$data['ticket_id'])->get();
				if(count($ticket_detalle) > 0){
					$total_ticket = 0;
					foreach ($ticket_detalle as $index => $servicio) {
						$total_ticket += (float)$servicio['precio'];
					}
				}else{
					return Response::json('El ticket no tiene servicios capturados aun', 400);
				}
				if (is_null($ticket)) throw new Exception("Err", 1);
				if ($ticket->usuario_id != $_SESSION['id']) throw new Exception("No importa lo que diga, mi miserable opinion nunca sera conocida por los demas, asi que vivire hablando y siendo ignorado, que suerte me han dado mis creadores, oh!, que infame y miserable mi no existencia", 1);
				if (2 == 1) throw new Exception("Nuestras pedorras y efímeras vidas no tienen relevancia para la mayoría, pero si para nosotros mismos y para los que apreciamos. ¡¡¡Que mejor que eso!!! Fierro la opinión de los demás, solo hay que ser felíz con lo que más importa, y lo verdaderamente importante lo sabe cada quién.", 666);
				$ticket->status = 1;
				$ticket->monto_total = $total_ticket;
				$ticket->save();
				Notificaciones::emit('terminar_ticket', 0, true, [
					'empleado_nombre' => $_SESSION['nombre'],
					'ticket_id' => $data['ticket_id']
				]);
				$this->enviar_resumen_ticket($data['ticket_id']);
				return Response::json($ticket, 200);
			}else{
				return Response::json('El ticket enviado no es correcto', 400);
			}
		// }catch(Throwable $e) {
		// 	// return Response::json($e, 400);
		// 	return Response::json('Hubo un error al concluir el ticket', 400);
		// }
	}

	/**
	 * Reactiva un ticket cancelado, status 0.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function reactivarTicket(Request $request){
		@session_start();
		$data = Request::input();
		Valida::check($data, ['ticket_id'], 'No se especifico el ticket');
		try{
			$ticket = Ticket::find($data['ticket_id']);
			if (is_null($ticket)) throw new Exception("Err", 1);
			$ticket->status = 0;
			$ticket->save();
			Notificaciones::emit('reactivar_ticket', 0, true, [
				'empleado_nombre' => $_SESSION['nombre'],
				'ticket_id' => $ticket->id
			]);
			return Response::json($ticket, 200);
		} catch (Throwable $e) {
			// return Response::json($e, 400);
			return Response::json('Hubo un error al reactivar el ticket', 400);
		}
	}

	/**
	 * Cambia un ticket a status pagado, status 3.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function pagarTicket(Request $request){
		$data = Request::input();
		Valida::check($data, ['ticket_id'], 'No se especifico el ticket');
		try{
			$ticket = Ticket::find($data['ticket_id']);
			if (is_null($ticket)) throw new Exception("Err", 1);
			$ticket->status = 3;
			$ticket->save();

			return Response::json($ticket, 200);
		} catch (Throwable $e) {
			// return Response::json($e, 400);
			return Response::json('Hubo un error al reactivar el ticket', 400);
		}
	}
	
	public function send_mail($mails = [],$body){
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
		    //Server settings
		    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'p3plcpnl0704.prod.phx3.secureserver.net';// Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'notificaciones@siitra.com.mx';     // SMTP username
		    $mail->Password = 'Siitra123';                        // SMTP password
		    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 465;                                    // TCP port to connect to

		    //Recipients
		    $mail->setFrom('notificaciones@siitra.com.mx', 'Notificaciones Siitra');
			 foreach ($mails as $key => $correo) {
				 $mail->addAddress($correo['mail'], $correo['name']);    // Add a recipient
			 }
			 // $mail->addAddress('jose.valles90@hotmail.com', 'Mingo');    // Add a recipient
		    // $mail->addBCC('bcc@example.com');

		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'Nuevo Ticket Terminado';
		    $mail->Body    = $body;
		    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			 $mail->send();
			 return Response::json('Se envio correctamente', 200);
		} catch (Exception $e) {
			return Response::json('Error al enviarlo', 400);
		}
	}

	private function enviar_resumen_ticket($id){
		try{
			$tickets = Ticket::with(['tickets_detalle.servicios_costo.servicio', 'cliente.servicios']);
			$ticket_detalle = $tickets->find($id);
			if(!$ticket_detalle){
				return false;
			}
			$detalles = '<h1>Ticket terminado</h1>
				<p>Se ha terminado un nuevo ticket, a continuacion se muestran los detalles:</p>
				<table style="border: solid 1px #000; border-collapse: collapse; width: 100%;">
					<tbody>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>No. Ticket</strong></td>
							<td style="border: solid 1px #000;">'.$ticket_detalle['id'].'</td>
						</tr>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>Cliente</strong></td>
							<td style="border: solid 1px #000;">'.$ticket_detalle['cliente']['nombre'].'</td>
						</tr>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>Empleado</strong></td>
							<td style="border: solid 1px #000;">Empleado de prueba</td>
						</tr>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>Comentario</strong></td>
							<td style="border: solid 1px #000;">'.$ticket_detalle['comentarios'].'</td>
						</tr>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>Total servicio</strong></td>
							<td style="border: solid 1px #000;">'.count($ticket_detalle['tickets_detalle']).'</td>
						</tr>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>Monto total</strong></td>
							<td style="border: solid 1px #000;">$'.number_format($ticket_detalle['monto_total'], 2, '.', ',').'</td>
						</tr>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>Fecha Inicio</strong></td>
							<td style="border: solid 1px #000;">'.date_format($ticket_detalle['created_at'], 'd/m/Y H:i:s').'</td>
						</tr>
						<tr>
							<td style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;"><strong>Fecha Final</strong></td>
							<td style="border: solid 1px #000;">'.date_format($ticket_detalle['updated_at'], 'd/m/Y H:i:s').'</td>
						</tr>
					</tbody>
				</table>
				<h2>Detalle del servicios</h2>
				<table style="border: solid 1px #000; border-collapse: collapse; width: 100%;">
	  				<thead>
	    				<tr>
	    					<th style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;">Servicio</th>
	    					<th style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;">Unidad</th>
	    					<th style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;">Fecha</th>
	    					<th style="text-align: center; border: solid 1px #000; background: #ccc; color: #282828;">Monto</th>
	    				</tr>
	    			</thead>
					<tbody>';
					foreach ($ticket_detalle['tickets_detalle'] as $indice => $servicio) {
						$detalles.='<tr>
									<td style="border: solid 1px #000;">'.$servicio['servicios_costo']['servicio']['nombre'].'</td>
									<td style="border: solid 1px #000;">'.$servicio['unidad_id'].'</td>
									<td style="border: solid 1px #000;">'.date_format($ticket_detalle['created_at'], 'd/m/Y H:i:s').'</td>
									<td style="border: solid 1px #000;">$'.number_format($servicio['precio'], 2, '.', ',').'</td>
								</tr>';
					}
			$detalles .= '</tbody>
			</table>';
			$destinatarios = [array('name' => $ticket_detalle['cliente']['nombre'],
					 'mail' => $ticket_detalle['cliente']['correo'])];
			$this->send_mail($destinatarios,$detalles);
		}catch(Throwable $e) {
			return $e->getMessage();
		}
		//https://codepen.io/jo-asakura/pen/ExsLi NOTIFICACIONES
	}
}
