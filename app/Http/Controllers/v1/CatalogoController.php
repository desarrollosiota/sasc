<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;

use App\Http\Models\Servicio;
use App\Http\Models\Puesto;
use App\Http\Models\EstadoCivil;


class CatalogoController extends Controller
{
    public static function obtenerCatalogos() {
        return [
            'servicio' => Servicio::get(),
            'puesto' => Puesto::get(),
            'estado_civil' => EstadoCivil::get()
        ];
    }
}
