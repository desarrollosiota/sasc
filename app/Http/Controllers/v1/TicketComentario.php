<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Models\TicketComentarios AS Coment;
use App\Http\Models\Ticket;
use Request;
use Response;
use PDOException;
use Valida;
use Notificaciones;

class TicketComentario extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      @session_start();
      $data = Request::input();
      Valida::check($data, ['ticket_id','comentario'], 'Se debe seleccionar un ticket y capturar un comentario');
      try{
         $comentario = new Coment;
         $comentario->ticket_id = $data['ticket_id'];
         $comentario->usuario_id = $_SESSION['id'];
         $comentario->comentario = $data['comentario'];
         $comentario->save();
        //  $notificar_admins = ($_SESSION['usuario_rol_id'] == 1) ? true : false;
         if ($_SESSION['usuario_rol_id'] == 2) {
             $tipo_noti = 'admin_comenta_ticket';
             $notificar_admins = false;
             $ticket = Ticket::find($data['ticket_id']);
             $notificar_a = $ticket->usuario_id;
             
            //  return [$tipo_noti, $notificar_admins, $notificar_a];
            } else {
                $tipo_noti = 'empleado_comenta_ticket';
                $notificar_admins = true;
                $notificar_a = 0;
                // return [$tipo_noti, $notificar_admins, $notificar_a];
         }
         $sss = Notificaciones::emit($tipo_noti, $notificar_a, $notificar_admins, [
            'usuario_nombre' => $_SESSION['nombre'],
            'ticket_id' => $data['ticket_id']
        ]);
         return Response::json($sss,200);
      } catch (PDOException $e) {
         // return Response::json($e,400);
         return Response::json('No se pudo guardar el comentario, intentelo de nuevo',400);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
      $usuario = Ticket::with('empleado')->where('id', $id)->get();
      $comentarios = Coment::with('usuario')->where('ticket_id', $id)->orderBy('created_at', 'desc')->get();
      foreach ($comentarios as $key => $value) {
         $comentarios[$key]->usuario->empleado;
      }
      if($usuario){
         return Response::json(array('usuario' => $usuario,'comentarios' => $comentarios),200);
      }else{
         return Response::json('El ticket no existe',400);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
