<?php

namespace App\Http\Controllers\v1;

use Request;
use Response;
use Throwable;
use PDOException;
use App\Http\Controllers\Controller;

use Valida;
use DB;

use App\Http\Models\Servicio;

class ServicioController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		return Response::json(Servicio::get(), 200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		$data = Request::input();
		Valida::check($data, ['nombre'], 'Los campos son obligatorios');
		$servicio = new Servicio;
		$servicio->nombre = $data['nombre'];
		$servicio->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : '';
		$servicio->save();
		return Response::json($servicio, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id){
		return Response::json(Servicio::find($id), 200);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		$data = Request::input();
		if (is_null($data['nombre'])) return Response::json("Concepto es obligatorio", 400);
		$servicio = Servicio::find($id);
		$servicio->nombre = $data['nombre'];
		$servicio->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : '';
		$servicio->save();
		return Response::json($servicio, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id){
		# Retorna 1 en caso de haber borrado algo o 0 en caso contrario
		try{
			try{
				$sdf = Servicio::destroy($id);
				return Response::json($sdf, 200);
			} catch(PDOException $e) {
				return Response::json('Este servicio tiene registros ligados', 400);
			} 
		} catch(Throwable $e) {
			return Response::json('No se puede eliminar el servicio', 400);
		}
	}
}
