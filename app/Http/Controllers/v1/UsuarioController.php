<?php

namespace App\Http\Controllers\v1;

use Request;
use Response;
use App\Http\Controllers\Controller;

use Valida;
use Hash;

use App\Http\Models\Usuario;
use App\Http\Models\Empleado;
use App\Http\Models\Puesto;
use App\Http\Models\EstadoCivil;
use App\Http\Models\Inegi_estado;
use App\Http\Models\Inegi_municipio;

class UsuarioController extends Controller
{
	/**
	 * Muestra una lista de todos los usuarios y sus roles
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return Response::json(Usuario::with('usuario_rol')->get(), 200);
	}

	/**
	 * Almacena los usuarios en la DB, valida que la data contenga
	 * nombre, usuario, password y correo, tambien valida que el correo
	 * no sea repetido
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$data = Request::input();
		# Checando los inputs obligatorios
		Valida::check($data, ['nombre', 'usuario', 'password', 'correo', 'ap_paterno', 'ap_materno','puesto_id', 'usuario_rol_id']);
		# Checando que no exista la direccion de correo
		if (Self::buscarCorreo($data['correo'])) {
			return Response::json('Este correo ya está registrado', 400);
		}
		# Checando que no exista el usuario introducido
		if (Self::buscarUsuario($data['usuario'])) {
			return Response::json('Este nombre de usuario ya está registrado', 400);
		}
		# Todo correcto, insertamos data
		$nuevo_usuario = new Usuario;
		$nuevo_usuario->nombre = $data['nombre'];
		$nuevo_usuario->usuario = $data['usuario'];
		$nuevo_usuario->password = Hash::make($data['password']);
		$nuevo_usuario->correo = $data['correo'];
		$nuevo_usuario->estado = 1;
		$nuevo_usuario->usuario_rol_id = $data['usuario_rol_id'];
		$nuevo_usuario->save();

		$nuevo_empleado = new Empleado;
		$nuevo_empleado->usuario_id = $nuevo_usuario->id;
		$nuevo_empleado->nombre = ucfirst($data['nombre']);
		$nuevo_empleado->ap_paterno = ucfirst($data['ap_paterno']);
		$nuevo_empleado->ap_materno = ucfirst($data['ap_materno']);
		isset($data['fecha_nacimiento']) ? $nuevo_empleado->fecha_nacimiento = $data['fecha_nacimiento'] : '';
		isset($data['estado_civil_id']) ? $nuevo_empleado->estado_civil_id = $data['estado_civil_id'] : '';
		isset($data['direccion']) ? $nuevo_empleado->direccion = ucfirst($data['direccion']) : '';
		isset($data['municipio_id']) ? $nuevo_empleado->municipio_id = $data['municipio_id'] : '';
		isset($data['telefono']) ? $nuevo_empleado->telefono = $data['telefono'] : '';
		isset($data['puesto_id']) ? $nuevo_empleado->puesto_id = $data['puesto_id'] : '';
		$nuevo_empleado->save();

		# Retorna el nuevo usuario insertado con su ID
		return Response::json($nuevo_usuario, 200);
	}

	/**
	 * Muestra un usuario especifico
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		# Retorna el usuario o un arreglo vacio
		$usuario = Usuario::with('usuario_rol')->find($id);
		if($usuario->empleado != null && $usuario->empleado->municipio_id != null && $usuario->empleado->municipio_id != 0){
			$usuario->empleado->estado_id = Inegi_municipio::find($usuario->empleado->municipio_id)->inegi_estado_id;
		}
		return Response::json($usuario, 200);
	}

	/**
	 * Actualiza un usuario especifico
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$data = Request::input();
		# Obteniendo el usuario a editar
		$usuario = Usuario::find($id);
		if (count((array)$usuario) == 0) {
			return Response::json('No existe el usuario', 400);
		}
		# Checar que no se repita el correo en otro lado
		if (isset($data['correo'])) {
			if (Self::buscarCorreo($data['correo'], $id)) {
				return Response::json('Este correo ya está registrado', 400);
			}
		}
		# Checar que no se repita el usuario
		if (isset($data['usuario'])) {
			if (Self::buscarUsuario($data['usuario'], $id)) {
				return Response::json('Este nombre de usuario ya está registrado', 400);
			}
		}
		$empleado = Empleado::where('usuario_id', $id)->first();
		# Actualizando datos
		(isset($data['nombre'])) ? $usuario->nombre = $data['nombre'] : '';
		(isset($data['usuario'])) ? $usuario->usuario = $data['usuario'] : '';
		(isset($data['password']) && $data['password'] != '') ? $usuario->password = Hash::make($data['password']) : '';
		(isset($data['correo'])) ? $usuario->correo = $data['correo'] : '';
		(isset($data['usuario_rol_id'])) ? $usuario->usuario_rol_id = $data['usuario_rol_id'] : '';

		isset($data['ap_paterno']) ? $empleado->ap_paterno = $data['ap_paterno'] : '';
		isset($data['ap_materno']) ? $empleado->ap_materno = $data['ap_materno'] : '';
		isset($data['fecha_nacimiento']) ? $empleado->fecha_nacimiento = $data['fecha_nacimiento'] : '';
		isset($data['estado_civil_id']) ? $empleado->estado_civil_id = $data['estado_civil_id'] : '';
		isset($data['direccion']) ? $empleado->direccion = $data['direccion'] : '';
		isset($data['municipio_id']) ? $empleado->municipio_id = $data['municipio_id'] : '';
		isset($data['telefono']) ? $empleado->telefono = $data['telefono'] : '';
		isset($data['puesto_id']) ? $empleado->puesto_id = $data['puesto_id'] : '';
		$usuario->save();
		$empleado->save();


		$usuario->save();
		return Response::json($data, 200);
	}

	/**
	 * Borra un usuario especifico
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		# Retorna 1 en caso de haber borrado algo o 0 en caso contrario
		return Response::json(Usuario::destroy($id), 200);
	}

	/**
	 * Busca los puestos de la empresa
	 * @return \Illuminate\Http\Response
	 */
	public function puestos(){
		return Response::json(Puesto::OrderBy('nombre')->get(),200);
	}

	/**
	 * Busca los puestos de la empresa
	 * @return \Illuminate\Http\Response
	 */
	public function usuarioActual(){
		@session_start();
		return Response::json(Self::show($_SESSION['id']),200);
	}

	/**
	 * Busca los estados civiles
	 * @return \Illuminate\Http\Response
	 */
	public function estadosCiviles(){
		return Response::json(EstadoCivil::get(),200);
	}

	/**
	 * Funcion que nos ayuda a ver si el correo existe o no
	 * @param  string $correo [El correo con el que vamos a buscar]
	 * @param  int $id [Si se envia un id, este se usara para exluirlo de la consulta]
	 * @return bool
	 */
	private function buscarCorreo($correo, $id = -1) {
		$checar_correo = Usuario::where('correo', $correo);
		if ($id != -1) $checar_correo->where('id', '!=', $id);
		if (count((array)$checar_correo->first()) > 0) return true;
		return false;
	}

	/**
	 * Funcion que nos ayuda a ver si el nombre de usuario existe o no
	 * @param  string $usuario [El usuario con el que vamos a buscar]
	 * @param  int $id [Si se envia un id, este se usara para exluirlo de la consulta]
	 * @return bool
	 */
	private function buscarUsuario($usuario, $id = -1) {
		$checar_usuario = Usuario::where('usuario', $usuario);
		if ($id != -1) $checar_usuario->where('id', '!=', $id);
		if (count((array)$checar_usuario->first()) > 0) return true;
		return false;
	}


	/**
	 * Actualiza el perfil del usuario
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function actualizarPerfil(Request $request) {
		@session_start();
		$data = Request::input();
		# Obteniendo el usuario a editar
		$usuario = Usuario::find($_SESSION['id']);
		if (count((array)$usuario) == 0) {
			return Response::json('No existe el usuario', 400);
		}
		# Checar que no se repita el correo en otro lado
		if (isset($data['correo'])) {
			if (Self::buscarCorreo($data['correo'], $_SESSION['id'])) {
				return Response::json('Este correo ya está registrado', 400);
			}
		}
		# Checar que no se repita el usuario
		if (isset($data['usuario'])) {
			if (Self::buscarUsuario($data['usuario'], $_SESSION['id'])) {
				return Response::json('Este nombre de usuario ya está registrado', 400);
			}
		}
		$empleado = Empleado::where('usuario_id', $_SESSION['id'])->first();
		# Actualizando datos
		(isset($data['nombre'])) ? $usuario->nombre = $data['nombre'] : '';
		(isset($data['usuario'])) ? $usuario->usuario = $data['usuario'] : '';
		(isset($data['password']) && $data['password'] != '') ? $usuario->password = Hash::make($data['password']) : '';
		(isset($data['correo'])) ? $usuario->correo = $data['correo'] : '';
		(isset($data['usuario_rol_id'])) ? $usuario->usuario_rol_id = $data['usuario_rol_id'] : '';

		isset($data['ap_paterno']) ? $empleado->ap_paterno = $data['ap_paterno'] : '';
		isset($data['ap_materno']) ? $empleado->ap_materno = $data['ap_materno'] : '';
		isset($data['fecha_nacimiento']) ? $empleado->fecha_nacimiento = $data['fecha_nacimiento'] : '';
		isset($data['estado_civil_id']) ? $empleado->estado_civil_id = $data['estado_civil_id'] : '';
		isset($data['direccion']) ? $empleado->direccion = $data['direccion'] : '';
		isset($data['municipio_id']) ? $empleado->municipio_id = $data['municipio_id'] : '';
		isset($data['telefono']) ? $empleado->telefono = $data['telefono'] : '';
		isset($data['puesto_id']) ? $empleado->puesto_id = $data['puesto_id'] : '';
		$usuario->save();
		$empleado->save();


		$usuario->save();
		return Response::json($data, 200);
	}


	public function buscarEstados(){
		return Response::json(Inegi_estado::OrderBy('nombre')->get(),200);
	}

	public function buscarMunicipios($id_estado=0){
		$id_estado = $_POST['id_estado'];
		return Response::json(Inegi_municipio::where('inegi_estado_id',$id_estado)->get(),200);
	}
}
