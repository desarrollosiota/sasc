<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuarioSocketId extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuario_socket_id', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('usuario_id');
            $table->string('socket_id',100);
            $table->timestamps();
            
            ///////INDICE/////////
            $table->foreign('usuario_id')
                    ->references('id')
                    ->on('usuario')
                    ->onDelete('no action')
                    ->onUpdate('no action');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('usuario_socket_id');
	}
}
