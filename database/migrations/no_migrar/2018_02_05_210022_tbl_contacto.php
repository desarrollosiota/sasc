<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblContacto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->string('telefono',20);
            $table->string('correo',60);
            $table->string('puesto',150);
            $table->unsignedInteger('cliente_id');
            $table->timestamps();
            
            ///////INDICE/////////
            $table->foreign('cliente_id')
                    ->references('id')
                    ->on('cliente')
                    ->onDelete('restrict')
                    ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacto');
    }
}
