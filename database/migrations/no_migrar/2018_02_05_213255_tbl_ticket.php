<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('cliente_id');
            $table->text('comentarios');
            $table->timestamp('fecha')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->decimal('monto_total',10,2);
            $table->tinyInteger('status');
            $table->timestamps();
            
            ///////////INDICES//////////////
            $table->foreign('cliente_id')
                    ->references('id')
                    ->on('cliente')
                    ->onDelete('restrict')
                    ->onUpdate('no action');

            $table->foreign('usuario_id')
                    ->references('id')
                    ->on('usuario')
                    ->onDelete('restrict')
                    ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket');
    }
}
