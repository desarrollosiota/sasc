<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblTicketDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ticket_id');
            $table->unsignedInteger('servicio_costo_id');
            $table->unsignedInteger('unidad_id')->nullable();
            $table->decimal('precio',8,2);
            $table->text('comentario');
            $table->tinyInteger('status');
            $table->timestamps();

            ////////INDICES///////////
            $table->foreign('ticket_id')
                    ->references('id')
                    ->on('ticket')
                    ->onDelete('restrict')
                    ->onUpdate('no action');

            $table->foreign('servicio_costo_id')
                    ->references('id')
                    ->on('cliente_servicio_costo')
                    ->onDelete('restrict')
                    ->onUpdate('no action');

            $table->foreign('unidad_id')
                    ->references('id')
                    ->on('unidad')
                    ->onDelete('restrict')
                    ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_detalle');
    }
}
