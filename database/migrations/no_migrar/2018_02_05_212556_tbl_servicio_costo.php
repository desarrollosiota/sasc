<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblServicioCosto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_costo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cliente_id');
            $table->unsignedInteger('servicio_id');
            $table->decimal('precio',8,2);
            $table->text('comentarios');
            $table->timestamps();

            /////////INDICES//////////
            $table->unique(['cliente_id','servicio_id']);
            
            $table->foreign('cliente_id')
                    ->references('id')
                    ->on('cliente')
                    ->onDelete('restrict')
                    ->onUpdate('no action');

            $table->foreign('servicio_id')
                    ->references('id')
                    ->on('servicio')
                    ->onDelete('restrict')
                    ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_costo');
    }
}
