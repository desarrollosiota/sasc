<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblEmpleado extends Migration
{
    /**
     * Run the migrations.
     * Descripción.- Tabla para tener un concentrado de los empleados de la empresa
     * @return void
     */
    public function up()
    {
        Schema::create('empleado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->string('ap_paterno',30);
            $table->string('ap_materno',30);
            $table->date('fecha_nacimiento')->nullable();
            $table->unsignedTinyInteger('estado_civil_id')->nullable();
            $table->string('direccion',255)->nullable();
            $table->integer('municipio_id')->nullable();
            $table->string('telefono',20)->nullable();
            $table->unsignedTinyInteger('puesto_id')->nullable();
            $table->unsignedInteger('usuario_id')->nullable();
            $table->timestamps();

            ////////////INDICES///////////////
            $table->foreign('estado_civil_id')
                    ->references('id')
                    ->on('estado_civil')
                    ->onDelete('restrict')
                    ->onUpdate('no action');

            $table->foreign('puesto_id')
                    ->references('id')
                    ->on('puesto')
                    ->onDelete('restrict')
                    ->onUpdate('no action');

            $table->foreign('usuario_id')
                    ->references('id')
                    ->on('usuario')
                    ->onDelete('cascade')
                    ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleado');
    }
}
