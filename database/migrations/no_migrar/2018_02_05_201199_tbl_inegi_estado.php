<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblInegiEstado extends Migration
{
    /**
     * Run the migrations.
     * Descripción.- Tabla para definir los roles que van a existir en la plataforma
     * @return void
     */
    public function up()
    {
        Schema::create('inegi_estado', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('nombre',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inegi_estado');
    }
}
