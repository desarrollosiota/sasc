<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TipoNotificaciones extends Migration
{
    /**
     * Run the migrations.
     * Descripción.- Tabla para definir los roles que van a existir en la plataforma
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_notificaciones', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('class',255)->nullable();
            $table->string('titulo',255);
            $table->string('cuerpo',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_notificaciones');
    }
}
