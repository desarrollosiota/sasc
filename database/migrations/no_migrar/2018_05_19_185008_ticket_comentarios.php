<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketComentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ticket_comentarios', function (Blueprint $table) {
         $table->increments('id');
         $table->unsignedInteger('ticket_id');
         $table->unsignedInteger('usuario_id');
         $table->text('comentario');
         $table->timestamps();

         ////////INDICES///////////
         $table->foreign('ticket_id')
                  ->references('id')
                  ->on('ticket')
                  ->onDelete('restrict')
                  ->onUpdate('no action');

         $table->foreign('usuario_id')
                  ->references('id')
                  ->on('usuario')
                  ->onDelete('restrict')
                  ->onUpdate('no action');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ticket_comentarios');
    }
}
