<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',150);
            $table->string('usuario',30);
            $table->string('password',64);
            $table->string('correo',100);
            $table->string('socket_id',100)->nullable();
            $table->tinyInteger('estado');
            $table->unsignedTinyInteger('usuario_rol_id');
            $table->timestamps();

            ///////////INDICES////////////
            $table->unique('usuario');

            $table->foreign('usuario_rol_id')
                    ->references('id')
                    ->on('usuario_rol')
                    ->onDelete('restrict')
                    ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
