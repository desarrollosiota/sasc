<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255);
            $table->string('rfc',20);
            $table->string('nombre_fiscal',255);
            $table->string('direccion',255);
            $table->integer('municipio_id');
            $table->string('telefono',20);
            $table->string('correo',60);
            $table->timestamps();

            ////////INDICE//////////
            $table->unique('rfc');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
