<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblInegiMunicipio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inegi_municipio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',30);
            $table->unsignedInteger('inegi_estado_id');
            $table->timestamps();

            ///////////INDICES////////////
            $table->foreign('inegi_estado_id')
                    ->references('id')
                    ->on('inegi_estado')
                    ->onDelete('restrict')
                    ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inegi_municipio');
    }
}
