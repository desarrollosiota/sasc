<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notificaciones extends Migration
{
	/**
	 * Run the migrations.
	 * Descripción.- Tabla para definir los roles que van a existir en la plataforma
	 * @return void
	 */
	public function up()
	{
		Schema::create('notificaciones', function (Blueprint $table) {
			$table->Increments('id');
			$table->unsignedInteger('usuario_id');
			$table->unsignedInteger('tipo_notificacion_id');
			$table->string('metadata',1020)->nullable();
			$table->tinyInteger('leida');
			$table->timestamps();

			$table->foreign('usuario_id')
					->references('id')
					->on('usuario')
					->onDelete('restrict')
					->onUpdate('no action');

			$table->foreign('tipo_notificacion_id')
					->references('id')
					->on('tipo_notificaciones')
					->onDelete('restrict')
					->onUpdate('no action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('notificaciones');
	}
}
