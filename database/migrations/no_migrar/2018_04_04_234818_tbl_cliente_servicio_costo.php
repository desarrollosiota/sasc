<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblClienteServicioCosto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_servicio_costo', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('cliente_id');
            $table->integer('servicio_id');
            $table->float('costo', 12, 4);
            $table->string('descripcion');
            $table->timestamps();
            
            // $table->primary(['cliente_id', 'servicio_id']);

            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('servicio_id')->references('id')->on('servicio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Shema::drop('cliente_servicio_costo');
    }
}
