<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblUnidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidad', function (Blueprint $table) {
            $table->increments('id');
            $table->string('marca',50);
            $table->string('modelo',50);
            $table->string('placas',10);
            $table->string('color',50);
            $table->string('numero',10);
            $table->unsignedInteger('cliente_id');
            $table->timestamps();

            ///////INDICE/////////
            $table->unique('placas');
            
            $table->foreign('cliente_id')
                    ->references('id')
                    ->on('cliente')
                    ->onDelete('restrict')
                    ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidad');
    }
}
