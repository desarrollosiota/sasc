<?php

use Illuminate\Database\Seeder;
// use Hash;

class SuperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('usuario_rol')->insert([
            'nombre' => 'Administrador',
            'descripcion' => 'Administrador'
        ]);

        DB::table('usuario_rol')->insert([
            'nombre' => 'Empleado',
            'descripcion' => 'empleado'
        ]);

        DB::table('usuario')->insert([
            'nombre' => 'Jorge Beck',
            'usuario' => 'beck',
            'password' => Hash::make(123),
            'correo' => "jorge@beck.com",
            'usuario_rol_id' => 1,
            'estado' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('usuario')->insert([
            'nombre' => 'Jose Domingo',
            'usuario' => 'halcon28',
            'password' => Hash::make(123),
            'correo' => "jose@domingo.com",
            'usuario_rol_id' => 1,
            'estado' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('empleado')->insert([
            'nombre' => 'Jorge',
            'ap_paterno' => 'Becerra',
            'ap_materno' => 'Beck',
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('empleado')->insert([
            'nombre' => 'Jose',
            'ap_paterno' => 'Domingo',
            'ap_materno' => 'Valles',
            'usuario_id' => 2,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('estado_civil')->insert([
            'nombre' => 'Soltero',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('estado_civil')->insert([
            'nombre' => 'Casado(a)',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('estado_civil')->insert([
            'nombre' => 'Divorciado(a)',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('estado_civil')->insert([
            'nombre' => 'Viudo(a)',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('puesto')->insert([
            'nombre' => 'Chofer',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('puesto')->insert([
            'nombre' => 'Empleado',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('puesto')->insert([
            'nombre' => 'Jefe',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
