@include('header')
{{Html::style('css/app/catalogos.css')}}
{{Html::style('css/librerias/spinerkit.css')}}
<main>
	<header class="subnav-hero-section" style="padding: 1em;">
		<h1 class="subnav-hero-headline">Catálogos del sistema<small></small></h1>
	</header>
	<div class="grid-container">
		<div class="grid-x" style="padding-top: 1em;">
			<div class="cell small-12 medium-12 large-12">
				<label>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fas fa-search"></i>
						</span>
						<input class="input-group-field" id="busqueda_catalogo" name="busqueda_catalogo" type="text" placeholder="Buscar elemento" autocomplete="off">
						<div class="input-group-button">
							<input type="submit" class="button success" value="Nuevo elemento" data-open="modal_nuevo_concepto" id="a_modal_nuevo_concepto">
						</div>
					</div>
				</label>
			</div>
		</div>

		<div id="tabs_catalogos">
			<div id="catalogos_cargando">
				<div class="sk-folding-cube" id='spiner_editar_cliente'>
					<div class="sk-cube1 sk-cube"></div>
					<div class="sk-cube2 sk-cube"></div>
					<div class="sk-cube4 sk-cube"></div>
					<div class="sk-cube3 sk-cube"></div>
				</div>
			</div>
			<div id="catalogos_contenido" style="display:none;">
				<ul class="tabs" data-responsive-accordion-tabs="tabs small-accordion large-tabs" data-allow-all-closed="true" id="example-tabs">
					<li class="tabs-title is-active" id="li_servicio"><a href="#panel_servicios" aria-selected="true" onclick="catalogo = 'Servicio';">Servicios</a></li>
					<li class="tabs-title" id="li_puestos" onclick="catalogo = 'Puesto'"><a href="#panel_puestos">Puestos laborales</a></li>
					<li class="tabs-title" id="li_estados_civiles" onclick="catalogo = 'Estado_civil'"><a href="#panel_estados_civiles">Estados civiles</a></li>
				</ul>

				<div class="tabs-content" data-tabs-content="example-tabs">
					<div class="tabs-panel is-active" id="panel_servicios">
						<div id="panel_servicios_contenido">
						</div>
					</div>
					<div class="tabs-panel" id="panel_puestos">
						<div id="panel_puestos_contenido">
						</div>
					</div>
					<div class="tabs-panel" id="panel_estados_civiles">
						<div id="panel_estados_civiles_contenido">
						</div>
					</div>
				</div>
			</div>		
		</div>

	</div>
</main>
<div class="reveal with-header" id="modal_nuevo_concepto" style="top:100px; !important" data-reveal>
	<header class="subnav-hero-section">
		<h1 class="subnav-hero-headline" id="header_nuevo_concepto">Cargando...<small></small></h1>
	</header>
	<div class="grid-container">
		<form data-abide novalidate id="form_nuevo">
			<div class="grid-x grid-padding-x my-3">
				<div class="cell small-12 medium-12 large-12">
					<label>
						Concepto
						<div class="input-group">
							<input class="input-group-field nuevo_evento" id="input_nuevo_nombre" name="nombre" type="text" required  placeholder="Capturar concepto" autocomplete="off">
						</div>
						<span class="form-error" data-form-error-for="input_nuevo_nombre">
							El concepto es obligatorio
						</span>
					</label>
				</div>
				<div class="cell small-12 medium-12 large-12">
					<label>
						Descripción
						<div class="input-group">
							<input class="input-group-field nuevo_evento" id="input_nuevo_descripcion" name="descripcion" type="text" placeholder="Capture una descripción" autocomplete="off">
						</div>
					</label>
				</div>
			</div>
			<!--#################BOTONES##################-->
			<div class="grid-x grid-padding-x">
				<div class="button-group small-12 medium-12 large-12 cell expanded" data-grouptype="OR" id="botones_nuevo_cliente">
					<a class="button expanded underline-from-center success" onclick="guardarConcepto()">Guardar</a>
				</div>
			</div>
		</form>
	</div>
	<a href="" class="close-reveal-modal"></a>
</div>
@include('footer')
{{Html::script('js/app/catalogos/catalogos.controller.js')}}
