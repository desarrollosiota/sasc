<?php $rev = '?rev=2'; ?>
{{Html::style('css/librerias/zurb/foundation.min.css' )}}
{{Html::style('css/app/header.css' )}}
{{Html::style('css/librerias/toastr.min.css' )}}
{{Html::style('css/librerias/select2/select2.min.css' )}}
{{Html::style('css/librerias/select2/select2.foundation.css' )}}
{{Html::style('css/librerias/spinerkit.css' )}}
{{Html::style('css/librerias/alertifyjs/alertify.min.css')}}
{{Html::style('css/app/footer.css' )}}

<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>SIITRA</title>
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	</head>
	<body>
		<header>
			<!-- primer header blanco que lleva icono y botones -->
			<nav class="top-bar topbar-responsive" id='primer_header'>
				<div class="top-bar-title">
					<span data-responsive-toggle="topbar-responsive" data-hide-for="medium">
						<button class="menu-icon" type="button" data-toggle="offCanvasLeftOverlap"></button>
					</span>
					<img src="{{asset('img/logo.png')}}" alt="" id='siitra_logo'>
				</div>
				<div id="topbar-responsive" class="topbar-responsive-links">
					<div class="top-bar-right">
						<ul class="menu simple vertical medium-horizontal">
							<li style="margin-right: 1em;"><a href="{{url('/')}}"><i class="fas fa-home fa-fw fa-2x"></i></a></li>
							<li style="margin-right: 1em;"><a href="{{url('/perfil')}}"><i class="fas fa-user fa-fw fa-2x"></i></a></li>
							<li style="margin-right: 1em;"><a href="#!"><i class="far fa-bell fa-2x"></i></a></li>
							<li style="margin-right: 1em;"><a href="#!" onclick="logout();"><i class="fas fa-sign-out-alt fa-2x"></i></a></li>
						</ul>
					</div>
				</div>
			</nav>

			<!-- Segundo header con la navegacion -->
			<div class="top-bar" style="background-color: #1779ba;padding: 0px;" id='segundo_header'>
				<div class="top-bar-left">
					<ul class="dropdown menu align-center" data-dropdown-menu>
						<li><a href="{{url('catalogos')}}">Catálogos</a></li>
						<li>
							<a href="#">Usuarios</a>
							<ul class="menu vertical">
								<li><a href="{{url('usuarios')}}">Lista de usuarios</a></li>
								<li><a href="{{url('usuarios/nuevo')}}">Nuevo usuario</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Clientes</a>
							<ul class="menu vertical">
								<li><a href="{{url('clientes')}}"> Lista de clientes</a></li>
								<li><a href="{{url('clientes/nuevo')}}"> Nuevo cliente</a></li>
							</ul>
						</li>
						<li><a href="{{url('reportes')}}">Reportes</a></li>
					</ul>
				</div>
			</div>

			<!-- Menu de celular -->
			<div class="off-canvas position-right" id="offCanvasLeftOverlap" data-off-canvas data-transition="overlap" style="background-color: white;">
				<div class="card" style="border:none;">
					<div>
						<img src="{{asset('img/wallpaper3.jpg')}}" alt="">
					</div>
					<div class="card-section">
						<p id='user_name'></p>
						<p><small id='user_rol'></small></p>
					</div>
				</div>
				<ul class="vertical menu" data-responsive-menu="drilldown medium-accordion" style="max-width: 250px;" data-autoclose>
					<li><a href="{{url('/')}}">Inicio</a></li>
					<li><a href="{{url('perfil')}}">Perfil</a></li>
					<li><a href="{{url('catalogos')}}">Catálogos</a></li>
					<li>
						<a href="#">Usuarios</a>
						<ul class="vertical menu">
							<li><a href="{{url('usuarios')}}">Lista de Usuarios</a></li>
							<li><a href="{{url('usuarios/nuevo')}}">Nuevo Usuario</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Clientes</a>
						<ul class="vertical menu">
							<li><a href="{{url('clientes')}}"> Lista de clientes</a></li>
							<li><a href="{{url('clientes/nuevo')}}"> Nuevo cliente</a></li>
						</ul>
					</li>
					<li>
						<a href="{{url('reportes')}}">Reportes</a>
					</li>
					<li><a href="#"  onclick="logout();">Salir</a></li>
				</ul>
			</div>
		</header>
{{Html::script('js/librerias/jquery.min.js' )}}
{{Html::script('js/librerias/zurb/vendor/foundation.min.js' )}}
{{Html::script('js/librerias/toastr.min.js' )}}
{{Html::script('js/librerias/select2/select2.full.min.js' )}}
{{Html::script('js/librerias/confirm.js' )}}

{{Html::script('http://localhost:3002/socket.io/socket.io.js')}}
{{Html::script('js/librerias/funciones.globales.js')}}
{{Html::script('js/librerias/socket.js')}}
{{Html::script('js/app/header.controller.js ')}}
{{Html::script('js/librerias/alertifyjs/alertify.min.js ')}}
