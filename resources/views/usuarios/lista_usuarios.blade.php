@include('header')
{{Html::style('css/app/usuarios.css')}}
{{Html::script('js/app/usuarios/lista.controller.js')}}
{{Html::style('css/librerias/zurb/foundation-datepicker.css')}}

<main>
	<header class="subnav-hero-section">
		<h1 class="subnav-hero-headline">Lista de usuarios<small></small></h1>
	</header>
	<div class="grid-container" style="padding: 1em;">
		<div class="grid-x">
			<div class="cell small-12 medium-12 large-12">
				<label>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fas fa-search"></i>
						</span>
						<input class="input-group-field" id="busqueda_usuario" name="busqueda_usuario" type="text" placeholder="Buscar usuario" autocomplete="false">
					</div>
				</label>
			</div>
		</div>
		<div class="grid-x">
			<table class="unstriped table-expand responsive-card-table">
				<thead>
					<th>Nombre</th>
					<th>Usuario</th>
					<th class="show-for-large">Correo</th>
					<th>Rol</th>
					<th>Acciones</th>
				</thead>
				<tbody id="lista_usuarios">

				</tbody>
				<!--Spiner necesita la estrucutra completa para funcionar-->
				<tbody id='spiner_tabla_usuarios' class="hide">
					<tr>
						<td colspan="5">
							<div class="contenedor">
								<div class="centrado">
									<div class="sk-cube-grid">
										<div class="sk-cube sk-cube1"></div>
										<div class="sk-cube sk-cube2"></div>
										<div class="sk-cube sk-cube3"></div>
										<div class="sk-cube sk-cube4"></div>
										<div class="sk-cube sk-cube5"></div>
										<div class="sk-cube sk-cube6"></div>
										<div class="sk-cube sk-cube7"></div>
										<div class="sk-cube sk-cube8"></div>
										<div class="sk-cube sk-cube9"></div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="large reveal with-header" id="editar_usuario_modal" data-reveal>
		<header class="subnav-hero-section">
			<h1 class="subnav-hero-headline">Editar Usuario<small></small></h1>
		</header>
		<form data-abide novalidate id="frm_editar_usuario">
				<!--############USUARIO-CONTRAEÑA#############-->
				<div class="grid-x grid-padding-x">
					<div class="cell small-12 medium-6 large-6">
						<label>
			    			Usuario
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-user-plus"></i></span>
								<input class="input-group-field" id="usuario" name="usuario" type="text" required  placeholder="Capturar usuario" autocomplete="off" autocorrect="off" autocapitalize="off">
							</div>
							<span class="form-error" data-form-error-for="usuario">
								Favor de capturar el nombre del usuario
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-6 large-6">
						<label>
							Password
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-key"></i></span>
								<input class="input-group-field" id="password" name="password" type="text" required placeholder="Capturar contraseña" autocomplete="off" autocorrect="off" autocapitalize="off">
							</div>
							<span class="form-error" data-form-error-for="password">
								Favor de capturar una contraseña segura
							</span>
						</label>
					</div>
				</div>
				<!--#################CORREO-TIPO USUARIO###################-->
				<div class="grid-x grid-padding-x">
					<div class="cell small-12 medium-6 large-6">
						<label>
							Correo
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-envelope"></i>
								</span>
								<input class="input-group-field" id="correo" name="correo" type="email" placeholder="Capturar Correo" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="correo">
								Favor de capturar un correo con un formato valido
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-6 large-6">
					<label>
						Tipo de usuario
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-address-card"></i>
							</span>
							<select class="input-group-field select2" id="select_tipo_usuario" name="usuario_rol_id">
								<option value="1">Empleado</option>
								<option value="2">Administrador</option>
							</select>
						</div>
					</label>
				</div>
				</div>

				<hr>

				<!--##########NOMBRE-AP-AM####################-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-12 large-6 cell">
						<label>
							Nombre
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-user"></i>
								</span>
								<input class="input-group-field" required id="nombre" name ="nombre" type="text" placeholder="Capturar nombre" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de capturar el nombre del usuario
							</span>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Apellido Paterno
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-mars"></i>
								</span>
								<input class="input-group-field" required id="ap_paterno" name ="ap_paterno" type="text" placeholder="Capturar apellido" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="ap_paterno">
								Favor de capturar el apellido
							</span>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Apellido Materno
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-venus"></i>
								</span>
								<input class="input-group-field" required id="ap_materno" name ="ap_materno" type="text" placeholder="Capturar apellido" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="ap_materno">
								Favor de capturar el apellido
							</span>
						</label>
					</div>
				</div>
				<!--##########ESTADO CIVIL-NACIMIENTO#########-->
				<div class="grid-x grid-padding-x">
					<div class="col small-12 medium-6 large-6 cell">
						<label>
							Estado Civil
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-venus"></i>
								</span>
								<select class="input-group-field select2" id="select_estados_civiles" name="estado_civil_id">

								</select>
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Fecha de nacimiento
							<div class="input-group">
								<span class="input-group-label">
									<i class="far fa-calendar-alt"></i>
								</span>
								<input class="input-group-field" type="date" id='fecha_nacimiento' name="fecha_nacimiento" placeholder="Fecha de nacimiento">
							</div>
						</label>
					</div>
				</div>
				<!--#######DIRECCION-ESTADO-MUNICIPIO#########-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-12 large-6 cell">
						<label>
							Dirección
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map-signs"></i>
								</span>
								<input class="input-group-field" id="direccion" name ="direccion" type="text" placeholder="Capturar direccíón" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Estado
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map"></i>
								</span>
								<select class="input-group-field select2" id="select_estados">

								</select>
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Municipio
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map-marker-alt"></i>
								</span>
								<select class="input-group-field select2" name="municipio" id="select_municipios">
									<option value="">Seleccionar Opción</option>
								</select>
							</div>
						</label>
					</div>
				</div>
				<!--#############TELEFONO-PUESTO##############-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Telefono
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-phone"></i>
								</span>
								<input class="input-group-field" id="telefono" name="telefono" type="tel" placeholder="Capturar telefono" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Puesto Laboral
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-users"></i>
								</span>
								<select class="input-group-field select2" name='puesto_id' id='select_puestos'>
								</select>
							</div>
						</label>
					</div>
				</div>
				<!--#################BOTONES##################-->
				<div class="grid-x grid-padding-x">
					<div class="button-group small-12 medium-12 large-12 cell expanded" id="botones_editar_usuario">

					</div>
				</div>
		</form>
		<div class="contenedor hide" id='spiner_editar_usuario'>
			<div class="centrado">
				<div class="sk-folding-cube">
					<div class="sk-cube1 sk-cube"></div>
					<div class="sk-cube2 sk-cube"></div>
					<div class="sk-cube4 sk-cube"></div>
					<div class="sk-cube3 sk-cube"></div>
				</div>
			</div>
		</div>
		<button class="close-button" data-close aria-label="Close modal" type="button" style="right:1.5rem;top:1rem;">
			<span aria-hidden="true" style="color:#FFFFFF">&times;</span>
		</button>
	</div>
</main>
@include('footer')
