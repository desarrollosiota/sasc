@include('header')

{{Html::style('css/librerias/inspinia/bootstrap.min.css')}}
{{Html::style('font-awesome/css/font-awesome.min.css')}}
{{Html::style('css/librerias/inspinia/style.css')}}
<style media="screen">
html {
   font-size: 100% !important;
}
</style>
<main>
<div id="wrapper">
<div class="wrapper wrapper-content gray-bg">
   <div class="row">
      <div class="col-lg-8">
         <div class="row">
             <div class="col-lg-12">
                 <div class="ibox float-e-margins">
                     <div class="ibox-content">
                        <div>
                           <span class="pull-right text-right">
                              <small>Cantidades totales de servicios en: <strong>Tamaulipas</strong></small>
                              <br/>
                              Dias mostrados: 5
                           </span>
                           <h3 class="font-bold no-margins">
                              Cantidad de servicios realizados
                           </h3>
                           <small>Grafica de barras</small>
                        </div>
                         <div class="m-t-sm">
                             <div class="row">
                                 <div class="col-md-8">
                                    <div>
                                    <canvas id="lineChart" height="114"></canvas>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <ul class="stat-list m-t-lg">
                                       <li>
                                          <h2 class="no-margins">251</h2>
                                          <small>Servicios Terminados</small>
                                          <div class="progress progress-mini">
                                             <div class="progress-bar" style="width: 100%;background-color: #3adb76 !important;"></div>
                                          </div>
                                       </li>
                                      <li>
                                          <h2 class="no-margins">227</h2>
                                          <small>Servicios Pagados</small>
                                          <div class="progress progress-mini">
                                              <div class="progress-bar" style="width: 88%;background-color:#2c91b5 !important;"></div>
                                          </div>
                                      </li>
                                      <li>
                                         <h2 class="no-margins ">24</h2>
                                         <small>Servicios Pendientes de Pagar</small>
                                         <div class="progress progress-mini">
                                            <div class="progress-bar" style="width: 12%;background-color:#767676 !important;"></div>
                                         </div>
                                      </li>
                                    </ul>
                                 </div>
                             </div>
                         </div>
                         <div class="m-t-md">
                             <small class="pull-right">
                                 <i class="fa fa-clock-o"> </i>
                                 Actualizado a 05:52:00 PM
                             </small>
                             <small>
                                 <strong>Analisis de datos: </strong>No se incluye los servicios que estan activos ya que el monoto final puede cambiar. Servicios Activos: <strong>50</strong>.
                             </small>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-content">
                     <div>
                        <span class="pull-right text-right">
                           <small>Cantidades totales de servicios en: <strong>Tamaulipas</strong></small>
                           <br/>
                           Dias mostrados: 5
                        </span>
                        <h3 class="font-bold no-margins">
                           Cantidad de Ingresos
                        </h3>
                        <small>Grafica de lineas</small>
                     </div>
                      <div class="m-t-sm">
                          <div class="row">
                              <div class="col-md-8">
                                 <div>
                                 <canvas id="lineChart2" height="114"></canvas>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <ul class="stat-list m-t-lg">
                                    <li>
                                       <h2 class="no-margins">$53,253.50</h2>
                                       <small>Ingresos Netos</small>
                                       <div class="progress progress-mini">
                                          <div class="progress-bar" style="width: 100%;background-color: #3adb76 !important;"></div>
                                       </div>
                                    </li>
                                   <li>
                                       <h2 class="no-margins">$49,683.00</h2>
                                       <small>Ingresos Pagados</small>
                                       <div class="progress progress-mini">
                                           <div class="progress-bar" style="width: 88%;background-color:#2c91b5 !important;"></div>
                                       </div>
                                   </li>
                                   <li>
                                      <h2 class="no-margins ">$3,570.50</h2>
                                      <small>Ingresos Pendientes de Pagar</small>
                                      <div class="progress progress-mini">
                                         <div class="progress-bar" style="width: 12%;background-color:#767676 !important;"></div>
                                      </div>
                                   </li>
                                 </ul>
                              </div>
                          </div>
                      </div>
                      <div class="m-t-md">
                          <small class="pull-right">
                              <i class="fa fa-clock-o"> </i>
                              Actualizado a 05:52:00 PM
                          </small>
                          <small>
                              <strong>Analisis de datos: </strong>No se incluye los servicios que estan activos ya que el monoto final puede cambiar. Ingresos pendientes: <strong>$1,250</strong>.
                          </small>
                      </div>
                  </div>
              </div>
          </div>
         </div>
      </div>
      <div class="col-lg-4">
         <div class="row">
            <div class="col-md-6">
               <div class="ibox float-e-margins">
                  <div class="ibox-title">
                     <span class="label label-success pull-right">Monthly</span>
                     <h5>Views</h5>
                  </div>
                  <div class="ibox-content">
                     <h1 class="no-margins">386,200</h1>
                     <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                     <small>Total views</small>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="ibox float-e-margins">
                  <div class="ibox-title">
                     <span class="label label-info pull-right">Annual</span>
                     <h5>Orders</h5>
                  </div>
                  <div class="ibox-content">
                     <h1 class="no-margins">80,800</h1>
                     <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                     <small>New orders</small>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="ibox float-e-margins">
                  <div class="ibox-title">
                     <span class="label label-primary pull-right">Today</span>
                     <h5>visits</h5>
                  </div>
                  <div class="ibox-content">
                     <div class="row">
                        <div class="col-md-6">
                           <h1 class="no-margins">$ 406,420</h1>
                           <div class="font-bold text-navy">44% <i class="fa fa-level-up"></i> <small>Rapid pace</small></div>
                        </div>
                        <div class="col-md-6">
                           <h1 class="no-margins">206,120</h1>
                           <div class="font-bold text-navy">22% <i class="fa fa-level-up"></i> <small>Slow pace</small></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="ibox float-e-margins">
                  <div class="ibox-title">
                     <span class="label label-success pull-right">Monthly</span>
                     <h5>Views</h5>
                  </div>
                  <div class="ibox-content">
                     <h1 class="no-margins">386,200</h1>
                     <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                     <small>Total views</small>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="ibox float-e-margins">
                  <div class="ibox-title">
                     <span class="label label-info pull-right">Annual</span>
                     <h5>Orders</h5>
                  </div>
                  <div class="ibox-content">
                     <h1 class="no-margins">80,800</h1>
                     <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                     <small>New orders</small>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="ibox float-e-margins">
                  <div class="ibox-title">
                     <span class="label label-primary pull-right">Today</span>
                     <h5>visits</h5>
                  </div>
                  <div class="ibox-content">
                     <div class="row">
                        <div class="col-md-6">
                           <h1 class="no-margins">$ 406,420</h1>
                           <div class="font-bold text-navy">44% <i class="fa fa-level-up"></i> <small>Rapid pace</small></div>
                        </div>
                        <div class="col-md-6">
                           <h1 class="no-margins">206,120</h1>
                           <div class="font-bold text-navy">22% <i class="fa fa-level-up"></i> <small>Slow pace</small></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</main>
@include('footer')

{{Html::script('js/librerias/inspinia/jquery-3.1.1.min.js')}}
{{Html::script('js/librerias/inspinia/bootstrap.min.js')}}
{{Html::script('js/librerias/inspinia/jquery.metisMenu.js')}}
{{Html::script('js/librerias/inspinia/jquery.slimscroll.min.js')}}
{{Html::script('js/librerias/inspinia/inspinia.js')}}
{{Html::script('js/librerias/inspinia/jquery.flot.js')}}
{{Html::script('js/librerias/inspinia/jquery.flot.tooltip.min.js')}}
{{Html::script('js/librerias/inspinia/jquery.flot.resize.js')}}
{{Html::script('js/librerias/inspinia/Chart.min.js')}}
<script>
    $(document).ready(function() {

        var lineData = {
           labels: ["01/06/2018", "02/06/2018", "03/06/2018", "04/06/2018", "05/06/2018"],
           datasets: [
                {
                    label: "Total Pendiente",
                    backgroundColor: "rgb(118,118,118,0.7)",
                    borderColor: "rgb(118,118,118)",
                    pointBackgroundColor: "rgba(0,0,0,0.3)",
                    pointBorderColor: "#fff",
                    data: [8, 0, 8, 4, 4]
                },
                {
                   label: "Total Pagados",
                   backgroundColor:"rgb(23,121,186,0.7)",
                   borderColor:  "rgb(23,121,186)",
                   pointBackgroundColor: "rgba(0,0,0,0.3)",
                   pointBorderColor: "#fff",
                   data: [40, 48, 52, 35, 52]
                },
                {
                   label: "Total Servicios",
                   backgroundColor: "rgb(58,219,118,0.7)",
                   borderColor: "rgb(58,219,118)",
                   pointBackgroundColor: "rgba(0,0,0,0.3)",
                   pointBorderColor: "#fff",
                   data: [48, 48, 60, 39, 56]
                }
           ]
        };

        var lineData2 = {
           labels: ["01/06/2018", "02/06/2018", "03/06/2018", "04/06/2018", "05/06/2018"],
           datasets: [
                {
                    label: "Total Pendiente",
                    backgroundColor: "rgb(118,118,118,0.7)",
                    borderColor: "rgb(118,118,118)",
                    pointBackgroundColor: "rgba(0,0,0,0.3)",
                    pointBorderColor: "#fff",
                    data: [1061.5, 0, 2365, 458, 7500.5]
                },
                {
                   label: "Total Pagados",
                   backgroundColor:"rgb(23,121,186,0.7)",
                   borderColor:  "rgb(23,121,186)",
                   pointBackgroundColor: "rgba(0,0,0,0.3)",
                   pointBorderColor: "#fff",
                   data: [10561.5, 6515.54, 8998.5, 2508, 0]
                },
                {
                   label: "Total Servicios",
                   backgroundColor: "rgb(58,219,118,0.7)",
                   borderColor: "rgb(58,219,118)",
                   pointBackgroundColor: "rgba(0,0,0,0.3)",
                   pointBorderColor: "#fff",
                   data: [12586.5, 6515.54, 10560, 3008, 7500.5]
                }
           ]
        };

         var lineOptions = {
            responsive: true,
            lineTension:0
         };
         var ctx = document.getElementById("lineChart").getContext("2d");
         var ctx2 = document.getElementById("lineChart2").getContext("2d");
         new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
         new Chart(ctx2, {type: 'line', data: lineData2, options:lineOptions});

    });
</script>
