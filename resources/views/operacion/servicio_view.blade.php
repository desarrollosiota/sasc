@include('header')
{{Html::style('css/app/tickets.css')}}
{{Html::style('css/librerias/semantic/feed.min.css')}}

<main>
	<header class="subnav-hero-section">
		<h2 class="subnav-hero-headline">Vista de servicios</h2>
	</header>

	<!--TABLA DE DATOS-->
	<div class="grid-container">

		<div class="spinner" id='spinner_page'></div>
		<!--Crear el servicio-->
		<div class="grid-x grid-padding-y hide" id='nuevo_ticket'>
			<div class="cell">
				<div class="callout">
					<div class="cell">
						<label>
							Seleccionar Cliente
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map"></i>
								</span>
								<select class="input-group-field" id="select_cliente">
								</select>
							</div>
						</label>
					</div>
					<div class="cell">
						<label>
							¿Tienes comentarios? ¡capturalos aqui!
							<textarea placeholder="Sin texto" id='com_nuevo_ticket'></textarea>
						</label>
					</div>
					<div class="cell">
						<div class="expanded button-group">
							<a class="button" onclick="crear_ticket();">Crear Ticket</a>
							<a class="button">Cancelar</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="grid-x grid-padding-y small-up-1 large-up-2 hide" id='dashboard'>
			<div class = 'cell'>
				<!--RESUMEN TICKET-->
				<div class="grid-x grid-padding-x">
					<div class="cell">
						<div class="card">
							<section class="product-feature-section">
								<div class="product-feature-section-outer">
									<h4 class="product-feature-section-headline">Detalles del ticket</h4>
									<div class="product-feature-section-inner">
										<div class="product-feature-section-feature top-left">
											<i class="fa fa-hashtag" aria-hidden="true"></i>
											<!-- <i class="fa fa-heart" aria-hidden="true"></i> -->
											<div>
												<p class="feature-title">Número de ticket</p>
												<p class="feature-desc" id='res_ticket'></p>
											</div>
										</div>
										<div class="product-feature-section-feature top-right">
											<i class="fa fa-user-circle" aria-hidden="true"></i>
											<!-- <i class="fa fa-heart" aria-hidden="true"></i> -->
											<div>
												<p class="feature-title">Cliente</p>
												<p class="feature-desc" id='res_cliente'></p>
											</div>
										</div>
										<div class="product-feature-section-feature bottom-left">
											<i class="fa fa-comments" aria-hidden="true"></i>
											<!-- <i class="fa fa-coffee" aria-hidden="true"></i> -->
											<div>
												<p class="feature-title">Comentarios</p>
												<p class="feature-desc" id='res_comentario'></p>
											</div>
										</div>
										<div class="product-feature-section-feature bottom-right">
											<!-- <i class="fa fa-map-marker" aria-hidden="true"></i> -->
											<i class="fa fa-calendar-alt" aria-hidden="true"></i>
											<div>
												<p class="feature-title">Fecha</p>
												<p class="feature-desc" id="res_fecha"></p>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<!--Captura datos-->
				<div class="grid-x grid-padding-x" id='div_captura'>
					<div class="cell">
						<div class="callout">
							<!--Captura Servicio-->
							<div class="cell">
								<label>
									Seleccionar Servicio
									<div class="input-group">
										<span class="input-group-label">
											<i class="fas fa-puzzle-piece fa-lg"></i>
										</span>
										<select class="input-group-field" id="select_servicio">
											<option value="">Seleccionar Opción</option>
										</select>
									</div>
								</label>
							</div>
							<!--Captura Unidad-->
							<div class="cell">
								<label>
									Unidad
									<div class="input-group">
										<span class="input-group-label">
											<i class="fas fa-bus fa-lg"></i>
										</span>
										<input class="input-group-field" id="unidad" name="unidad" type="text" required placeholder="Capturar unidad" autocorrect="off" autocapitalize="off">
									</div>
									<span class="form-error" data-form-error-for="usuario">
										Favor de capturar la unidad a la que se le dio servicio
									</span>
								</label>
							</div>
							<!--Captura Comentarios-->
							<div class="cell">
								<label>
									¿Tienes comentarios? ¡capturalos aqui!
									<textarea placeholder="Sin texto" id='comentario_servicio'></textarea>
								</label>
							</div>
							<!--Botones-->
							<div class="cell">
								<div class="expanded button-group">
									<button id="nuevo_servicio_boton" class="button" onclick="guardar_servicio()">
										<i class="fas fa-spinner fa-spin fa-lg hide" id='nuevo_servicio_loader'></i>
										<span id='nuevo_servicio_text'>Guardar Servicio<span>
									</button>
									<a class="button">Cancelar</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--SECCION DE LOS COMENTARIOS-->
				<div class="grid-x grid-padding-x hide" id='div_comentarios'>
					<div class="cell">
						<label>
							Captura comentarios sobre el ticket
							<textarea placeholder="Sin texto" id='comentario_ticket' style="margin-bottom: .4em;"></textarea>
						</label>
						<button class="button expanded" onclick='guardar_comentaro()' id='btn-comentar'>
							<i class="fas fa-spinner fa-spin fa-lg hide"></i>
							<span>Comentar</span>
						</button>
						<div class="callout" id='comentarios_generales' style="height:33%;overflow-y:scroll;">
							<div class="sk-three-bounce" style='position: absolute;top: 45%;left: 45%;' id='comments_loader'>
							  <div class="sk-child sk-bounce1"></div>
							  <div class="sk-child sk-bounce2"></div>
							  <div class="sk-child sk-bounce3"></div>
							</div>
							<section>
								<div class="ui feed"></div>
							</section>
						</div>
					</div>
				</div>
			</div>
			<div class = 'cell'>
				<!--Tabla Datos-->
				<div class="grid-x">
					<div class="cell">
						<div class="callout">
							<div class="checkout-summary">
								<div class="checkout-summary-title">
									<h5>Número de servicios</h5>
									<h5 id='no_servicios'></h5>
								</div>
								<div id="items">
									<div class="checkout-summary-item hide">
										<div class="item-name">
											<a>Comfy Knit Blazer</a>
											<p><span class="title">Color: </span>Blue</p>
											<p><span class="title">Size: </span>M</p>
										</div>
										<div class="item-price">
											<p class="title"></p>
											<a href="#"></a>
										</div>
									</div>
								</div>
								<div class="checkout-summary-details">
									<div class="left">
										<p><strong>Total:</strong></p>
									</div>
									<div class="right">
										<p id='total_ticket'></p>
									</div>
								</div>
								<button class="button expanded" onclick="terminar_ticket()" id='btn-terminar'>
									<span>Terminar Ticket</span>
								</button>
								<button class="button expanded hide" id='btn-terminado'>
									<span>Ticket terminado</span>
								</button>
								<button class="button expanded hide" id='btn-cancelado'>
									<span>Terminar Ticket</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <table class="unstriped table-expand responsive-card-table hide" >
			<thead>
				<th>Servicio</th>
				<th>Unidad</th>
				<th>Precio</th>
				<th>Fecha</th>
			</thead>
			<tbody id="lista_clientes">
				<tr>
					<td data-label="Servicio">Lavador de interiores</td>
					<td data-label="Unidad">Cambion 01</td>
					<td data-label="Precio">%1,500.00</td>
					<td data-label="Fecha">26/04/2018</td>
					<td data-label="Hora">22:47</td>
				</tr>
			</tbody>
			Spiner necesita la estrucutra completa para funcionar
			<tbody id='spiner_tabla_clientes' class="hide">
				<tr>
					<td colspan="5">
						<div class="contenedor">
							<div class="centrado">
								<div class="sk-cube-grid">
									<div class="sk-cube sk-cube1"></div>
									<div class="sk-cube sk-cube2"></div>
									<div class="sk-cube sk-cube3"></div>
									<div class="sk-cube sk-cube4"></div>
									<div class="sk-cube sk-cube5"></div>
									<div class="sk-cube sk-cube6"></div>
									<div class="sk-cube sk-cube7"></div>
									<div class="sk-cube sk-cube8"></div>
									<div class="sk-cube sk-cube9"></div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table> -->
</main>
<script type="text/javascript">
	$("#select_cliente").select2({'placeholder':'Seleccionar Opción', theme: "foundation"});
	$("#select_cliente").prop({disabled: '',});
</script>
{{Html::script('js/app/ticket/ticket_view.js')}}
@include('footer')
