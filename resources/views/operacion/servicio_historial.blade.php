@include('operacion.header')

<main>
	<header class="subnav-hero-section">
		<h2 class="subnav-hero-headline">Mis tickets</h2>
	</header>

	<section style="margin:3em;">

	<div class="card grid-container">

		<form class="grid-x grid-padding-x" id="filtros">

			<div class="small-12 medium-6 large-6 cell">
				<label>
					Seleccionar Cliente
					<div class="input-group">
						<span class="input-group-label">
							<i class="fas fa-map"></i>
						</span>
						<select class="input-group-field" id="cliente_id" name="cliente_id">
							<option disabled selected>Cargando clientes...</option>
						</select>
					</div>
				</label>
			</div>
			<div class="small-12 medium-6 large-6 cell">
				<label>Status del ticket
					<div class="input-group">
						<span class="input-group-label">
							<i class="far fa-check-square"></i>
						</span>
						<select class="input-group-field" id="status" name="status">
							<option value="-1">Todos los status</option>
							<option value="0">Abierto</option>
							<option value="1">Terminado</option>
							<option value="2">Cancelado</option>
						</select>
					</div>
				</label>
			</div>

			<div class="small-12 medium-6 large-6 cell">
				<label>Fecha inicial
					<div class="input-group">
						<span class="input-group-label">
							<i class="far fa-calendar-alt"></i>
						</span>
						<input class="input-group-field" type="date" id='fecha_inicial' name="fecha_inicial" placeholder="Fecha de nacimiento">
					</div>
				</label>
			</div>

			<div class="small-12 medium-6 large-6 cell">
				<label>
					Fecha final
					<div class="input-group">
						<span class="input-group-label">
							<i class="far fa-calendar-alt"></i>
						</span>
						<input class="input-group-field" type="date" id='fecha_final' name="fecha_final" placeholder="Fecha de nacimiento">
					</div>
				</label>
			</div>
			<div class="small-12 medium-12 large-12 cell">
				<div class="cell">
					<div class="expanded button-group">
						<a class="button" onclick="obtenerTickets()">Buscar</a>
					</div>
				</div>
			</div>
		</form>
	</div>


	<!-- <div class="margin-2"> -->
		<table class="hover table-expand responsive-card-table">
			<thead>
				<th>No. Ticket</th>
				<th>Status</th>
				<th>Cliente</th>
				<th>Comentario</th>
				<th>Servicios</th>
				<!-- <th>Total</th> -->
				<th>Creado</th>
				<th>Terminado</th>
			</thead>
			<tbody id="tickets">

			</tbody>
			<!--Spiner necesita la estrucutra completa para funcionar-->
			<tbody id='spiner_tabla_tickets' class="hide">
				<tr>
					<td colspan="8" style="height: 10em;">
								<div class="sk-cube-grid">
									<div class="sk-cube sk-cube1"></div>
									<div class="sk-cube sk-cube2"></div>
									<div class="sk-cube sk-cube3"></div>
									<div class="sk-cube sk-cube4"></div>
									<div class="sk-cube sk-cube5"></div>
									<div class="sk-cube sk-cube6"></div>
									<div class="sk-cube sk-cube7"></div>
									<div class="sk-cube sk-cube8"></div>
									<div class="sk-cube sk-cube9"></div>
								</div>
					</td>
				</tr>
			</tbody>
		</table>
	</section>
</main>

</div>
</main>

<style>
.input-group-label i {
  color: white;
  width: 1rem;
}

.input-group-label {
  background-color: #1779ba;
  border-color: #1779ba;
}

.input-group-field {
  border-color: #1779ba;
}
</style>

{{Html::script('js/app/ticket/historial.js')}}
@include('footer')
