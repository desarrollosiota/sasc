@include('header')

<main>
	<header class="subnav-hero-section">
		<h3 class="subnav-hero-headline">Reportes</h3>
	</header>

	<section style="margin:2em;">

		<!-- <div class="card grid-container" >
		sdfsdfsdf
		</div> -->
		<div class="card grid-container" >

			<form class="grid-x grid-padding-x" id="filtros">

				<div class="small-12 medium-10 large-10 cell">
					<div class="grid-x grid-padding-x">
						<div class="small-12 medium-6 large-6 cell">
							<label>
								Seleccionar Cliente
								<div class="input-group">
									<span class="input-group-label">
										<i class="fas fa-map"></i>
									</span>
									<select class="input-group-field" id="cliente_id" name="cliente_id">
										<option disabled selected>Cargando clientes...</option>
									</select>
								</div>
							</label>
						</div>
						<div class="small-12 medium-6 large-6 cell">
							<label>Status del ticket
								<div class="input-group">
									<span class="input-group-label">
										<i class="far fa-check-square"></i>
									</span>
									<select class="input-group-field" id="status" name="status">
										<option value="-1">Todos los status</option>
										<option value="3">Pagado</option>
										<option value="1">Terminado</option>
										<option value="0">Abierto</option>
										<option value="2">Cancelado</option>
										
									</select>
								</div>
							</label>
						</div>

						<div class="small-12 medium-6 large-6 cell">
							<label>
								Usuario
								<div class="input-group">
									<span class="input-group-label">
										<i class="far fa-user"></i>
									</span>
									<select class="input-group-field" id="usuario_id" name="usuario_id">
										<option disabled selected>Cargando usuarios...</option>
									</select>
								</div>
							</label>
						</div>

						<div class="small-12 medium-3 large-3 cell">
							<label>
								Fecha inicial
								<div class="input-group">
									<span class="input-group-label">
										<i class="far fa-calendar-alt"></i>
									</span>
									<input class="input-group-field" type="date" id='fecha_inicial' name="fecha_inicial" placeholder="Fecha de nacimiento">
								</div>
							</label>
						</div>

						<div class="small-12 medium-3 large-3 cell">
							<label>
								Fecha final
								<div class="input-group">
									<span class="input-group-label">
										<i class="far fa-calendar-alt"></i>
									</span>
									<input class="input-group-field" type="date" id='fecha_final' name="fecha_final" placeholder="Fecha de nacimiento">
								</div>
							</label>
						</div>
					</div>
				</div>
				<div class="small-12 medium-2 large-2 cell">
					<div class="grid-x grid-padding-x">
						<div class="small-12 medium-12 large-12 cell" style="margin-top: 20px;">
							<ul class="stats-list">
								<li>
									<p id="total_tickets" style="margin:0px;">...</p> <span class="stats-list-label">Tickets</span>
								</li>
								<li class="stats-list-positive">
									<p id="total_monto_tickets" style="margin:0px;">...</p> <span class="stats-list-label">Ingresos</span>
								</li>
							</ul>
						</div>
						<div class="small-12 medium-12 large-12 cell" style="margin-top: 5px;">
							<div class="cell">
								<div class="expanded button-group">
									<a class="button" onclick="">Generar excel</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="small-12 medium-12 large-12 cell">
					<div class="cell">
						<div class="expanded button-group">
							<a class="button" onclick="obtenerTickets()">Buscar</a>
						</div>
					</div>
				</div>
			</form>
		</div>


	<!-- <div class="margin-2"> -->
		<table class="hover table-expand responsive-card-table">
			<thead>
				<th>No. Ticket</th>
				<th>Status</th>
				<th>Usuario</th>
				<th>Cliente</th>
				<th>Comentario</th>
				<th>Servicios</th>
				<th>Total</th>
				<th>Creado</th>
				<th>Terminado</th>
			</thead>
			<tbody id="tickets">
				
			</tbody>
			<!--Spiner necesita la estrucutra completa para funcionar-->
			<tbody id='spiner_tabla_tickets' class="hide">
				<tr>
					<td colspan="9" style="height: 10em;">
								<div class="sk-cube-grid">
									<div class="sk-cube sk-cube1"></div>
									<div class="sk-cube sk-cube2"></div>
									<div class="sk-cube sk-cube3"></div>
									<div class="sk-cube sk-cube4"></div>
									<div class="sk-cube sk-cube5"></div>
									<div class="sk-cube sk-cube6"></div>
									<div class="sk-cube sk-cube7"></div>
									<div class="sk-cube sk-cube8"></div>
									<div class="sk-cube sk-cube9"></div>
								</div>
					</td>
				</tr>
			</tbody>
		</table>
	</section>
</main>

</div>
</main>

<style>
.input-group-label i {
  color: white;
  width: 1rem;
}

.input-group-label {
  background-color: #1779ba;
  border-color: #1779ba;
}

.input-group-field {
  border-color: #1779ba;
}

.stats-list {
  list-style-type: none;
  clear: left;
  margin: 0;
  padding: 0;
  text-align: center;
  margin-bottom: 30px;
}

.stats-list .stats-list-positive {
  color: #3adb76;
}

.stats-list .stats-list-negative {
  color: #cc4b37;
}

.stats-list > li {
  display: inline-block;
  margin-right: 5px;
  padding-right: 5px;
  border-right: 1px solid #cacaca;
  text-align: center;
  font-size: 1.1em;
  font-weight: bold;
}

.stats-list > li:last-child {
  border: none;
  margin: 0;
  padding: 0;
}

.stats-list > li .stats-list-label {
  display: block;
  margin-top: 2px;
  font-size: 0.9em;
  font-weight: normal;
}

/* .sk-cube-grid {
	margin-top:70%;
} */

</style>

{{Html::script('js/app/reportes/reportes.js')}}
@include('footer')
