@include('header')

{{Html::style('css/app/usuarios.css')}}
{{Html::style('css/app/clientes.css')}}
{{Html::style('css/librerias/spinerkit.css')}}
{{Html::style('css/librerias/semantic/dropdown.css')}}
{{Html::style('css/librerias/semantic/transition.min.css')}}

{{Html::script('js/app/clientes/lista.controller.js')}}
{{Html::script('js/librerias/semantic/dropdown.js')}}
{{Html::script('js/librerias/semantic/transition.min.js')}}

<main>
	<header class="subnav-hero-section">
		<h1 class="subnav-hero-headline">Lista de clientes<small></small></h1>
	</header>
	<div class="grid-container" style="padding: 1em;">
		<div class="grid-x">
			<div class="cell small-12 medium-12 large-12">
				<label>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fas fa-search"></i>
						</span>
						<input class="input-group-field" id="busqueda_cliente" name="busqueda_cliente" type="text" placeholder="Buscar cliente" autocomplete="false">
					</div>
				</label>
			</div>
		</div>
		<div class="grid-x">
			<table class="unstriped table-expand responsive-card-table">
				<thead>
					<th>Nombre</th>
					<th>Teléfono</th>
					<th>Correo</th>
					<th>Dirección</th>
					<th>Acciones</th>
				</thead>
				<tbody id="lista_clientes">

				</tbody>
				<!--Spiner necesita la estrucutra completa para funcionar-->
				<tbody id='spiner_tabla_clientes' class="hide">
					<tr>
						<td colspan="5">
							<div class="contenedor">
								<div class="centrado">
									<div class="sk-cube-grid">
										<div class="sk-cube sk-cube1"></div>
										<div class="sk-cube sk-cube2"></div>
										<div class="sk-cube sk-cube3"></div>
										<div class="sk-cube sk-cube4"></div>
										<div class="sk-cube sk-cube5"></div>
										<div class="sk-cube sk-cube6"></div>
										<div class="sk-cube sk-cube7"></div>
										<div class="sk-cube sk-cube8"></div>
										<div class="sk-cube sk-cube9"></div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="large reveal with-header" id="editar_cliente_modal" data-reveal>
		<header class="subnav-hero-section">
			<h1 class="subnav-hero-headline">Editar Cliente<small></small></h1>
		</header>
		<div class="grid-container">
			<form data-abide novalidate id="frm_editar_cliente" class='invisible'>
				<!--###############NOMBRE-RFC#################-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-12 medium-6 large-6">
						<label>
							Nombre Empresa*
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-building"></i></span>
								<input class="input-group-field" id="nombre" name="nombre" type="text" required  placeholder="Capturar nombre" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de capturar el nombre de la empresa
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-6 large-6">
						<label>
							RFC*
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-lock"></i>
								</span>
								<input class="input-group-field" id="rfc" name="rfc" type="text" required placeholder="Capturar RFC" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="rfc">
								Favor de capturar el RFC de la empresa
							</span>
						</label>
					</div>
				</div>
				<!--############ALIAS-DIRECCION###############-->
				<div class="grid-x grid-padding-x">
					<div class="cell small-12 medium-12 large-6">
						<label>
							Nombre Fiscal
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-thumbtack"></i>
								</span>
								<input class="input-group-field" id="nombre_fiscal" name="nombre_fiscal" type="text" placeholder="Capturar Nombre Fiscal" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-6">
						<label>
							Dirección
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map-signs"></i>
								</span>
								<input class="input-group-field" id="direccion" name="direccion" type="text" placeholder="Capturar Dirección" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--#######DIRECCION-ESTADO-MUNICIPIO#########-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Estado
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map"></i>
								</span>
								<select class="input-group-field" id="select_estados">

								</select>
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Municipio
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map-marker-alt"></i>
								</span>
								<select class="input-group-field" name="municipio" id="select_municipios">
									<option value="">Seleccionar Opción</option>
								</select>
							</div>
						</label>
					</div>
				</div>
				<!--#############TELEFONO-CORREO##############-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Telefono
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-phone"></i>
								</span>
								<input class="input-group-field" id="telefono" name="telefono" type="text" placeholder="Capturar telefono" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Correo
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-envelope"></i>
								</span>
								<input class="input-group-field" id="correo" name="correo" type="text" placeholder="Capturar Correo" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--#################BOTONES##################-->
				<div class="grid-x grid-padding-x">
					<div class="button-group small-12 medium-12 large-12 cell expanded" id="botones_editar_cliente">

					</div>
				</div>
			</form>
			<div class="contenedor">
				<div class="centrado">
					<div class="sk-folding-cube" id='spiner_editar_cliente'>
						<div class="sk-cube1 sk-cube"></div>
						<div class="sk-cube2 sk-cube"></div>
						<div class="sk-cube4 sk-cube"></div>
						<div class="sk-cube3 sk-cube"></div>
					</div>
				</div>
			</div>
		</div>
		<button class="close-button" data-close aria-label="Close modal" type="button" style="right:1.5rem;top:1rem;">
			<span aria-hidden="true" style="color:#FFFFFF">&times;</span>
		</button>
	</div>
</main>
@include('footer')
