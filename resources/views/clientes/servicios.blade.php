@include('header')
{{Html::style('css/app/servicios.css')}}

<main>
	<header class="subnav-hero-section">
		<h4 class="subnav-hero-headline">Servicios del cliente <p id="subtitulo_cliente">...</p> <small></small></h4>
	</header>

	<!--TABLA DE DATOS-->
	<div class="grid-container" style="padding: 1em;">
		<div class="grid-x">
			<div class="cell small-12 medium-12 large-12">
				<label>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fas fa-search"></i>
						</span>
						<input class="input-group-field" id="busqueda_servicio" name="busqueda_servicio" type="text" placeholder="Buscar servicio" autocomplete="off">
						<div class="input-group-button">
							<input type="submit" class="button success" value="Nuevo servicio" data-open="nuevo_servicio">
						</div>
					</div>
				</label>
			</div>
		</div>
		<div class="grid-x">
			<table class="unstriped table-expand responsive-card-table">
				<thead>
					<tr>
						<th>Servicio</th>
						<th>Precio</th>
						<th>comentarios</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody id="lista_servicios">
		
				</tbody>
				<tbody id='spiner_tabla_servicios' class="hide">
					<tr>
						<td colspan="5">
							<div class="contenedor">
								<div class="centrado">
									<div class="sk-cube-grid">
										<div class="sk-cube sk-cube1"></div>
										<div class="sk-cube sk-cube2"></div>
										<div class="sk-cube sk-cube3"></div>
										<div class="sk-cube sk-cube4"></div>
										<div class="sk-cube sk-cube5"></div>
										<div class="sk-cube sk-cube6"></div>
										<div class="sk-cube sk-cube7"></div>
										<div class="sk-cube sk-cube8"></div>
										<div class="sk-cube sk-cube9"></div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<!--MDOAL NUEVO SERVICIO-->
	<div class="reveal with-header" id="nuevo_servicio" data-reveal>
		<header class="subnav-hero-section">
			<h1 class="subnav-hero-headline">Agregar servicio<small></small></h1>
		</header>
		<div class="grid-container">
			<form data-abide novalidate id="frm_nuevo_servicio">
				<!--#######SERVICIO-COSTO-DESCRIPCION######-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-12 medium-12 large-12">
						<label>
							Servicio
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-truck"></i></span>
								<select class="input-group-field select2" id="servicio" name="servicio" autocomplete="off">
									<option value="">Seleccionar Opción</option>
								</select>
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de seleccionar un servicio
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Costo
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-dollar-sign fa-lg"></i>
								</span>
								<input class="input-group-field" id="costo" name="costo" type="number" required placeholder="Capturar costo" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de capturar el costo del servicio
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Comentarios
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-comment"></i></span>
								<input class="input-group-field" id="comentario" name="comentario" type="text" placeholder="Capturar comentarios" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--##########BOTON DE GUARDAR################-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-6 medium-3 large-3">
						<button class="button expanded success" onclick="guardar_servicio_costo()" id='btn_guardar_servicio'>
							<i class="fas fa-spinner fa-spin fa-lg hide"></i>
							<span>Guardar</span>
						</button>
					</div>
				</div>
			</form>
		</div>
		<button class="close-button" data-close aria-label="Close modal" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<!--MDOAL EDITAR SERVICIO-->
	<div class="reveal with-header" id="editar_servicio" data-reveal>
		<header class="subnav-hero-section">
			<h1 class="subnav-hero-headline">Editar servicio<small></small></h1>
		</header>
		<div class="grid-container">
			<form data-abide novalidate id="frm_editar_servicio">
				<input id="id_servicio" name="servicio_id" type="text" class='hide'>
				<!--###############NOMBRE-TELEFONO############-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-12 medium-12 large-12">
						<label>
							Servicio
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-truck"></i></span>
								<select class="input-group-field select2" id="servicio_editar" name="servicio" autocomplete="off">
									<option value="">Seleccionar Opción</option>
								</select>
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de seleccionar un servicio
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Costo
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-dollar-sign fa-lg"></i>
								</span>
								<input class="input-group-field" id="costo_editar" name="costo" type="number" required placeholder="Capturar costo" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de capturar el costo del servicio
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Comentarios
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-comment"></i></span>
								<input class="input-group-field" id="comentario_editar" name="descripcion" type="text" placeholder="Capturar comentarios" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--##########BOTON DE GUARDAR################-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-6 medium-3 large-3">
						<button class="button expanded success" onclick="guardar_servicio()" id='btn_editar_servicio'>
							<i class="fas fa-spinner fa-spin fa-lg hide"></i>
							<span>Guardar</span>
						</button>
					</div>
				</div>
			</form>
		</div>
		<div class="contenedor hide" id='spiner_editar_servicio'>
			<div class="centrado">
				<div class="sk-folding-cube">
					<div class="sk-cube1 sk-cube"></div>
					<div class="sk-cube2 sk-cube"></div>
					<div class="sk-cube4 sk-cube"></div>
					<div class="sk-cube3 sk-cube"></div>
				</div>
			</div>
		</div>
		<button class="close-button" data-close aria-label="Close modal" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</main>

@include('footer')
{{Html::script('js/app/clientes/servicios.controller.js')}}