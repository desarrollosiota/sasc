@include('header')
{{Html::style('css/app/usuarios.css')}}

<main>
	<header class="subnav-hero-section">
		<h4 class="subnav-hero-headline">Lista de contactos <p id="subtitulo_cliente">...</p> <small></small></h4>
	</header>

	<!--TABLA DE DATOS-->
	<div class="grid-container" style="padding: 1em;">
		<div class="grid-x">
			<div class="cell small-12 medium-12 large-12">
				<label>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fas fa-search"></i>
						</span>
						<input class="input-group-field" id="busqueda_contacto" name="busqueda_contacto" type="text" placeholder="Buscar contacto" autocomplete="off">
						<div class="input-group-button">
							<input type="submit" class="button success" value="Nuevo contacto" data-open="nuevo_contacto">
						</div>
					</div>
				</label>
			</div>
		</div>
		<div class="grid-x">
			<table class="unstriped table-expand responsive-card-table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Teléfono</th>
						<th>Correo</th>
						<th>Puesto</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody id="lista_contactos">
		
				</tbody>
				<tbody id='spiner_tabla_contactos' class="hide">
					<tr>
						<td colspan="5">
							<div class="contenedor">
								<div class="centrado">
									<div class="sk-cube-grid">
										<div class="sk-cube sk-cube1"></div>
										<div class="sk-cube sk-cube2"></div>
										<div class="sk-cube sk-cube3"></div>
										<div class="sk-cube sk-cube4"></div>
										<div class="sk-cube sk-cube5"></div>
										<div class="sk-cube sk-cube6"></div>
										<div class="sk-cube sk-cube7"></div>
										<div class="sk-cube sk-cube8"></div>
										<div class="sk-cube sk-cube9"></div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<!--MDOAL NUEVO CONTACTO-->
	<div class="reveal with-header" id="nuevo_contacto" data-reveal>
		<header class="subnav-hero-section">
			<h1 class="subnav-hero-headline">Agregar contacto<small></small></h1>
		</header>
		<div class="grid-container">
			<form data-abide novalidate id="frm_nuevo_contacto">
				<!--###############NOMBRE-TELEFONO############-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-12 medium-12 large-12">
						<label>
							Nombre*
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-user-plus"></i></span>
								<input class="input-group-field" id="nombre" name="nombre" type="text" required  placeholder="Capturar nombre" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de capturar el nombre del contacto
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Teléfono*
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-phone"></i>
								</span>
								<input class="input-group-field" id="telefono" name="telefono" type="tel" placeholder="Capturar Telefono" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--#############CORREO-PUESTO################-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-12 medium-12 large-12">
						<label>
							Correo*
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-envelope"></i></span>
								<input class="input-group-field" id="correo" name="correo" type="text" placeholder="Capturar correo" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Puesto*
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-users"></i></span>
								<input class="input-group-field" id="puesto" name="puesto" type="text" placeholder="Capturar puesto" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--##########BOTON DE GUARDAR################-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-6 medium-3 large-3">
						<a class="button expanded success" onclick="guardar_contacto()" id='btn_guardar_contacto'>
							<i class="fas fa-spinner fa-spin fa-lg hide"></i>
							<span>Guardar</span>
						</a>
					</div>
				</div>
			</form>
		</div>
		<button class="close-button" data-close aria-label="Close modal" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<!--MDOAL EDITAR CONTACTO-->
	<div class="reveal with-header" id="editar_contacto" data-reveal>
		<header class="subnav-hero-section">
			<h1 class="subnav-hero-headline">Editar contacto<small></small></h1>
		</header>
		<div class="grid-container">
			<form data-abide novalidate id="frm_editar_contacto">
				<input id="id_contacto" name="id_contacto" type="text" class='hide'>
				<!--###############NOMBRE-TELEFONO############-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-12 medium-12 large-12">
						<label>
							Nombre
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-user-plus"></i></span>
								<input class="input-group-field" id="editar_nombre" name="nombre" type="text" required  placeholder="Capturar nombre" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de capturar el nombre del contacto
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Teléfono
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-phone"></i>
								</span>
								<input class="input-group-field" id="editar_telefono" name="telefono" type="tel" placeholder="Capturar Telefono" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--#############CORREO-PUESTO################-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-12 medium-12 large-12">
						<label>
							Correo
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-envelope"></i></span>
								<input class="input-group-field" id="editar_correo" name="correo" type="text" placeholder="Capturar correo" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="cell small-12 medium-12 large-12">
						<label>
							Puesto
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-users"></i></span>
								<input class="input-group-field" id="editar_puesto" name="puesto" type="text" placeholder="Capturar puesto" autocomplete="off">
							</div>
						</label>
					</div>
				</div>
				<!--##########BOTON DE GUARDAR################-->
				<div class="grid-x grid-padding-x my-3">
					<div class="cell small-6 medium-3 large-3">
						<a class="button expanded success" onclick="editar_contacto()" id='btn_editar_contacto'>
							<i class="fas fa-spinner fa-spin fa-lg hide"></i>
							<span>Guardar</span>
						</a>
					</div>
				</div>
			</form>
		</div>
		<div class="contenedor hide" id='spiner_editar_contacto'>
			<div class="centrado">
				<div class="sk-folding-cube">
					<div class="sk-cube1 sk-cube"></div>
					<div class="sk-cube2 sk-cube"></div>
					<div class="sk-cube4 sk-cube"></div>
					<div class="sk-cube3 sk-cube"></div>
				</div>
			</div>
		</div>
		<button class="close-button" data-close aria-label="Close modal" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</main>

@include('footer')
{{Html::script('js/app/clientes/contactos.controller.js')}}