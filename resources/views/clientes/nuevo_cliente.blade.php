@include('header')
{{Html::style('css/app/usuarios.css')}}

<main>
	<header class="subnav-hero-section">
		<h1 class="subnav-hero-headline">Alta de Clientes<small></small></h1>
	</header>
	<div class="grid-container">
		<form data-abide novalidate id="frm_nuevo_cliente">
			<!--###############NOMBRE-RFC#################-->
			<div class="grid-x grid-padding-x my-3">
				<div class="cell small-12 medium-6 large-6">
					<label>
		    			Nombre Empresa*
						<div class="input-group">
							<span class="input-group-label"><i class="fas fa-building"></i></span>
							<input class="input-group-field" id="nombre" name="nombre" type="text" required  placeholder="Capturar nombre" autocomplete="off">
						</div>
						<span class="form-error" data-form-error-for="nombre">
							Favor de capturar el nombre de la empresa
						</span>
					</label>
				</div>
				<div class="cell small-12 medium-6 large-6">
					<label>
		    			RFC*
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-lock"></i>
							</span>
							<input class="input-group-field" id="rfc" name="rfc" type="text" required placeholder="Capturar RFC" autocomplete="off">
						</div>
						<span class="form-error" data-form-error-for="rfc">
							Favor de capturar el RFC de la empresa
						</span>
					</label>
				</div>
			</div>
			<!--############ALIAS-DIRECCION###############-->
			<div class="grid-x grid-padding-x">
				<div class="cell small-12 medium-12 large-6">
					<label>
						Nombre Fiscal
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-thumbtack"></i>
							</span>
							<input class="input-group-field" id="nombre_fiscal" name="nombre_fiscal" type="text" placeholder="Capturar Nombre Fiscal" autocomplete="off">
						</div>
					</label>
				</div>
				<div class="cell small-12 medium-12 large-6">
					<label>
						Dirección
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-map-signs"></i>
							</span>
							<input class="input-group-field" id="direccion" name="direccion" type="text" placeholder="Capturar Dirección" autocomplete="off">
						</div>
					</label>
				</div>
			</div>
			<!--#######DIRECCION-ESTADO-MUNICIPIO#########-->
			<div class="grid-x grid-padding-x">
				<div class="small-12 medium-6 large-6 cell">
					<label>
						Estado
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-map"></i>
							</span>
							<select class="input-group-field" id="select_estados">

							</select>
						</div>
					</label>
				</div>
				<div class="small-12 medium-6 large-6 cell">
					<label>
						Municipio
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-map-marker-alt"></i>
							</span>
							<select class="input-group-field" name="municipio" id="select_municipios" disabled="disabled">
								<option value="">Seleccionar Opción</option>
							</select>
						</div>
					</label>
				</div>
			</div>
			<!--#############TELEFONO-CORREO##############-->
			<div class="grid-x grid-padding-x">
				<div class="small-12 medium-6 large-6 cell">
					<label>
						Telefono
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-phone"></i>
							</span>
							<input class="input-group-field" id="telefono" name="telefono" type="text" placeholder="Capturar telefono" autocomplete="off">
						</div>
					</label>
				</div>
				<div class="small-12 medium-6 large-6 cell">
					<label>
						Correo
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-envelope"></i>
							</span>
							<input class="input-group-field" id="correo" name="correo" type="text" placeholder="Capturar Correo" autocomplete="off">
						</div>
					</label>
				</div>
			</div>
			<!--#################BOTONES##################-->
			<div class="grid-x grid-padding-x">
				<div class="button-group small-12 medium-12 large-12 cell expanded">
					<a class="button expanded success" onclick="$(this).submit();" id="btn_nuevo_usuario">
						<i class="fas fa-spinner fa-spin fa-lg hide"></i>
						<span>Guardar</span>
					</a>
					<a class="button expanded" onclick="limpiar_nuevo_cliente();">Limpiar</a>
				</div>
			</div>
		</form>
	</div>
</main>

@include('footer')
{{Html::script('js/app/clientes/nuevo.controller.js')}}