{{Html::style('css/librerias/zurb/foundation.min.css')}}
{{Html::style('css/app/header.css')}}
{{Html::style('css/app/footer.css')}}
<main>
	<header class="subnav-hero-section">
		<h1 class="subnav-hero-headline">404 Página no encontrada<small></small></h1>
	</header>
	<div class="grid-container" style="padding: 1em;">
        <br><br><br><br><br>
        <center style="color:#bbbbbb;"><b>La página que buscas no fue encontrada</b></center>
	</div>
</main>
@include('footer')
