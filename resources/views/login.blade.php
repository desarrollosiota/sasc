<?php $rev = '?rev=2'; ?>
{{Html::style('css/librerias/zurb/foundation.min.css'.$rev)}}
{{Html::style('css/app/login.css' . $rev)}}

{{Html::script('js/librerias/jquery.min.js' . $rev)}}
{{Html::script('js/librerias/zurb/vendor/foundation.min.js' . $rev)}}
{{Html::script('js/librerias/funciones.globales.js' . $rev)}}
{{Html::script('js/app/login.controller.js' . $rev)}}
 <!DOCTYPE html>
 <html>

 <head>
	  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	  <meta charset="utf-8">
	  <title>Login</title>
 </head>

 <body>
	  <div class="grid-x">
			<div class="cell" id="img_logo">
				 <img src="img/logo.png" alt="" id='siitra_logo'>
			</div>
	  </div>
	  <form id="login_form">
			<div class="login">
				 <div class="login-screen">
					  <div class="app-title">
							<h1>Login</h1>
					  </div>
					  <div class="login-form">
							<div class="control-group">
								 <input type="text" class="login-field" name="usuario" placeholder="Usuario" id="login-name">
								 <label class="login-field-icon fui-user" for="login-name"></label>
							</div>
							<div class="control-group">
								 <input type="password" class="login-field" name="password" placeholder="Password" id="login-pass" autocomplete="off">
								 <label class="login-field-icon fui-lock" for="login-pass" autocomplete="off"></label>
							</div>
							<a class="btn btn-primary btn-large btn-block" href="#" onclick="login()">Entrar</a>
							<a class="login-link" href="#">Recupera tu contraseña</a>
					  </div>
				 </div>
			</div>
	  </form>
 </body>

 </html>
