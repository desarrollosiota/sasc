@include('header')
{{Html::style('css/app/usuarios.css')}}
{{Html::style('css/librerias/zurb/foundation-datepicker.css')}}

<main>
	<header class="subnav-hero-section">
		<h1 class="subnav-hero-headline">Perfil <span id="titulo_nombre_perfil">...</span><small></small></h1>
	</header>

	<div class="grid-container">
		<form class="hide" data-abide novalidate id="frm_usuario">
				<!--############USUARIO-CONTRAEÑA#############-->
				<div class="grid-x grid-padding-x">
					<div class="cell small-12 medium-6 large-6">
						<label>
							Usuario
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-user-plus"></i></span>
								<input class="input-group-field" id="usuario" name="usuario" type="text" required  placeholder="Capturar usuario" autocomplete="off" autocorrect="off" autocapitalize="off">
							</div>
							<span class="form-error" data-form-error-for="usuario">
								Favor de capturar el nombre del usuario
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-6 large-6">
						<label>
							Password
							<div class="input-group">
								<span class="input-group-label"><i class="fas fa-key"></i></span>
								<input class="input-group-field" id="password" name="password" type="text" placeholder="Capturar contraseña" autocomplete="off" autocorrect="off" autocapitalize="off">
							</div>
						</label>
					</div>
				</div>
				<!--#################CORREO-TIPO USUARIO###################-->
				<div class="grid-x grid-padding-x">
					<div class="cell small-12 medium-6 large-6">
						<label>
							Correo
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-envelope"></i>
								</span>
								<input class="input-group-field" id="correo" name="correo" type="email" placeholder="Capturar Correo" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="correo">
								Favor de capturar un correo con un formato valido
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-6 large-6">
					<label>
						Tipo de usuario
						<div class="input-group">
							<span class="input-group-label">
								<i class="fas fa-address-card"></i>
							</span>
							<select disabled class="input-group-field select2" id="select_tipo_usuario" name="usuario_rol_id">
								<option value="1">Empleado</option>
								<option value="2">Administrador</option>
							</select>
						</div>
					</label>
				</div>
				</div>

				<hr>

				<!--##########NOMBRE-AP-AM####################-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-12 large-6 cell">
						<label>
							Nombre
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-user"></i>
								</span>
								<input class="input-group-field" required id="nombre" name ="nombre" type="text" placeholder="Capturar nombre" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="nombre">
								Favor de capturar el nombre del usuario
							</span>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Apellido Paterno
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-mars"></i>
								</span>
								<input class="input-group-field" required id="ap_paterno" name ="ap_paterno" type="text" placeholder="Capturar apellido" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="ap_paterno">
								Favor de capturar el apellido
							</span>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Apellido Materno
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-venus"></i>
								</span>
								<input class="input-group-field" required id="ap_materno" name ="ap_materno" type="text" placeholder="Capturar apellido" autocomplete="off">
							</div>
							<span class="form-error" data-form-error-for="ap_materno">
								Favor de capturar el apellido
							</span>
						</label>
					</div>
				</div>
				<!--##########ESTADO CIVIL-NACIMIENTO#########-->
				<div class="grid-x grid-padding-x">
					<div class="col small-12 medium-6 large-6 cell">
						<label>
							Estado Civil
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-venus"></i>
								</span>
								<select class="input-group-field select2" id="select_estados_civiles" name="estado_civil_id">

								</select>
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Fecha de nacimiento
							<div class="input-group">
								<span class="input-group-label">
									<i class="far fa-calendar-alt"></i>
								</span>
								<input class="input-group-field" type="date" id='fecha_nacimiento' name="fecha_nacimiento" placeholder="Fecha de nacimiento">
							</div>
						</label>
					</div>
				</div>
				<!--#######DIRECCION-ESTADO-MUNICIPIO#########-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-12 large-6 cell">
						<label>
							Dirección
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map-signs"></i>
								</span>
								<input class="input-group-field" id="direccion" name ="direccion" type="text" placeholder="Capturar direccíón" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Estado
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map"></i>
								</span>
								<select class="input-group-field select2" id="select_estados">

								</select>
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-3 cell">
						<label>
							Municipio
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-map-marker-alt"></i>
								</span>
								<select class="input-group-field select2" name="municipio" id="select_municipios">
									<option value="">Seleccionar Opción</option>
								</select>
							</div>
						</label>
					</div>
				</div>
				<!--#############TELEFONO-PUESTO##############-->
				<div class="grid-x grid-padding-x">
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Telefono
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-phone"></i>
								</span>
								<input class="input-group-field" id="telefono" name="telefono" type="tel" placeholder="Capturar telefono" autocomplete="off">
							</div>
						</label>
					</div>
					<div class="small-12 medium-6 large-6 cell">
						<label>
							Puesto Laboral
							<div class="input-group">
								<span class="input-group-label">
									<i class="fas fa-users"></i>
								</span>
								<select disabled class="input-group-field select2" name='puesto_id' id='select_puestos'>
								</select>
							</div>
						</label>
					</div>
				</div>
				<!--#################BOTONES##################-->
				<div class="grid-x grid-padding-x">
					<div class="button-group small-12 medium-12 large-12 cell expanded" id="botones_usuario">
						<button class="button expanded success" onclick="actualizarUsuario()" id="btn_usuario">
							<i class="fas fa-spinner fa-spin fa-lg hide"></i>
							<span>Actualizar</span>
						</button>
					</div>
				</div>
		</form>
		<div class="contenedor" id='spiner_usuario' style="height: 10em;position:relative;">
				<div class="sk-folding-cube">
					<div class="sk-cube1 sk-cube"></div>
					<div class="sk-cube2 sk-cube"></div>
					<div class="sk-cube4 sk-cube"></div>
					<div class="sk-cube3 sk-cube"></div>
				</div>
		</div>
	</div>


</main>


@include('footer')
{{Html::script('js/app/usuarios/perfil.controller.js')}}
