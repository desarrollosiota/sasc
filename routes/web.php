<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# Truco para escribir views con menos codigo
function rv($v = '') {
	return function() use ($v){
		return view($v);
	};
}
Route::get('login', rv('login'))->middleware('login');
# Middleware que evalua si existe la sesion
Route::group(['middleware' => 'authSession'], function () {

	# Con esta ruta de inicio checamos que tipo de sesion hay con el middleware
	Route::get('/', rv('index'))->middleware('indexSession');
	# Perfil
	Route::get('perfil', rv('perfil'));
	# Rutas para los modulos de los administradores
	Route::group(['middleware' => 'adminSession'], function () {
		Route::get('usuarios',rv('usuarios.lista_usuarios'));
		Route::get('usuarios/nuevo',rv('usuarios.nuevo_usuario'));
		Route::get('clientes',rv('clientes.lista_clientes'));
		Route::get('clientes/nuevo',rv('clientes.nuevo_cliente'));
		Route::get('clientes/contactos',rv('clientes.contactos'));
		Route::get('clientes/servicios',rv('clientes.servicios'));
		Route::get('catalogos',rv('catalogos.catalogos'));
		Route::get('reportes',rv('reportes.reportes'));
		Route::get('reportes/ticket/{id}',rv('reportes.ticket'));
		Route::get('servicios/ver',rv('operacion.servicio_view'));
		Route::get('resultados/financieros',rv('resultados.financieros'));
	});
	# Rutas para los modulos de los empleados
	Route::group(['middleware' => 'userSession', 'prefix' => 'operacion'], function () {
		Route::get('/',rv('operacion.index'));
		Route::get('perfil', rv('operacion.perfil'));
		Route::get('servicios/nuevo',rv('operacion.servicio_nuevo'));
		Route::get('servicios/historial',rv('operacion.servicio_historial'));
	});

});

# Handler para vistas de error
Route::fallback(function($uri){
	# Handler donde se encuentran listadas las vistas de error 404
	return Views404::check($uri);
});
