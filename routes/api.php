<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# Rutas con namespace
Route::group(['namespace' => 'v1'] , function(){

	# Ruta para loguearse
	Route::post('login', 'LoginController@login');

	# Rutas privadas que requieren session para mostrar datos
	Route::group(['middleware' => 'authSession'] , function(){

		# E
		Route::put('actualizar_perfil', 'UsuarioController@actualizarPerfil');

		Route::post('actualizar_socket_id', 'LoginController@actualizarSocketId');
		Route::post('logout', 'LoginController@logout');

		# El pinche cagadero del mingo
		Route::post('usuarios', 'UsuarioController@puestos');
		Route::post('usuario_actual', 'UsuarioController@usuarioActual');
		Route::post('usuarios_estados_civiles', 'UsuarioController@estadosCiviles');
		Route::post('estados', 'UsuarioController@buscarEstados');
		Route::post('municipios', 'UsuarioController@buscarMunicipios');
		Route::post('catalogoServicios', 'ClienteServicioCostoController@buscarServicios');
		Route::post('buscarServiciosDatos','ClienteServicioCostoController@buscarServiciosDatos');

		# El pinche poder del Forge Beker
		Route::resource('usuario', 'UsuarioController');
		Route::resource('cliente', 'ClienteController');
		Route::resource('servicio', 'ServicioController');
		Route::resource('cliente_servicio_costo', 'ClienteServicioCostoController');
		Route::resource('puesto', 'PuestoController');
		Route::resource('estado_civil', 'EstadoCivilController');
		Route::resource('ticket', 'TicketController');
		Route::resource('ticket_comentario', 'TicketComentario');
		Route::resource('notificacion', 'NotificacionController');
		Route::post('ticket_terminado', 'TicketController@ticketTerminado');
		Route::post('reactivar_ticket', 'TicketController@reactivarTicket');

		Route::post('pagar_ticket', 'TicketController@pagarTicket');
		Route::post('send_mail', 'TicketController@send_mail');
		Route::resource('ticket_detalle', 'TicketDetalleController');
		Route::delete('ticket_detalle/{ticket_id}/{ticket_detalle_id}', 'TicketDetalleController@removerServicio');
		Route::post('servicio_terminado', 'TicketDetalleController@servicioTerminado');
		Route::post('reactivar_servicio', 'TicketDetalleController@reactivarServicio');

		Route::get('catalogos', 'CatalogoController@obtenerCatalogos');

		# Mas cagadero del mingo <---- al chile no mames jajja estan con madre <-----Tu tienes el poder del resource <----- y tu el de SQL pelon (migatte no gokui de las DB) <----- tu puedes mover la melena miestras programas <----- y tu le mueves las nalgas a carlitros mientras programas jajajajaja gei
		Route::post('contactos_get', 'ClienteController@contactos_get');
		Route::post('agregar_contacto', 'ClienteController@agregar_contacto');
		Route::post('datos_contacto', 'ClienteController@datos_contacto');
		Route::post('editar_contacto','ClienteController@editar_contacto');
		Route::post('eliminar_contacto','ClienteController@eliminar_contacto');

	});
});
