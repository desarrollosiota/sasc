
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('mysql');
var port = process.env.PORT || 3002;

app.get('/', function (req, res) {
	res.send('yey :D');
});

// El MySQL
// var con = mysql.createConnection({
// 	host: "localhost",
// 	user: "root",
// 	password: ""
// });

// mini servidorcito para notificaciones
// var notificaciones = io.of('/notificaciones').on('connection', function (socket_notis) {
// 		socket_notis.on('test', function (msg) {
// 			// io.emit('chat message2', msg);
// 			console.log(socket_notis.id);
// 		});
// 	});

// socket general
io.on('connection', function (socket) {
	// Este canal se encarga de emitir el id de socket para guardar
	io.to(socket.id).emit('canal login', socket.id);
	console.log(socket.id + ' llegar');
	
	socket.on('canal server', function(msg) {
		if (msg.socket_id) {

			if (typeof msg.socket_id == 'string') {
				console.log('emitiendo privado ' + msg.socket_id);
				let socket_id = msg.socket_id;
				delete msg.socket_id;
				io.to(msg.socket_id).emit('canal secreto cliente', {msg:msg, pd: 'mensaje secreto'});
			} else if (typeof msg.socket_id == 'object') {
				let msg_clone = JSON.parse(JSON.stringify(msg));
				delete msg_clone.socket_id;
				msg.socket_id.forEach(s_id => {
					console.log('emitiendo privado a varios ids: ' + s_id);
					
					io.to(s_id).emit('canal secreto cliente', { msg: msg_clone, pd: 'mensaje secreto' });
					console.log({ msg: msg_clone, pd: 'mensaje secreto' });
				});
				
			} else {
				console.log('no se pudieron enviar mensajes a: ' + msg.socket_id);
				console.log(typeof msg.socket_id);
			}
		} else {
			// Emitir publico	
			io.emit('canal del cliente', socket.id);
			console.log('lanzando:' + msg);
		}
		// io.emit('cliente_nueva_conexion ', socket.id);
	});	
	socket.on('disconnect', function() {
		console.log(socket.id + ' se ir');
		// con.connect(function (err) {
		// 	if (err) throw err;
		// 	console.log("Connected!");
		// 	con.query("SELECT * FROM sasc.usuario", function (err, result) {
		// 		if (err) throw err;
		// 		console.log(result);
		// 		con.end();
		// 	});
		// });
	})
});

// Correr el server
http.listen(port, function () {
	console.log('listening on *:' + port);
});
